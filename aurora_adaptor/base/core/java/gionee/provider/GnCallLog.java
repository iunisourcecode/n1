package gionee.provider;

import com.android.internal.telephony.CallerInfo;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.provider.BaseColumns;
import android.provider.CallLog;

/**
 * The CallLog provider contains information about placed and received calls.
 */
public class GnCallLog {
    public static String AUTHORITY = CallLog.AUTHORITY;

    /**
     * The content:// style URL for this provider
     */
    public static Uri CONTENT_URI = CallLog.CONTENT_URI;

    /**
     * Contains the recent calls.
     */
    public static class Calls implements BaseColumns {
        /**
         * The content:// style URL for this table
         */
        public static Uri CONTENT_URI = CallLog.Calls.CONTENT_URI;

        /**
         * The content:// style URL for filtering this table on phone numbers
         */
        public static Uri CONTENT_FILTER_URI = CallLog.Calls.CONTENT_FILTER_URI;

        /**
         * An optional URI parameter which instructs the provider to allow the operation to be
         * applied to voicemail records as well.
         * <p>
         * TYPE: Boolean
         * <p>
         * Using this parameter with a value of {@code true} will result in a security error if the
         * calling package does not have appropriate permissions to access voicemails.
         *
         * @hide
         */
        public static String ALLOW_VOICEMAILS_PARAM_KEY = CallLog.Calls.ALLOW_VOICEMAILS_PARAM_KEY;;

        /**
         * Content uri with {@link #ALLOW_VOICEMAILS_PARAM_KEY} set. This can directly be used to
         * access call log entries that includes voicemail records.
         *
         * @hide
         */
        public static Uri CONTENT_URI_WITH_VOICEMAIL = CallLog.Calls.CONTENT_URI_WITH_VOICEMAIL;

        /**
         * The default sort order for this table
         */
        public static String DEFAULT_SORT_ORDER = CallLog.Calls.DEFAULT_SORT_ORDER;

        /**
         * The MIME type of {@link #CONTENT_URI} and {@link #CONTENT_FILTER_URI}
         * providing a directory of calls.
         */
        public static String CONTENT_TYPE = CallLog.Calls.CONTENT_TYPE;

        /**
         * The MIME type of a {@link #CONTENT_URI} sub-directory of a single
         * call.
         */
        public static String CONTENT_ITEM_TYPE = CallLog.Calls.CONTENT_ITEM_TYPE;

        /**
         * The type of the call (incoming, outgoing or missed).
         * <P>Type: INTEGER (int)</P>
         */
        public static String TYPE = CallLog.Calls.TYPE;

        /** Call log type for incoming calls. */
        public static int INCOMING_TYPE = CallLog.Calls.INCOMING_TYPE;
        /** Call log type for outgoing calls. */
        public static int OUTGOING_TYPE = CallLog.Calls.OUTGOING_TYPE;
        /** Call log type for missed calls. */
        public static int MISSED_TYPE = CallLog.Calls.MISSED_TYPE;
        /**
         * Call log type for voicemails.
         * @hide
         */
        public static int VOICEMAIL_TYPE = CallLog.Calls.VOICEMAIL_TYPE;

        /**
         * The phone number as the user entered it.
         * <P>Type: TEXT</P>
         */
        public static String NUMBER = CallLog.Calls.NUMBER;

        /**
         * The ISO 3166-1 two letters country code of the country where the
         * user received or made the call.
         * <P>
         * Type: TEXT
         * </P>
         *
         * @hide
         */
        public static String COUNTRY_ISO = CallLog.Calls.COUNTRY_ISO;

        /**
         * The date the call occured, in milliseconds since the epoch
         * <P>Type: INTEGER (long)</P>
         */
        public static String DATE = CallLog.Calls.DATE;

        /**
         * The duration of the call in seconds
         * <P>Type: INTEGER (long)</P>
         */
        public static String DURATION = CallLog.Calls.DURATION;

        /**
         * Whether or not the call has been acknowledged
         * <P>Type: INTEGER (boolean)</P>
         */
        public static String NEW = CallLog.Calls.NEW;

        /**
         * The cached name associated with the phone number, if it exists.
         * This value is not guaranteed to be current, if the contact information
         * associated with this number has changed.
         * <P>Type: TEXT</P>
         */
        public static String CACHED_NAME = CallLog.Calls.CACHED_NAME;

        /**
         * The cached number type (Home, Work, etc) associated with the
         * phone number, if it exists.
         * This value is not guaranteed to be current, if the contact information
         * associated with this number has changed.
         * <P>Type: INTEGER</P>
         */
        public static String CACHED_NUMBER_TYPE = CallLog.Calls.CACHED_NUMBER_TYPE;

        /**
         * The cached number label, for a custom number type, associated with the
         * phone number, if it exists.
         * This value is not guaranteed to be current, if the contact information
         * associated with this number has changed.
         * <P>Type: TEXT</P>
         */
        public static String CACHED_NUMBER_LABEL = CallLog.Calls.CACHED_NUMBER_LABEL;

        /**
         * URI of the voicemail entry. Populated only for {@link #VOICEMAIL_TYPE}.
         * <P>Type: TEXT</P>
         * @hide
         */
        public static String VOICEMAIL_URI = CallLog.Calls.VOICEMAIL_URI;

        /**
         * Whether this item has been read or otherwise consumed by the user.
         * <p>
         * Unlike the {@link #NEW} field, which requires the user to have acknowledged the
         * existence of the entry, this implies the user has interacted with the entry.
         * <P>Type: INTEGER (boolean)</P>
         */
        public static String IS_READ = CallLog.Calls.IS_READ;

        /**
         * A geocoded location for the number associated with this call.
         * <p>
         * The string represents a city, state, or country associated with the number.
         * <P>Type: TEXT</P>
         * @hide
         */
        public static String GEOCODED_LOCATION = CallLog.Calls.GEOCODED_LOCATION;

        /**
         * The cached URI to look up the contact associated with the phone number, if it exists.
         * This value is not guaranteed to be current, if the contact information
         * associated with this number has changed.
         * <P>Type: TEXT</P>
         * @hide
         */
        public static String CACHED_LOOKUP_URI = CallLog.Calls.CACHED_LOOKUP_URI;

        /**
         * The cached phone number of the contact which matches this entry, if it exists.
         * This value is not guaranteed to be current, if the contact information
         * associated with this number has changed.
         * <P>Type: TEXT</P>
         * @hide
         */
        public static String CACHED_MATCHED_NUMBER = CallLog.Calls.CACHED_MATCHED_NUMBER;

        /**
         * The cached normalized version of the phone number, if it exists.
         * This value is not guaranteed to be current, if the contact information
         * associated with this number has changed.
         * <P>Type: TEXT</P>
         * @hide
         */
        public static String CACHED_NORMALIZED_NUMBER = CallLog.Calls.CACHED_NORMALIZED_NUMBER;

        /**
         * The cached photo id of the picture associated with the phone number, if it exists.
         * This value is not guaranteed to be current, if the contact information
         * associated with this number has changed.
         * <P>Type: INTEGER (long)</P>
         * @hide
         */
        public static String CACHED_PHOTO_ID = CallLog.Calls.CACHED_PHOTO_ID;

        /**
         * The cached formatted phone number.
         * This value is not guaranteed to be present.
         * <P>Type: TEXT</P>
         * @hide
         */
        public static String CACHED_FORMATTED_NUMBER = CallLog.Calls.CACHED_FORMATTED_NUMBER;

        /**
         * The subscription id.
         * <P>Type: Integer</P>
         * @hide
         */
        public static String SUBSCRIPTION = GnTelephony.GN_SIM_ID;

        /**
         * The network type
         * <P>Type: Integer</P>
         * @hide
         */
        public static String NETWORK_TYPE = "network_type";

        //MTK-START [mtk04070][111128][ALPS00093395]MTK added
        /**
         * {@hide}
         */
        public static String SIM_ID = CallLog.Calls.SIM_ID;
        
        /**
         * {@hide}
         */
        public static String VTCALL = CallLog.Calls.VTCALL;
        //MTK-END [mtk04070][111128][ALPS00093395]MTK added

        /**
         * Adds a call to the call log.
         *
         * @param ci the CallerInfo object to get the target contact from.  Can be null
         * if the contact is unknown.
         * @param context the context used to get the ContentResolver
         * @param number the phone number to be added to the calls db
         * @param presentation the number presenting rules set by the network for
         *        "allowed", "payphone", "restricted" or "unknown"
         * @param callType enumerated values for "incoming", "outgoing", or "missed"
         * @param start time stamp for the call in milliseconds
         * @param duration call duration in seconds
         *
         * {@hide}
         */
        public static Uri addCall(CallerInfo ci, Context context, String number,
                int presentation, int callType, long start, int duration) {
            return CallLog.Calls.addCall(ci, context, number, presentation, callType, start, duration, 0, -1);
        }

        /**
         * Adds a call to the call log for dual SIM.
         *
         * @param ci the CallerInfo object to get the target contact from.  Can be null
         * if the contact is unknown.
         * @param context the context used to get the ContentResolver
         * @param number the phone number to be added to the calls db
         * @param presentation the number presenting rules set by the network for
         *        "allowed", "payphone", "restricted" or "unknown"
         * @param callType enumerated values for "incoming", "outgoing", or "missed"
         * @param start time stamp for the call in milliseconds
         * @param duration call duration in seconds
         * @param simId valid value is 0 or 1
         *
         * {@hide}
         */
        public static Uri addCall(CallerInfo ci, Context context, String number,
                int presentation, int callType, long start, int duration, int simId) {
            return CallLog.Calls.addCall(ci, context, number, presentation, callType, start, duration, simId, -1);
        }

        /**
         * Adds a call to the call log for dual SIM.
         *
         * @param ci the CallerInfo object to get the target contact from.  Can be null
         * if the contact is unknown.
         * @param context the context used to get the ContentResolver
         * @param number the phone number to be added to the calls db
         * @param presentation the number presenting rules set by the network for
         *        "allowed", "payphone", "restricted" or "unknown"
         * @param callType enumerated values for "incoming", "outgoing", or "missed"
         * @param start time stamp for the call in milliseconds
         * @param duration call duration in seconds
         * @param simId valid value is 0 or 1
         * @param vtCall:
         *  normal telephone = 0;
         *  visual telephone = 1;
         *
         * {@hide}
         */
        public static Uri addCall(CallerInfo ci, Context context, String number,
                int presentation, int callType, long start, int duration, int simId, int vtCall) {
        	return CallLog.Calls.addCall(ci, context, number, presentation, callType, start, duration, simId, vtCall);
        }

        /**
         * Query the call log database for the last dialed number.
         * @param context Used to get the content resolver.
         * @return The last phone number dialed (outgoing) or an empty
         * string if none exist yet.
         */
        public static String getLastOutgoingCall(Context context) {
        	return CallLog.Calls.getLastOutgoingCall(context);
        }

        private static void removeExpiredEntries(Context context) {
            ContentResolver resolver = context.getContentResolver();
            resolver.delete(CONTENT_URI, "_id IN " +
                    "(SELECT _id FROM calls ORDER BY " + DEFAULT_SORT_ORDER
                    + " LIMIT -1 OFFSET 500)", null);
        }

        //MTK-START [mtk04070][111128][ALPS00093395]MTK proprietary methods

        //The fillowing lines are provided and maintained by Mediatek inc.

        /**
         * save call log corresponding phone number ID
         * {@hide}
         */
        public static String DATA_ID = CallLog.Calls.DATA_ID;
        
        /**
         * save raw contact id of a call log corresponding to phone number 
         * {@hide}
         */
        public static String RAW_CONTACT_ID = CallLog.Calls.RAW_CONTACT_ID;

        //The previous lines are provided and maintained by Mediatek inc.
    }
}
