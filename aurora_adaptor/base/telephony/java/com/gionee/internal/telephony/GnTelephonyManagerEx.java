package com.gionee.internal.telephony;

// gionee gaoj 2011-12-1 added for 8255 start
import android.content.Context;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.Log;
import com.android.internal.telephony.ITelephony;
// gionee gaoj 2011-12-1 added for 8255 end
import com.mediatek.telephony.TelephonyManagerEx;
import android.telephony.TelephonyManager;
import android.provider.Settings;

public class GnTelephonyManagerEx {

    private static GnTelephonyManagerEx mInstance = new GnTelephonyManagerEx();
    
    public static GnTelephonyManagerEx getDefault() {
        // TODO Auto-generated method stub
        return mInstance;
    }

    public int getSimIndicatorStateGemini(int slotId) {
        // TODO Auto-generated method stub
        return TelephonyManagerEx.getDefault().getSimIndicatorStateGemini(slotId);
    }
    
    // gionee gaoj 2011-12-1 added for 8255 start
    /**
       * Get service center address
       * @param simId SIM ID
	* @return Current service center address
	*/
    public String getScAddress(int slotId) {
//	   try {
//		   return getITelephony().getScAddressGemini(slotId);
//	   } catch(RemoteException e1) {
//		   return null;
//	   } catch(NullPointerException e2) {
    	return TelephonyManagerEx.getDefault().getScAddress(slotId);
//	   }
    }

    private ITelephony getITelephony() {
        return ITelephony.Stub.asInterface(ServiceManager.getService(Context.TELEPHONY_SERVICE));
    }
    // gionee gaoj 2011-12-1 added for 8255 end

    public boolean setScAddress(String string, int slotId) {
    	return TelephonyManagerEx.getDefault().setScAddress(string, slotId);
    }
	
	//add for QC solution pass not use in the MTK solution. by guoyx 20130201
	public int getSimState (int slotId) {
        return -1; 
    }
    
    //gionee jiaoyuan 20130131 add by CR00770292 start
    public int getNetworkType(){
    	
      return TelephonyManager.getDefault().getNetworkType();
 	
    }

    /**
    * Returns the unique device ID of a subscription, for example, the IMEI for
    * GSM phones. Return null if device ID is not available.
    *
    * <p>Requires Permission:
    *   {@link android.Manifest.permission#READ_PHONE_STATE READ_PHONE_STATE}
    *
    * @param subscription of which deviceID is returned
    */
    public String getDeviceId(int subscription){
    	
    	return TelephonyManager.getDefault().getDeviceIdGemini(subscription);
    	
    }
    //gionee jiaoyuan 20130131 add by CR00770292 end
    /**
    * Returns the unique subscriber ID, for example, the IMSI for a GSM phone
    * for a subscription.
    * Return null if it is unavailable.
    * <p>
    * Requires Permission:
    *   {@link android.Manifest.permission#READ_PHONE_STATE READ_PHONE_STATE}
    *
    * @param subscription whose subscriber id is returned
    */
     public String getSubscriberId(int subscription){
	 return TelephonyManager.getDefault().getSubscriberIdGemini(subscription);
     }

     /**
     * Returns the Service Provider Name (SPN) of a subscription.
     * <p>
     * Availability: SIM state must be {@link #SIM_STATE_READY}
     *
     * @see #getSimState
     *
     * @hide
     */
     public String getSimOperatorName(int subscription){
	 return TelephonyManager.getDefault().getSimOperatorNameGemini(subscription);
     }
	 
	 //Gionee guoyx 20130221 add for CR00773050 begin
    public boolean setPreferredDataSubscription (int subscription) {
        return false;//MSimTelephonyManager.getDefault().setPreferredDataSubscription(subscription);
    }
    
    public int getPreferredDataSubscription () {
        return -1;//MSimTelephonyManager.getDefault().getPreferredDataSubscription();
    }
    //Gionee guoyx 20130221 add for CR00773050 end

    //gionee jiaoyuan 20130305 add for CR00779101 begin
    public String getMultiSimName (Context context, int subscription){
        return null;//Settings.System.getString(context.getContentResolver(), Settings.System.MULTI_SIM_NAME[subscription]);
    }
    //gionee jiaoyuan 20130305 add for CR00779101 end
}
