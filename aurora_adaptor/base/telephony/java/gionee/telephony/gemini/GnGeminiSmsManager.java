
package gionee.telephony.gemini;

import java.util.ArrayList;
import java.util.List;

import com.android.internal.telephony.ISms;
import gionee.telephony.GnSmsMemoryStatus;

import android.app.PendingIntent;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.telephony.PhoneNumberUtils;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.text.TextUtils;
import android.util.Log;
import android.telephony.gemini.GeminiSmsManager;
import android.telephony.SmsMemoryStatus;

public class GnGeminiSmsManager {

    public static void sendTextMessageGemini(String paramString1, String paramString2,
            String paramString3, int paramInt, PendingIntent paramPendingIntent1,
            PendingIntent paramPendingIntent2) {
    	GeminiSmsManager.sendTextMessageGemini(paramString1, paramString2,
                paramString3, paramInt, paramPendingIntent1,
                paramPendingIntent2);
    }

    /**
     * sendMultipartTextMessageGemini is for other operators except of CMCC
     * @param mDest
     * @param mServiceCenter
     * @param messages
     * @param slotId
     * @param sentIntents
     * @param deliveryIntents
     */
    public static void sendMultipartTextMessageGemini(String mDest, String mServiceCenter,
            ArrayList<String> messages, int slotId, ArrayList<PendingIntent> sentIntents,
            ArrayList<PendingIntent> deliveryIntents) {
    	GeminiSmsManager.sendMultipartTextMessageGemini(mDest, mServiceCenter,
                messages, slotId, sentIntents,
                deliveryIntents);
    }

    public static int copyTextMessageToIccCardGemini(String scAddress, String mAddress,
            List<String> messages, int smsStatus, long timeStamp, int slotId) {
        // TODO Auto-generated method stub
        return GeminiSmsManager.copyTextMessageToIccCardGemini(scAddress, mAddress,
                messages, smsStatus, timeStamp, slotId);
    }

    private static String getSmsServiceName(int paramInt) {
        if (paramInt == 0)
            return "isms";
        if (paramInt == 1) {
            return "isms2";
        }
        return null;
    }

    private static boolean isValidParameters(String paramString1, String paramString2,
            PendingIntent paramPendingIntent) {
        ArrayList localArrayList1 = new ArrayList();

        ArrayList localArrayList2 = new ArrayList();

        localArrayList1.add(paramPendingIntent);
        localArrayList2.add(paramString2);

        if (TextUtils.isEmpty(paramString2)) {
            throw new IllegalArgumentException("Invalid message body");
        }

        return isValidParameters(paramString1, localArrayList2, localArrayList1);
    }

    private static boolean isValidParameters(String paramString, ArrayList<String> paramArrayList,
            ArrayList<PendingIntent> paramArrayList1) {
        if (!isValidSmsDestinationAddress(paramString)) {
            for (int i = 0; i < paramArrayList1.size(); ++i) {
                PendingIntent localPendingIntent = (PendingIntent) paramArrayList1.get(i);
                if (localPendingIntent == null)
                    continue;
                try {
                    localPendingIntent.send(1);
                } catch (PendingIntent.CanceledException localCanceledException) {
                }
            }
            Log.d("SMS", "Invalid destinationAddress: " + paramString);
            return false;
        }

        if (TextUtils.isEmpty(paramString)) {
            throw new IllegalArgumentException("Invalid destinationAddress");
        }
        if ((paramArrayList == null) || (paramArrayList.size() < 1)) {
            throw new IllegalArgumentException("Invalid message body");
        }

        return true;
    }

    private static boolean isValidSmsDestinationAddress(String da) {
    	String encodeAddress = PhoneNumberUtils.extractNetworkPortion(da);
        if (encodeAddress == null) {
            return true;
        }
        int spaceCount = 0;
        for (int i = 0; i < da.length(); i++) {
        	if ((da.charAt(i) == ' ') || (da.charAt(i) == '-')) {
        		spaceCount++;
        	}
        }
        return encodeAddress.length() == da.length() - spaceCount;
    }
    
    public static boolean copyMessageToIccGemini(SmsMessage sms) {
        return GeminiSmsManager.copyMessageToIccGemini(sms.getSmsc(), 
                sms.getTpdu(), SmsManager.STATUS_ON_ICC_READ, sms.getMessageSimId());
    }
    
    /**
     * sendMultipartTextMessageWithEncodingTypeGemini is for CMCC operator
     * @param dest
     * @param serviceCenter
     * @param slotId
     * @param codingType
     * @param messages
     * @param deliveryIntents
     * @param sentIntents
     */
    public static void sendMultipartTextMessageWithEncodingTypeGemini(String dest, String serviceCenter, 
            ArrayList<String> messages, int codingType, int slotId, 
            ArrayList<PendingIntent> sentIntents,
            ArrayList<PendingIntent> deliveryIntents) {
    	GeminiSmsManager.sendMultipartTextMessageWithEncodingTypeGemini(dest, serviceCenter, 
                messages, codingType, slotId, sentIntents, deliveryIntents);
    }

    public static SmsMemoryStatus getSmsSimMemoryStatusGemini(int currentSlotId) {
        return GeminiSmsManager.getSmsSimMemoryStatusGemini(currentSlotId);
    }
	public static ArrayList<SmsMessage> getAllMessagesFromIccGemini(int slotId) {
        return GeminiSmsManager.getAllMessagesFromIccGemini(slotId);
    }

    public static boolean deleteMessageFromIccGemini(int i, int slotId) {
        return GeminiSmsManager.deleteMessageFromIccGemini(i, slotId);
    }
}
