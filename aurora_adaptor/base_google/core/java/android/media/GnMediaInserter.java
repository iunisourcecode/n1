package android.media;
import android.content.IContentProvider;
import java.lang.reflect.Method;
import android.os.Build;
import java.lang.reflect.Constructor;
import android.media.MediaInserter;

public class GnMediaInserter{

    public static MediaInserter GenerateMediaInserter(IContentProvider provider, String packageName, int bufferSizePerUri){

          Class[] paramTypes1 = { IContentProvider.class, int.class};
          Class[] paramTypes2= { IContentProvider.class, String.class, int.class};
          Object[] params1 = {provider, bufferSizePerUri}; 
          Object[] params2 = {provider, packageName, bufferSizePerUri}; 
          if(Build.VERSION.SDK_INT<18)
          {

             try{
                  Class<?> sPolicy=null;
                  sPolicy=Class.forName("android.media.MediaInserter");
                  Constructor cst=sPolicy.getConstructor(paramTypes1); 
                  return (MediaInserter)cst.newInstance(params1);

            }catch(Exception e){}
         }
         else
         {
               try{
                  Class<?> sPolicy=null;
                  sPolicy=Class.forName("android.media.MediaInserter");
                  Constructor cst=sPolicy.getConstructor(paramTypes2); 
                  return (MediaInserter)cst.newInstance(params2);
                 }catch(Exception e){}

         }

         return null;
    }


}
