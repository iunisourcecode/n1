package com.gionee.internal.telephony;

import android.content.Context;
import android.os.RemoteException;
import android.os.IBinder;

import com.android.internal.telephony.ITelephony;
import com.gionee.internal.telephony.GnPhone;
import android.os.SystemProperties;
import gionee.telephony.GnTelephonyManager;
import android.os.ServiceManager;
import gionee.telephony.GnTelephonyManager;

public class GnITelephony {
    public static final ITelephony iTelephonyService = ITelephony.Stub.asInterface(ServiceManager
            .getService(Context.TELEPHONY_SERVICE));
    
    public static String getIccCardTypeGemini (ITelephony iTel, int slotId) {
        String result = null;
        try {
		    //Gionee <cheny><2013-4-1> modify for CR00791582 begin
			//result = iTelephonyService.getIccCardTypeGemini(slotId);
		    if (iTel != null) {
                result = iTel.getIccCardTypeGemini(slotId);
			}
		    //Gionee <cheny><2013-4-1> modify for CR00791582 end			
        } catch (Exception e) {
            // TODO: handle exception
        }
        return result;
    }
    
    public static String getIccCardType (ITelephony iTel) {
        String result = null;
        try {
            result = iTelephonyService.getIccCardType();
        } catch (Exception e) {
            // TODO: handle exception
        }
        return result;
//        return "USIM";
    }
    
    public static boolean isTestIccCard (ITelephony iTel) {
        boolean result = false;
        try {
            result = iTelephonyService.isTestIccCard();
        } catch (Exception e) {
        	e.printStackTrace();
        }
        return result;
    }
    
    public static int get3GCapabilitySIM (ITelephony iTelephony) {
        int result = -1;
        try {
            result = iTelephonyService.get3GCapabilitySIM();
        } catch (Exception e) {
            // TODO: handle exception
        }
        return result;
    }

    public static boolean set3GCapabilitySIM(ITelephony iTelephony, int simId){
        boolean result = false;
        try {
            result = iTelephonyService.set3GCapabilitySIM(simId);
        } catch (Exception e) {
            // TODO: handle exception
        }
        return result;
    }

    public static boolean isRadioOnGemini(ITelephony iTelephony,
            int currentSlotId) {
        boolean result = false;
        try {
            result = iTelephonyService.isRadioOnGemini(currentSlotId);
        } catch (RemoteException e) {
        	e.printStackTrace();
        }
        return result;
    }
    
    public static boolean is3GSwitchLocked(ITelephony iTelephony){
        boolean result = false;
        try {
            result = iTelephonyService.is3GSwitchLocked();
        } catch (Exception e) {
            // TODO: handle exception
        }
        return result;
    }

    public static boolean isSimInsert(ITelephony iTelephony, int simId){
        boolean result = false;
        try {
            result = iTelephonyService.isSimInsert(simId);
        } catch (Exception e) {
            // TODO: handle exception
        }
        return result;
    }

   public static void setDataRoamingEnabledGemini(ITelephony iTelephony, boolean enable, int simId){
        try {
            iTelephonyService.setDataRoamingEnabledGemini(enable, simId);
        } catch (Exception e) {
            // TODO: handle exception
        }
   }

   public static void setRoamingIndicatorNeddedProperty(ITelephony iTelephony, boolean property1, boolean property2){
        try {
            iTelephonyService.setRoamingIndicatorNeddedProperty(property1, property2);
        } catch (Exception e) {
            // TODO: handle exception
        }
   }

   public static void registerForSimModeChange(ITelephony iTelephony, IBinder binder, int what){
        try {
            iTelephonyService.registerForSimModeChange(binder, what);
        } catch (Exception e) {
        	e.printStackTrace();
        }
   }
   public static void unregisterForSimModeChange(ITelephony iTelephony, IBinder binder){
        try {
            iTelephonyService.unregisterForSimModeChange(binder);
        } catch (Exception e) {
        	e.printStackTrace();
        }
   }

   public static boolean hasIccCardGemini(ITelephony iTelephony, int slotId) {
       try {
           return iTelephonyService.hasIccCardGemini(slotId);
       } catch (Exception e) {
           
       }
       return false;
   }
   
   public static boolean isFDNEnabledGemini(ITelephony iTelephony, int slotId) {
       try {
           return iTelephonyService.isFDNEnabledGemini(slotId);
       } catch (Exception e) {
    	   e.printStackTrace();
       }
       return false;
   }
   
   public static boolean isFDNEnabled(ITelephony iTelephony) {
       try {
           return iTelephonyService.isFDNEnabled();
       } catch (Exception e) {
           
       }
       return false;
   }
   
   public static boolean isPhbReady(ITelephony iTelephony) {
       try {
           return iTelephonyService.isPhbReady();
       } catch (Exception e) {
           
       }
       return true;
   }
   
   public static boolean isPhbReadyGemini(ITelephony iTelephony, int slotId) {
       try {
           return iTelephonyService.isPhbReadyGemini(slotId);
       } catch (Exception e) {
    	   e.printStackTrace();
       }
       return false;
   }
   
   public static boolean isRejectAllVoiceCall(ITelephony iTelephony) {
       try {
           return false;
       } catch (Exception e) {
    	   e.printStackTrace();
       }
       return false;
   }
   
   public static boolean isRejectAllVideoCall(ITelephony iTelephony) {
       try {
           return false;
       } catch (Exception e) {
    	   e.printStackTrace();
       }
       return false;
   }
   
   public static boolean handlePinMmiGemini(ITelephony iTelephony, String dialString, int simId) {
       try {
           return iTelephonyService.handlePinMmiGemini(dialString, simId);
       } catch (Exception e) {
           
       }
       return false;
   }

// Gionee <wangym><2013-0510> add  for support CMDA  card begin
     public static int getGeminiPhoneType (ITelephony iTelephony, int slotId) {
        return -1;
    }

    public static int getPhoneType(ITelephony iTelephony){
                   return -1;
    }
// Gionee <wangym><2013-0510> add  for support CMDA  card end
     // Gionee <GnFramework><wangym><2013-1230> add  for CR00997639   begin
    public static boolean supplyPinGemini(ITelephony iTelephony,String pin, int simId)
    {    
       return false;
    }
    public static boolean supplyPukGemini(ITelephony iTelephony,String puk, String pin, int simId)
    {       
       return false;
    }
    public static  int getIccPin1RetryCountGemini(ITelephony iTelephony,int simId)
    {
		return -1;
        /*if (simId == GnPhone.GEMINI_SIM_2) {
            return SystemProperties.getInt("gsm.sim.retry.pin1.2",-1);
        } else {
            return SystemProperties.getInt("gsm.sim.retry.pin1",-1);
        }*/
    }
    public static  int getIccPuk1RetryCountGemini(ITelephony iTelephony,int simId)
    {
        return -1;
        /*if (simId == GnPhone.GEMINI_SIM_2) {
            return SystemProperties.getInt("gsm.sim.retry.puk1.2",-1);
        } else {
            return SystemProperties.getInt("gsm.sim.retry.puk1",-1);
        }*/
    }
   // Gionee <GnFramework><wangym><2013-1230> add  for CR00997639   end
    
   //aurora add zhouxiaobing 20131115 start
    public static int get3GCapabilitySIM () {
//        return iTelephony.get3GCapabilitySIM();
        return -1;
    }   
    public static int getSimIndicatorState()
    {
     return 1;
    }
    public static int getSimIndicatorStateGemini(int simId)
    {
     return getSimIndicatorState();
    }
    //aurora add zhouxiaobing 20131115 end

}
