package gionee.telephony;

import android.content.Context;
import android.os.RemoteException;
import android.os.SystemProperties;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import com.android.internal.telephony.ITelephony;
import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneConstants;
import com.android.internal.telephony.TelephonyProperties;
// Gionee:wangth 20130228 add for CR00773823 begin
import java.io.IOException;
// Gionee:wangth 20130228 add for CR00773823 end
import android.util.Log;
import com.android.internal.telephony.ITelephony;
//import com.android.internal.telephony.IVideoTelephony;
import android.os.ServiceManager;
import android.os.RemoteException;
import android.provider.Settings;

import com.mediatek.common.featureoption.FeatureOption;
import gionee.provider.GnTelephony.SIMInfo;
import gionee.provider.GnTelephony.SimInfo;

public class GnTelephonyManager {
    //Gionee guoyx 20130223 add for Qualcomm solution CR00773050 begin
    /** SIM card state: SIM Card Deactivated, only the sim card is activated,
     *   we can get the other sim card state(eg:ready, pin lock...)
     *@hide
     */
     public static int SIM_STATE_DEACTIVATED = 0x0A;
   //Gionee guoyx 20130223 add for Qualcomm solution CR00773050 end

    private static GnTelephonyManager mInstance = new GnTelephonyManager();

    public static boolean isMultiSimEnabled() {
//        return FeatureOption.MTK_GEMINI_SUPPORT;
    	return true;
    }
    
    public static GnTelephonyManager getDefault() {
        return mInstance;
    }

    public static boolean hasIccCardGemini(int geminiSim2) {
        return telManager.hasIccCardGemini(geminiSim2);
    }
    
    private static TelephonyManager telManager = TelephonyManager.getDefault();

    public static String getLine1Number() {
        return telManager.getLine1Number();
    }

    public static boolean hasIccCard() {
        return telManager.hasIccCard();
    }

    public static int getDataState() {
        return telManager.getDataState();
    }

    public static String getLine1NumberGemini(int slotId) {
//        return MmsApp.getApplication().getTelephonyManager().getLine1NumberGemini(slotId);
        return telManager.getLine1Number();
    }
    
    public static boolean isNetworkRoamingGemini(int simId) {
//        return (simId == Phone.GEMINI_SIM_1)
//            ? "true".equals(SystemProperties.get(TelephonyProperties.PROPERTY_OPERATOR_ISROAMING))
//            : "true".equals(SystemProperties.get(TelephonyProperties.PROPERTY_OPERATOR_ISROAMING_2));
    	return telManager.isNetworkRoamingGemini(simId);
    }

    public static int getDataStateGemini(int simId) {
    	return telManager.getDataStateGemini(simId);
    }
    
    public static void listenGemini(PhoneStateListener listener, int state, int simId) {
//        ((TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE))
//                .listenGemini(listener, state, simId);
    	telManager.listenGemini(listener, state, simId);
    }
    
    public static int getCallStateGemini(int simId) {
//        return ((TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE))
//                .getCallStateGemini(simId);
    	return telManager.getCallStateGemini(simId);
    }

    public static int getSimStateGemini(int currentSlotId) {
        return telManager.getSimStateGemini(currentSlotId);
    }
    
    public static String getVoiceMailNumberGemini(int slotId) {
        return telManager.getVoiceMailNumberGemini(slotId);
    }
    
    public static String getDeviceIdGemini(int simId) {
        return telManager.getDeviceIdGemini(simId);
    }
    
    public static String getSN() {
        ITelephony iTel = ITelephony.Stub.asInterface(ServiceManager.getService(Context.TELEPHONY_SERVICE));
        
        try {
            return iTel.getSN();
        } catch (Exception ex) {
            // the phone process is restarting.
            return null;
        }
    }

	public static void creatQcNvItems(Context context) {
		return;
	}
	public static String getSN(Context context) {
		return null;
	}

    public static String getNetworkOperatorGemini(int simId) {
        return telManager.getNetworkOperatorGemini(simId);
    }

    public static String getSubscriberIdGemini(int simId) {
        return telManager.getSubscriberIdGemini(simId);
    }
    
    public static String getSimOperatorGemini(int simId) {
        return telManager.getSimOperatorGemini(simId);
    }

    public static boolean isValidSimState(int slotId) {
        return false;//telManager.isValidSimState();
    }
    
    public static int getIccSubSize(int slotId) {
        return 0;//TelephonyManager.getDefault().getIccSubSize();
    }

    public static int getSIMStateIcon(int simStatus) {
        return -1;
    }
    
    // Gionee:wangth 20130329 add for CR00791133 begin
    public static boolean isVTCallActive() {
        boolean isVTActive = false;
        
/*        try {
            if (isMultiSimEnabled()) {
                IVideoTelephony vtCall = IVideoTelephony.Stub.asInterface(ServiceManager
                        .checkService("videophone"));

                if (vtCall != null) {
                    if (!vtCall.isVtIdle()) {
                        isVTActive = true;
                    }
                }
            }
        } catch (RemoteException e) {
            Log.w("GnTelephonyManager", "isVTActive() failed", e);
        }
*/
        return isVTActive;
    }
    
    public static void showCallScreenWithDialpad(boolean show) {
        Log.w("GnTelephonyManager", "   show. " + show);
//        ITelephonyMSim telephonyServiceMSim = ITelephonyMSim.Stub
//                .asInterface(ServiceManager.checkService("phone_msim"));
//        if (telephonyServiceMSim == null) {
//            Log.w("GnTelephonyManager", "Unable to find ITelephony interface.");
//            return;
//        } else {
//            try {
//                telephonyServiceMSim.showCallScreenWithDialpad(show);
//            } catch (RemoteException e) {
//                Log.w("GnTelephonyManager", "phone.showCallScreenWithDialpad() failed", e);
//            }
//        }
    }
    // Gionee:wangth 20130329 add for CR00791133 end

    // aurora add zhouxiaobing 20131115 start
    public static int getNetworkTypeGemini(int simId) {

        try {
            ITelephony telephony = ITelephony.Stub.asInterface(ServiceManager
            		.getService(Context.TELEPHONY_SERVICE));
            if (telephony != null) {
                return telephony.getNetworkTypeGemini(simId);
            } else {
                // This can happen when the ITelephony interface is not up yet.
                return 0;
            }
        } catch (RemoteException ex) {
            // This shouldn't happen in the normal case
            return 0;
        } catch (NullPointerException ex) {
            // This could happen before phone restarts due to crashing
            return 0;
        }
    }

    // aurora add zhouxiaobing 20131115 end
    // aurora add zhouxiaobing 20140521 start
    public static int getPhoneCount() {
     	if(!isMultiSimEnabled()) {
    		return 1;
    	} else {
    		return 2;
    	}
    }
    // aurora add zhouxiaobing 20140521 end
    
    public static int getDefaultSubscription(Context context) {
      	if(!isMultiSimEnabled()) {
    		return 0;
    	} else {
    		return getMtkSub(context, Settings.System.VOICE_CALL_SIM_SETTING);
    	}
    }
    
	public static int getPreferredDataSubscription(Context context) {
	   	if(!isMultiSimEnabled()) {
    		return 0;
    	} else {
            return getMtkSub(context, Settings.System.GPRS_CONNECTION_SIM_SETTING);
    	}
	}
    
	private static int getMtkSub(Context context, String dataString) {
		long simId = Settings.System.getLong(context.getContentResolver(), 
			  dataString,
	          Settings.System.DEFAULT_SIM_NOT_SET);
		  SIMInfo simInfo = SIMInfo.getSIMInfoById(context, simId);
		  if(simInfo != null) {
			  return simInfo.mSlot;
		  } else {
			  return 0;
		  }
	}
	
    public static String getNetworkOperatorNameGemini(int simId) {
        return telManager.getNetworkOperatorNameGemini(simId);
    }
    
	public static boolean isSimClosed(Context context, int slot) {
        return false;
	}
	
} 
