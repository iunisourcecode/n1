/*******************************************************************************
 * Filename:
 * ---------
 *  ProductConfiguration.java
 *
 * Project:
 * --------
 *   Browser
 *
 * Description:
 * ------------
 *  get UA infor
 *
 * Author:
 * -------
 *  2012.07.19  
 *
 ****************************************************************************/
package com.gionee.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.content.Context;
import android.os.Build;
import android.os.SystemProperties;
import android.telephony.TelephonyManager;

public class ProductConfiguration {

	public static String getUAString(){
		/*
		UA: Mozilla/5.0 (Linux; U; Android 4.0.3; zh-cn;GiONEE-GN868/Phone

		Build/IMM76D) AppleWebKit534.30(KHTML,like Gecko)Version/4.0 Mobile

		Safari/534.30 Id/54D9886DF90D58715DFFFA2306D30075

		*/
		String brand = SystemProperties.get("ro.product.brand", "GiONEE");
        String model = SystemProperties.get("ro.product.model", "Phone");
        String extModel = SystemProperties.get("ro.gn.extmodel", "Phone");
        //Date date = new Date(Build.TIME);
        //String strTime = new SimpleDateFormat("MM.dd.yyyy").format(date);
        String language = Locale.getDefault().getLanguage();	//zh
        String country = Locale.getDefault().getCountry().toLowerCase();	//cn
	
        String strImei = TelephonyManager.getDefault().getDeviceId();
        String decodeImei = GNDecodeUtils.get(strImei);
        
        String uaString = "Mozilla/5.0 (Linux; U; Android " + Build.VERSION.RELEASE +"; " 
        + language + "-" + country + ";" + brand + "-" + model + "/" + extModel 
        + " Build/IMM76D) AppleWebKit534.30(KHTML,like Gecko)Version/4.0 Mobile Safari/534.30 Id/"
        + decodeImei;
        
		return uaString; 
	}
	
	public static String getUAString(String strImei){
		String brand = SystemProperties.get("ro.product.brand", "GiONEE");
        String model = SystemProperties.get("ro.product.model", "Phone");
        String extModel = SystemProperties.get("ro.gn.extmodel", "Phone");
        String language = Locale.getDefault().getLanguage();	//zh
        String country = Locale.getDefault().getCountry().toLowerCase();	//cn
	
        String decodeImei = GNDecodeUtils.get(strImei);
        
        String uaString = "Mozilla/5.0 (Linux; U; Android " + Build.VERSION.RELEASE +"; " 
        + language + "-" + country + ";" + brand + "-" + model + "/" + extModel 
        + " Build/IMM76D) AppleWebKit534.30(KHTML,like Gecko)Version/4.0 Mobile Safari/534.30 Id/"
        + decodeImei;
        
		return uaString; 
	}
}

