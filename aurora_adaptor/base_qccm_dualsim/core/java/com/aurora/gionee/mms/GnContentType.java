package com.google.gionee.mms;

import java.util.ArrayList;

public class GnContentType {

    public static final String TEXT_PLAIN        = "text/plain";
    public static final String TEXT_HTML         = "text/html";
    public static final String TEXT_VCALENDAR    = "text/x-vCalendar";
    public static final String TEXT_VCARD        = "text/x-vCard";
    public static final String TEXT_TS           = "text/texmacs"; 
    
    public static final String IMAGE_UNSPECIFIED = "image/*";
    public static final String IMAGE_JPEG        = "image/jpeg";
    public static final String IMAGE_JPG         = "image/jpg";
    public static final String IMAGE_GIF         = "image/gif";
    public static final String IMAGE_WBMP        = "image/vnd.wap.wbmp";
    public static final String IMAGE_PNG         = "image/png";
    public static final String IMAGE_BMP         = "image/x-ms-bmp";
    
    public static final String AUDIO_UNSPECIFIED = "audio/*";
    public static final String AUDIO_AAC         = "audio/aac";
    public static final String AUDIO_AAC_MP4     = "audio/aac_mp4";
    public static final String AUDIO_QCELP       = "audio/qcelp";
    public static final String AUDIO_EVRC        = "audio/evrc";
    public static final String AUDIO_AMR         = "audio/amr";
    public static final String AUDIO_IMELODY     = "audio/imelody";
    public static final String AUDIO_MID         = "audio/mid";
    public static final String AUDIO_MIDI        = "audio/midi";
    public static final String AUDIO_MP3         = "audio/mp3";
    public static final String AUDIO_MPEG3       = "audio/mpeg3";
    public static final String AUDIO_MPEG        = "audio/mpeg";
    public static final String AUDIO_MPG         = "audio/mpg";
    public static final String AUDIO_MP4         = "audio/mp4";
    public static final String AUDIO_X_MID       = "audio/x-mid";
    public static final String AUDIO_X_MIDI      = "audio/x-midi";
    public static final String AUDIO_X_MP3       = "audio/x-mp3";
    public static final String AUDIO_X_MPEG3     = "audio/x-mpeg3";
    public static final String AUDIO_X_MPEG      = "audio/x-mpeg";
    public static final String AUDIO_X_MPG       = "audio/x-mpg";
    public static final String AUDIO_3GPP        = "audio/3gpp";
    public static final String AUDIO_OGG         = "application/ogg";
    public static final String AUDIO_VORBIS      = "audio/vorbis";
    
    public static final String VIDEO_UNSPECIFIED = "video/*";
    public static final String VIDEO_3GPP        = "video/3gpp";
    public static final String VIDEO_3G2         = "video/3gpp2";
    public static final String VIDEO_H263        = "video/h263";
    public static final String VIDEO_MP4         = "video/mp4";
    public static final String VIDEO_TS          = "video/mp2ts";
    
    private static final ArrayList<String> sRestrictedContentTypes = new ArrayList<String>();
    
    static {
        sRestrictedContentTypes.add(IMAGE_JPEG);
        sRestrictedContentTypes.add(IMAGE_JPG);
        sRestrictedContentTypes.add(IMAGE_WBMP);        
        sRestrictedContentTypes.add(IMAGE_GIF);                
        sRestrictedContentTypes.add(TEXT_PLAIN);                        
        sRestrictedContentTypes.add(TEXT_TS);  
        sRestrictedContentTypes.add(AUDIO_MID);                                
        sRestrictedContentTypes.add(AUDIO_MIDI);
        sRestrictedContentTypes.add(AUDIO_AMR);        
        sRestrictedContentTypes.add(AUDIO_VORBIS);        
        sRestrictedContentTypes.add(AUDIO_X_MID);                
        sRestrictedContentTypes.add(AUDIO_X_MIDI);
        sRestrictedContentTypes.add(VIDEO_3GPP);        
        sRestrictedContentTypes.add(VIDEO_3G2);                
        sRestrictedContentTypes.add(VIDEO_MP4);                        
        sRestrictedContentTypes.add(VIDEO_TS); 
    }
    
    private GnContentType() {
    }
    
    public static boolean isRestrictedType(String contentType) {
        return (null != contentType) && sRestrictedContentTypes.contains(contentType);
    }
}
