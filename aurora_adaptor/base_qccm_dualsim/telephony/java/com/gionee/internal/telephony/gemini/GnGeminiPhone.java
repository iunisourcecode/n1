package com.gionee.internal.telephony.gemini;


public class GnGeminiPhone{

  public static String EVENT_PRE_3G_SWITCH = "com.mtk.PRE_3G_SWITCH";
  public static String EVENT_3G_SWITCH_DONE = "com.mtk.3G_SWITCH_DONE";
  public static String EVENT_3G_SWITCH_START_MD_RESET = "com.mtk.EVENT_3G_SWITCH_START_MD_RESET";
  public static String EVENT_3G_SWITCH_LOCK_CHANGED = "com.mtk.EVENT_3G_SWITCH_LOCK_CHANGED";

  public static String EXTRA_3G_SIM = "3G_SIM";
  public static String EXTRA_3G_SWITCH_LOCKED = "com.mtk.EXTRA_3G_SWITCH_LOCKED";

}
