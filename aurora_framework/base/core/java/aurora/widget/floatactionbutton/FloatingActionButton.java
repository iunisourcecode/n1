package aurora.widget.floatactionbutton;


import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.StateListAnimator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Outline;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.MotionEvent;
import android.view.TouchDelegate;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewOutlineProvider;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import java.lang.CharSequence;

import com.aurora.R;
import com.aurora.utils.DensityUtil;
import com.aurora.utils.ViewUtils;
public class FloatingActionButton extends View {


	
	
	private static final int SIZE_MINI = 0;
	
	private static final int SIZE_NORMAL = 1;
	
	private static final int SIZE_LARGE = 2;

  /**
   * listener for main button clicked
   */
  private OnFloatActionButtonClickListener mClickListener;
  
  private Context mContext;
  
  private Resources mRes;
  

  private Drawable mIconDrawable;
  
  private Paint mPaint;
  
  private int mWidth,mHeight;
  
  /**
   * listener for float action button clicked
   * @author alexluo
   *
   */
  public interface OnFloatActionButtonClickListener{
	  /**
	   * if you want to do something when main button clicked,
	   * such as:show a dialog,or start an activity,you should
	   * implement this method
	   */
	  void onClick();
  }
  
  
  public FloatingActionButton(Context context) {
      super(context);
  }

  public FloatingActionButton(Context context, AttributeSet attrs) {
      this(context, attrs, R.attr.floatActionButtonStyle);
  }

  public FloatingActionButton(Context context, AttributeSet attrs, int defStyleAttr) {
      this(context, attrs, defStyleAttr, 0);
  }

  public FloatingActionButton(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
      super(context, attrs, defStyleAttr, defStyleRes);
      init(context, attrs, defStyleAttr);
  }

  private void init(Context context, AttributeSet attributeSet,int defStyle) {
	  mPaint = new Paint();
	  mPaint.setAntiAlias(true);
	  
	  mContext = context;
	  mRes = context.getResources();
	  int size = R.dimen.float_button_size_normal;
	  TypedArray attr = context.obtainStyledAttributes(attributeSet, R.styleable.FloatingActionButton, defStyle,0 );
	  
	  int buttonSize =  attr.getInt(R.styleable.FloatingActionButton_floatButtonSize,SIZE_NORMAL);
	  attr.recycle();
	  
	  attr = context.obtainStyledAttributes(attributeSet, com.android.internal.R.styleable.ImageView, defStyle,0 );
	  mIconDrawable = attr.getDrawable(com.android.internal.R.styleable.ImageView_src);
	  attr.recycle();
		switch (buttonSize) {
		case SIZE_LARGE:
			size = R.dimen.float_button_size_large;
			break;
		case SIZE_NORMAL:
			size = R.dimen.float_button_size_normal;
			break;
		case SIZE_MINI:
			size = R.dimen.float_button_size_mini;
			break;

		}
	mWidth = mHeight = mRes.getDimensionPixelOffset(size);
     setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			
			if(mClickListener != null){
				mClickListener.onClick();
				
			}
		}
	});
      
//     ViewUtils.setupFloatingActionButton(this, getResources());
  }

	/**
	 * clip this view to cycle by set outline
	 */
	private void clip(){
		setClipToOutline(true);
		StateListAnimator animation = AnimatorInflater.loadStateListAnimator(mContext, R.anim.float_button_state_list_anim);
		setStateListAnimator(animation);
	}

  /**
   * set click listener to float button
   * @param listener
   */
  public void setOnFloatingActionButtonClickListener(OnFloatActionButtonClickListener listener){
	  mClickListener = listener;
  }


  @Override
protected void onDraw(Canvas canvas) {
	// TODO Auto-generated method stub
	super.onDraw(canvas);
	final int width = mIconDrawable.getIntrinsicWidth();
	final int height = mIconDrawable.getIntrinsicHeight();
	final int parentWidth = getMeasuredWidth();
	final int parentHeight = getMeasuredHeight();
	
	final int left = parentWidth /2 - width/2;
	final int top = parentHeight/2 - height/2;
	mIconDrawable.setBounds(left, top, left+width, top+height);
	mIconDrawable.draw(canvas);
}
  /**
   * update current icon drawable
   * @param icon
   */
  public void setIconDrawable(Drawable icon){
	  mIconDrawable = icon;
	  invalidate();
  }
  
  /**
   * play animation when click float button
   */
  public void playTranslateAnimation(){
	  
  }
  
  @Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		// TODO Auto-generated method stub
//		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		
		setMeasuredDimension(mWidth, mHeight);
		clip();
	}
  
  
  
  
  
  
}
