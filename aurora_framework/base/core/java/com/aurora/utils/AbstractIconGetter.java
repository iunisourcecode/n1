package com.aurora.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.HashMap;
import java.util.List;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.ComponentInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;

/**
 * 
 * apply some interface for get icons.
 * @author antino
 *
 */
public abstract class AbstractIconGetter {
	private static final String TAG = IconConst.TAG;
	
	/* Shadow type */
	public static final int OUTER_SHADOW = 0;
	public static final int INTER_SHADOW = 1;
	public static final int OUTER_NONE_SHADOW_WITH_BOTTOM = 2;
	public static final int INTER_NONE_SHADOW_WITH_BOTTOM = 3;
	public static final int NONE_SHADOW = 4;
	
	//Target package info.
	public static String RES_PKG="com.aurora.publicicon.res";
	protected Context mPreContext;
	//Save icon
	protected IconSaver mIconSaver;
	//Icon configuration in LauncherRes resource
	protected static final String PACKAGE_CLASS= "lable_map";
	protected HashMap<String, String> mlables_icons = new HashMap<String, String>();
	//Icon dpi
	protected static int mIconDpi;
	
	protected AbstractIconGetter(Context context){
		initPreContext(context);
		initPreContextResources();
		dealVersionChanged();
	}
	
	private void dealVersionChanged() {
		Context context = mPreContext;
		String curLauncherResVersion = getVersion(context,RES_PKG);
		String versionFilePath = IconConst.icon_dir_prefix+IconConst.ICON_VERSION_FILENAME;
		String oldLauncherResVersion = getStringFromPath(versionFilePath,IconConst.ICON_VERSION_KEY,"1.000");
		if(!isEqual(curLauncherResVersion,oldLauncherResVersion)){
			deleteDirExeptRoot(IconConst.icon_dir_prefix);
			deleteDirExeptRoot(IconConst.icon_sddir_prefix);
			saveVersion(IconConst.icon_dir_prefix,IconConst.ICON_VERSION_KEY,curLauncherResVersion);
		}
	}
	
	private void saveVersion(String path, String key,
			String value) {
		FileUtils.setValue(path, key, value);
	}

	private boolean isEqual(String s1,String s2){
		if(s1!=null&&(!"".equals(s1))&&s1.equals(s2))return true;
		return false;
	}
	
	private void deleteDirExeptRoot(String path) {
		FileUtils.deleteFileExeptRoot(path);
	}
	
	private void deleteDir(String path) {
		FileUtils.deleteFile(path);
	}

	private String getVersion(Context context,String packageName) {
	    try {
	        PackageManager manager = context.getPackageManager();
	        PackageInfo info = manager.getPackageInfo(packageName, 0);
	        String version = info.versionName;
	        return version;
	    } catch (Exception e) {
	        e.printStackTrace();
	        return null;
	    }
	}
	
	private String getStringFromPath(String path,String key,String defaultVal){
		String returnVal = "";
	    String res ="";
		File file = new File(path);
		if(file.exists()){
			BufferedReader bread = null;
			try {
				bread = new BufferedReader(new FileReader(file));
				while( (res = bread.readLine()) != null){
					if(res.contains(key)){
						returnVal = res.split(":")[1].toString();
						break;
					}else{
						returnVal = defaultVal;
					}
				}
				bread.close(); 
			} catch (Exception e) {
				Log.d(TAG,"Get ICON Local Version:",e);
			}finally{
				try {
					if(bread != null){
						bread.close();
					}
				} catch (Exception e2) {
					Log.d(TAG,"Get ICON Local Version:",e2);
				}
			}
		}
		return returnVal;
	}
	
	private void initPreContext(Context context) {
		try {
			mPreContext = context.createPackageContext(RES_PKG,Context.CONTEXT_IGNORE_SECURITY);
		} catch (NameNotFoundException e) {
			Log.e(TAG, "Create Res Apk Failed:NameNotFoundException.Please ensure the packageName = "+RES_PKG+" is intalled.");
		} catch (Exception e) {
			Log.e(TAG, "Create Res Apk Failed");
		}
	}

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void initPreContextResources() {
		Context context= mPreContext;
		//Icon DPI
		ActivityManager activityManager = (ActivityManager) context
				.getSystemService(Context.ACTIVITY_SERVICE);
		mIconDpi = activityManager.getLauncherLargeIconDensity();	
		//Launcher Res Map
		Resources res = context.getResources();
		HashMap<String, String> packageClassMap = new HashMap<String, String>();
		final int resId = res.getIdentifier(PACKAGE_CLASS, "array", RES_PKG);
		if (resId == 0){
			return;
		}
		final String[] packageClasseIcons = context.getResources()
				.getStringArray(resId);
		for (String packageClasseIcon : packageClasseIcons) {
			String[] packageClasses_Icon = packageClasseIcon.split("#");
			if (packageClasses_Icon.length == 2) {
				String[] packageClasses = packageClasses_Icon[0].split("\\|");
				for (String s : packageClasses) {
					packageClassMap.put(s.trim(), packageClasses_Icon[1]);
					String[] packageClass = s.split("\\$");
					if (packageClass.length == 2) {
						packageClassMap.put(packageClass[0],
								packageClasses_Icon[1]);
					}
				}
			}
		}
		mlables_icons = packageClassMap;
	}
	
	public Bitmap getIcon(ResolveInfo info,int iconMode){
		Bitmap icon = null;
		if(info.activityInfo!=null){
			icon=getIcon(info.activityInfo,iconMode);
		}else if(info.serviceInfo!=null){
			icon=getIcon(info.serviceInfo,iconMode);
		}
		if(icon==null){
			icon = getDefaultIcon(iconMode);
		}
		return icon;
	}
	
	public Bitmap getIcon(ActivityInfo info,int iconMode){
		Bitmap icon = null;
		icon = getIconFromLocalCache(info,iconMode);
		if(icon==null){
			String iconName = getIconName(info);
			Log.i("xiejun","1:iconName = "+iconName);
			if(iconName!=null){
				icon = getBitmapByName(iconName, iconMode);
			}
			Log.i("xiejun","1:icon = "+icon);
			if(icon==null){
				//TODO:we need get Icon from apk.
				icon=getIconFromApkDefault(info,mPreContext,iconMode);
			}
			Log.i("xiejun","2:icon = "+icon);
		}
		return icon;
	}
	
	public Bitmap getIcon(ServiceInfo info,int iconMode){
		Bitmap icon = null;
		icon = getIconFromLocalCache(info,iconMode);
		if(icon==null){
			String iconName = getIconName(info);
			if(iconName!=null){
				icon = getBitmapByName(iconName, iconMode);
			}
			if(icon==null){
				//TODO:we need get Icon from apk.
				icon=getIconFromApkDefault(info,mPreContext,iconMode);
			}
		}
		return icon;
	}

	public Bitmap getIcon(String pkg,String cls ,int iconMode){
		Context context = mPreContext;
		Bitmap icon = getIconFromCacheDir(pkg,cls,iconMode);;
		if(icon==null){
			String iconName=getIconName(pkg,cls);
			if (iconName != null) {
				icon = getBitmapByName(iconName, iconMode);
			}
			if(icon==null){
				//TODO:we need get Icon from apk.
				icon = getIconFromApk(pkg,cls,iconMode);
			}
		}
		return icon;
	}
	
	public Bitmap getIcon(String pkg, int iconMode){
		Context context = mPreContext;
		Bitmap icon = null;
		icon = getIconFromCacheDir(pkg, null, iconMode);
		if(icon == null){
			String iconName = getIconName(pkg);
			if(iconName==null){
				CharSequence key = getAppLabel(pkg,context);
				if(key!=null){
					iconName = getIconName(key.toString());
				}	
			}
			if(iconName!=null){
				icon = getBitmapByName(iconName, iconMode);
			}
			if (icon == null) {
				ApplicationInfo info = getApplicationInfo(pkg,context);
				icon = getIconFromApkDefault(info, context, iconMode);
			}
		}
		return icon;
	}

	private Bitmap getIconFromApk(String pkg, String cls, int iconMode) {
		if(pkg==null&&cls==null)return null;
		Bitmap bitmap =null;
		if(pkg!=null){
			if(cls!=null){
				ResolveInfo info =getResolveInfo(pkg,cls);
				ComponentInfo cpInfo=info.activityInfo!=null?info.activityInfo:info.serviceInfo;
				bitmap = getIconFromApkDefault(cpInfo,mPreContext,iconMode);
			}else{
				//Load application icon
				ApplicationInfo info = getApplicationInfo(pkg,mPreContext);
				bitmap = getIconFromApkDefault(info,mPreContext,iconMode);
			}
		}
		return bitmap;
	}

	private ApplicationInfo getApplicationInfo(String pkg,Context context) {
		PackageManager pm = context.getPackageManager();
		ApplicationInfo info;
		try {
			info = pm.getApplicationInfo(pkg, 0);
		} catch (NameNotFoundException e1) {
			Log.i(TAG, "getApkFileIcon  :  NameNotFoundException  :  " + pkg);
			info = null;
		}
		return info;
	}
	
	private Bitmap getIconFromApkDefault(ApplicationInfo info, Context context,int iconMode) {
		Bitmap bmp = getIconFromApk(info,mPreContext,iconMode);
		if(bmp == null){
			bmp=getDefaultIcon(iconMode);
		}else{
			//TODO:save icon
		}
		return bmp;
	}
	
	private Bitmap getIconFromApkDefault(ComponentInfo info, Context context,int iconMode) {
		Bitmap bmp = getIconFromApk(info,mPreContext,iconMode);
		if(bmp == null){
			bmp=getDefaultIcon(iconMode);
		}else{
			//TODO:save icon
		}
		return bmp;
	}

	private Bitmap getIconFromApk(ApplicationInfo info, Context context,int iconMode) {
		PackageManager pm = context.getPackageManager();
		Drawable d = null;
		if(info!=null){
			try {
				d = info.loadIcon(pm);
			} catch (Resources.NotFoundException e) {
				d = null;
			}
		}
		if(d!=null){
			Log.i("getIconFromApk","d:("+d.getIntrinsicWidth()+" , "+d.getIntrinsicHeight()+")"+"  ,  info  "+info);
			d = PhotoUtils.normalizeIcon(d,mPreContext);
			Drawable bg_scale = getIconByName(IconConst.BACKGROUND_SCALE_3RD,iconMode);
			Bitmap bmp = PhotoUtils.drawable2bitmap(d);
			Bitmap bm = null;
			if(bg_scale!=null&&d!=null){
			   //bm = PhotoUtils.zoom(bmp, PhotoUtils.drawable2bitmap(bg_scale));
			}
			Log.i("daye","bmp:( "+bmp.getWidth()+" , "+bmp.getHeight()+" )");
			d = customizeIcon(d, iconMode, false);
		}
		return d==null?null:PhotoUtils.drawable2bitmap(d);
	}

	@SuppressLint("NewApi")
	private Bitmap getIconFromApk(ComponentInfo info, Context context,int iconMode) {
		Drawable d = null;
		if (info != null) {
			PackageManager pm = context.getPackageManager();
			Resources res;
			try {
				res = pm.getResourcesForApplication(info.applicationInfo);
			} catch (NameNotFoundException e) {
				res = null;
				Log.i(TAG, "exception has catched!", e);
			}
			// try to get the icon from application

			if (res != null) {
				int iconId = info.getIconResource();
				//Log.i(TAG,"iconId = "+iconId);
				if (iconId != 0) {
					try {
						d = res.getDrawableForDensity(iconId, mIconDpi);
					} catch (Resources.NotFoundException e) {
						d = null;
					}
				}else{
					d = pm.getApplicationIcon(info.applicationInfo);
				}
			}
		}
		
		if(d!=null){
			Log.i("getIconFromApk","d:("+d.getIntrinsicWidth()+" , "+d.getIntrinsicHeight()+")"+"  ,  info  "+info);
			d = PhotoUtils.normalizeIcon(d,mPreContext);
			Drawable bg_scale = getIconByName(IconConst.BACKGROUND_SCALE_3RD,iconMode);
			Bitmap bmp = PhotoUtils.drawable2bitmap(d);
			Bitmap bm = null;
			if(bg_scale!=null&&d!=null){
			   //bm = PhotoUtils.zoom(bmp, PhotoUtils.drawable2bitmap(bg_scale));
			}
			Log.i("daye","bmp:( "+bmp.getWidth()+" , "+bmp.getHeight()+" )");
			d = customizeIcon(bm==null?bmp:bm, iconMode, false);
		}
		return d==null?null:PhotoUtils.drawable2bitmap(d);
	}
	
	private ResolveInfo getResolveInfo(String pkg, String cls) {
		Context context = mPreContext;
		PackageManager pm = context.getPackageManager();
		Intent intent = new Intent();
		intent.setClassName(pkg, cls);
		List<ResolveInfo> infos = pm.queryIntentActivities(intent, 0);
		ResolveInfo info = null;
		if(!infos.isEmpty()){
			info = infos.get(0);
		}
		return info;
	}

	
	private CharSequence getAppLabel(String pkg, Context context) {
		PackageManager pm = context.getPackageManager();
		ApplicationInfo info = null;
		try {
			info = pm.getApplicationInfo(pkg, 0);
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		CharSequence key = null;
		if(info != null){
			key = info.loadLabel(pm);
		}
		return key;
	}

	private Bitmap getBitmapByName(String iconName, int iconMode) {
		Bitmap bitmap = null;
		Drawable drawable = getIconByName(iconName,iconMode);
		Log.d("GetBitmapByName","1:getBitmapByName:drawable="+drawable);
		if(drawable!=null){
			drawable=customizeIcon(drawable,iconMode,true);
			if(drawable!=null){
				//TODO: Here maybe have a better implementation.
				bitmap = PhotoUtils.drawable2bitmap(drawable); //PhotoUtils.createIconBitmap(drawable,mPreContext);
			}
		}
		Log.d("GetBitmapByName","2:getBitmapByName:bitmap="+bitmap);
		return bitmap;
	}
	
	private boolean isNeedScale(Drawable src){
		if(src!=null){
			int w = src.getIntrinsicWidth();
			if(w > PhotoUtils.getIconBitmapSize()){
				return false;
			}
		}
		return true;
	}
	
	private boolean isNeedScale(Bitmap src){
		if(src!=null){
			int w = src.getWidth();
			if(w > PhotoUtils.getIconBitmapSize()){
				return false;
			}
		}
		return true;
	}
	
	private Drawable customizeIcon(Bitmap src,int iconMode, boolean isOurSelf) {
		Drawable bg_scale = null;
		Drawable bg=null;
		Drawable shadow = null;
		Drawable mask = null;
		if (isOurSelf) {
			if (isNeedScale(src)) {
				bg_scale = getIconByName(IconConst.BACKGROUND_SCALE, iconMode);
				bg = getIconByName(IconConst.BACKGROUND, iconMode);
				shadow = getIconByName(IconConst.SHADOW, iconMode);
				mask = getIconByName(IconConst.MASK, iconMode);
			}
		} else {
			//bg_scale = getIconByName(IconConst.BACKGROUND_SCALE_3RD,iconMode);
			bg=getIconByName(IconConst.BACKGROUND3RD,iconMode);
			shadow=getIconByName(IconConst.SHADOW3RD,iconMode);
			mask = getIconByName(IconConst.MASK3RD,iconMode);
		}
		Bitmap resultBmp = PhotoUtils.customizeIcon(src,bg_scale,bg, shadow, mask);
		return resultBmp==null?null:new BitmapDrawable(mPreContext.getResources(),resultBmp);
	}
	private Drawable customizeIcon(Drawable src,int iconMode, boolean isOurSelf) {
		Drawable bg_scale = null;
		Drawable bg=null;
		Drawable shadow = null;
		Drawable mask = null;
		if(isOurSelf){
			if (isNeedScale(src)) {
				bg_scale = getIconByName(IconConst.BACKGROUND_SCALE, iconMode);
				bg = getIconByName(IconConst.BACKGROUND, iconMode);
				shadow = getIconByName(IconConst.SHADOW, iconMode);
				mask = getIconByName(IconConst.MASK, iconMode);
			}
		}else{
			//bg_scale = getIconByName(IconConst.BACKGROUND_SCALE_3RD,iconMode);
			bg=getIconByName(IconConst.BACKGROUND3RD,iconMode);
			shadow=getIconByName(IconConst.SHADOW3RD,iconMode);
			mask = getIconByName(IconConst.MASK3RD,iconMode);
		}
		Bitmap resultBmp = PhotoUtils.customizeIcon(src,bg_scale,bg,shadow, mask);
		return resultBmp==null?null:new BitmapDrawable(mPreContext.getResources(),resultBmp);
	}

	private Drawable getIconByName(String iconName, int iconMode) {
		Context context = mPreContext;
		Drawable drawable =null;
		if(context!=null){
			drawable = PhotoUtils.getIconDrawable(RES_PKG,iconName,context);
		}
		return drawable;
	}

	/**
	 * using packageName , activityName,serviceName or label to find iconName
	 * @param info
	 * @return the icon name is configed in the  Launcher Res array.xml.if it is not exit,return null.
	 */
	private String getIconName(ComponentInfo info) {
		return getIconName(info.packageName,info.name);
	}
	
	private String getIconName(String pkg, String cls) {
		CharSequence key = pkg + "$" + cls;
		String iconName = getIconName(key.toString());
		if(iconName==null){
			iconName = getIconName(pkg);
		}
		if(iconName==null){
			ApplicationInfo info= getApplicationInfo(pkg, mPreContext);
			PackageManager pm = mPreContext.getPackageManager();
			if(info!=null){	
				CharSequence label = info.loadLabel(pm);
				if(label!=null){
					iconName = getIconName(label.toString());
				}
			}
			
		}
		return iconName;
	}

	private String getIconName(String key) {
		String bitmapName = mlables_icons.get(key.trim());
		String iconName = null;
		if (bitmapName != null) {
			String[] s = bitmapName.split("\\.");
			if (s.length == 2) {
				iconName = s[0];
			}
		}
		return iconName;
	}

	public void clearLocalCaches(){
		deleteDirExeptRoot(IconConst.icon_dir_prefix);
		deleteDirExeptRoot(IconConst.icon_sddir_prefix);
	}
	public Bitmap getDefaultIcon(int iconMode){
		Context context = mPreContext;
		Resources resources = null;
		Drawable d = null;
		Bitmap result = null;
		if(context!=null){
			resources= context.getResources();
		}
		int defaultIconId = resources.getIdentifier("default_app_icon", "mipmap",
				RES_PKG);
		if(defaultIconId!=0){
			d = resources.getDrawable(defaultIconId);
		}else{
			d = getSystemDefaultIcon();
		}
		if(d!=null){
			d=customizeIcon(d, iconMode, true);
		}
		if(d!=null){
			//TODO: Here maybe have a better implementation.
			result = PhotoUtils.createIconBitmap(d,mPreContext);
		}
		return result;
	}
	
	@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1)
	private Drawable getSystemDefaultIcon(){
		Drawable d;
        try {
            d = Resources.getSystem().getDrawableForDensity(android.R.mipmap.sym_def_app_icon, mIconDpi);
        } catch (Resources.NotFoundException e) {
            d = null;
        }
        return d;
	}
	
	public Drawable getSystemIconDrawable(Drawable dr,int iconMode){
		return customizeIcon(dr, iconMode, true);
	}
	
	public Drawable getDefaultIconDrawable(int iconMode) {
		Bitmap bmp = getDefaultIcon(iconMode);
		return bmp==null?null:new BitmapDrawable(mPreContext.getResources(), bmp);
	}
	
	private Bitmap getIconFromLocalCache(ComponentInfo info, int iconMode) {
		return null;
	}
	
	private Bitmap getIconFromCacheDir(String pkg, String cls, int iconMode) {
		return null;
	}
	
	public Bitmap getRoundedBitmap(Bitmap bmp,int iconMode,boolean isOurself) {
		Drawable d = PhotoUtils.normalizeIcon(new BitmapDrawable(mPreContext.getResources(), bmp),mPreContext);
		Bitmap bm = PhotoUtils.drawable2bitmap(d);
		Log.i("daye","bmp:( "+bm.getWidth()+" , "+bm.getHeight()+" )");
		d = customizeIcon(bm==null?bm:bm,iconMode,isOurself);
		return  d==null?null:PhotoUtils.drawable2bitmap(d);
	}
	
	public Bitmap getRoundedBitmap(Bitmap bmp,boolean isOurself) {
		return getRoundedBitmap(bmp,-1,isOurself);
	}
	
	public Bitmap getRoundedBitmap(Bitmap bm) {
		return getRoundedBitmap(bm,true);
	}
	
	public Drawable getIconDrawable(ResolveInfo info,int iconMode){
		Bitmap bmp = getIcon(info,iconMode);
		return bmp==null?null:new BitmapDrawable(mPreContext.getResources(), bmp);
	}
	
	public Drawable getIconDrawable(ActivityInfo info, int iconMode){
		Bitmap bmp = getIcon(info,iconMode);
		return bmp==null?null:new BitmapDrawable(mPreContext.getResources(), bmp);
		
	}
	
	public Drawable getIconDrawable(ServiceInfo info,int iconMode){
		Bitmap bmp = getIcon(info,iconMode);
		return bmp==null?null:new BitmapDrawable(mPreContext.getResources(), bmp);
		
	}
	
	public Drawable getIconDrawable(String pkg, String cls, int iconMode){
		Bitmap bmp = getIcon(pkg,cls,iconMode);
		return bmp==null?null:new BitmapDrawable(mPreContext.getResources(), bmp);
		
	}
	
	public Drawable getIconDrawable( String pkg, int iconMode){
		Bitmap bmp = getIcon(pkg,iconMode);
		return bmp==null?null:new BitmapDrawable(mPreContext.getResources(), bmp);
	}
	
}
