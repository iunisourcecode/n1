/*************************************************************************************
 * 
 * Description:
 * 	Defines ArcSoft APIs for night shot.
 *
 * Author : zhangwu
 * Email  : zhangwu@gionee.com
 * Date   : 2013-09-07
 *
 *************************************************************************************/

#define LOG_TAG "ArcSoftNightVideo"
#include <android/log.h>
#include <string.h>

#include "ArcSoftNightVideo.h"

#define MEM_SIZE	5 * 1024 * 1024 + (2560 * 1440 * 3 / 2) * 6

namespace android { 

ArcSoftNightVideo::ArcSoftNightVideo()
	: mMem(NULL)
	, mMemMgr(NULL)
	, mEnhancer(NULL)
{
	
}
	
int32 
ArcSoftNightVideo::init() 
{
	MRESULT res = 0; 
	
	memset(&mParam, 0, sizeof(mParam));

	pthread_mutex_init(&mMutex, NULL);

	return res;
}
void  
ArcSoftNightVideo::deinit()
{
	ANH_Uninit(&mEnhancer);

	if(mMemMgr != NULL) {
		MMemMgrDestroy(mMemMgr);
		mMemMgr = NULL;
	}

	if(mMem != NULL) {
		MMemFree(NULL, mMem);
		mMem = NULL;
	}

	pthread_mutex_destroy(&mMutex); 
}

int32 
ArcSoftNightVideo::processNightVideo(LPASVLOFFSCREEN param)
{
	MRESULT res = 0;

	//pthread_mutex_lock(&mMutex);

	ASVLOFFSCREEN dstImg;

	dstImg.i32Width = param->i32Width;
	dstImg.i32Height = param->i32Height;
	dstImg.ppu8Plane[0] = param->ppu8Plane[0];
	dstImg.ppu8Plane[1] = param->ppu8Plane[1];
	dstImg.ppu8Plane[2] = param->ppu8Plane[2];
	dstImg.u32PixelArrayFormat = param->u32PixelArrayFormat;

	switch (dstImg.u32PixelArrayFormat) {
		case ASVL_PAF_YV12:
			dstImg.pi32Pitch[0] = dstImg.i32Width;
			dstImg.pi32Pitch[1] = dstImg.i32Width / 2;
			dstImg.pi32Pitch[2] = dstImg.i32Width / 2;
			break;
		case ASVL_PAF_YUYV:
			dstImg.pi32Pitch[0] = dstImg.i32Width;
			dstImg.pi32Pitch[1] = 0;
			dstImg.pi32Pitch[2] = 0;
			break;
		case ASVL_PAF_NV21:
			dstImg.pi32Pitch[0] = dstImg.i32Width;
			dstImg.pi32Pitch[1] = dstImg.i32Width;
			dstImg.pi32Pitch[2] = 0;
			break;
		case ASVL_PAF_I420:
			dstImg.pi32Pitch[0] = dstImg.i32Width;
			dstImg.pi32Pitch[1] = dstImg.i32Width / 2;
			dstImg.pi32Pitch[2] = dstImg.i32Width / 2;
			break;
		default:
			dstImg.pi32Pitch[0] = dstImg.i32Width;
			dstImg.pi32Pitch[1] = dstImg.i32Width / 2;
			dstImg.pi32Pitch[2] = dstImg.i32Width / 2;
			break;
	}
	
	res = ANH_Enhance(mEnhancer, param, &dstImg, &mParam);
	if (res != MOK) {
		PRINTD("%s ANH_Enhance faile [%ld]", __func__, res);
	} 

	//pthread_mutex_unlock(&mMutex);
	
	return res;
}

int32 
ArcSoftNightVideo::enableNightVideo()
{
	MRESULT res = 0;

	if (mMem == NULL) {
		mMem = MMemAlloc(NULL, MEM_SIZE);
		mMemMgr = MMemMgrCreate(mMem, MEM_SIZE);

		res = ANH_Init(mMemMgr, &mEnhancer);
		if (res != 0) {
			PRINTE("Failed to initialize nightshot enginer [#%ld].", res);
			deinit();
		} else {
			res = ANH_GetDefaultParam(&mParam);
			if (res != MOK) {
				PRINTD("%s ANH_GetDefaultParam failed", __func__);
			}
			//we can change the param if needed
			//mParam.lIntensity = mParam.lIntensity - 30;
			//mParam.lDenoise = 10;
		}
	}

	return res;
}

int32 
ArcSoftNightVideo::disableNightVideo()
{
	MRESULT res = 0;

	deinit();

	return res;
}

};


