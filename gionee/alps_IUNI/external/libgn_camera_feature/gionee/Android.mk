LOCAL_PATH:= $(call my-dir)
LOCAL_GIONEE_LIB_ROOT_PATH:= $(LOCAL_PATH)/bin

include $(CLEAR_VARS)

LOCAL_MODULE_TAGS:= optional

LOCAL_SRC_FILES:= GioneeCameraFeature.cpp \
                  FrameProcessor.cpp \
                  GioneeDefogShot.cpp \
				  GioneeSceneDetection.cpp \
                
TARGET_ARCH_ABI :=armeabi-v7a
LOCAL_ARM_MODE := arm
ifeq ($(TARGET_ARCH_ABI),armeabi-v7a)
# 采用NEON优化技术
LOCAL_ARM_NEON := true
endif

LOCAL_SHARED_LIBRARIES:= \
    libutils \
    libcutils \
    libcamera_client

LOCAL_LDFLAGS += -L$(LOCAL_GIONEE_LIB_ROOT_PATH) -limage_defog\
                 -O3 -fopenmp -mfpu=neon-vfpv4   -marm -mfloat-abi=softfp -ffast-math  -march=armv7-a  -std=gnu++11\
                -fprefetch-loop-arrays\
                -ftree-vectorize  -mvectorize-with-neon-quad\
                -funroll-loops

LOCAL_CFLAGS += -Wno-write-strings  #-Wall -Wno-error
LOCAL_LDLIBS    += -lm -llog -landroid -lgomp 
LOCAL_LDLIBS +=  -L$(SYSROOT)/usrb
LOCAL_LDLIBS += -L$(SYSROOT)/usr/lib

LOCAL_MODULE:= libgionee

LOCAL_C_INCLUDES:= \
    $(LOCAL_PATH)/include \
    $(TOP)/external/libgn_camera_feature/include

PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/bin/libimage_defog.so:system/lib/libimage_defog.so \
    $(LOCAL_PATH)/bin/libimage_defog.so:system/lib64/libimage_defog.so

LOCAL_LDFLAGS += \
    $(LOCAL_GIONEE_LIB_ROOT_PATH)/libimage_defog.so

include $(BUILD_STATIC_LIBRARY)
