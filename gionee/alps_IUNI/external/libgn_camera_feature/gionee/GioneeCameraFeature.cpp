/*************************************************************************************
 * 
 * Description:
 * 	Defines Gionee APIs for camera HAL.
 *
 * Author : wutangzhi
 * Email  : wutz@gionee.com
 * Date   : 2013-03-15
 *
 *************************************************************************************/
 
#define LOG_TAG "GioneeCameraFeature"

#include <android/log.h>
#include <cutils/properties.h>
#include "GioneeCameraFeature.h"

namespace android {
GioneeCameraFeature::GioneeCameraFeature()
    : mGNCameraFeature(GN_CAMERA_FEATURE_NONE)
    , mListener(NULL)
    , mFormat(GN_IMG_FORMAT_NONE)
    , mPreviewBufferSize(0)
    , mWidth(0)
    , mHeight(0)
    , mGioneeDefogShot(NULL)
    , mGioneeSceneDetection(NULL)
    , mDumpCnt(0)
{
	mGioneeDefogShot = new GioneeDefogShot();
	if (mGioneeDefogShot == NULL) {
		PRINTD("Failed to new GioneeDefog");
	}

	mGioneeSceneDetection = new GioneeSceneDetection();
	if (mGioneeSceneDetection == NULL) {
		PRINTD("Failed to new GioneeSceneDetection");
	}

    pthread_mutex_init(&mMutex, NULL);
}

GioneeCameraFeature::
~GioneeCameraFeature()
{
    pthread_mutex_destroy(&mMutex);
}

GioneeCameraFeature*
GioneeCameraFeature::
createInstance()
{
    return new GioneeCameraFeature();
}

void
GioneeCameraFeature::
destroyInstance()
{
    delete this;
}

int32
GioneeCameraFeature::
init()
{
    int32 res = 0;

	pthread_mutex_lock(&mMutex);
	
	if (mGioneeDefogShot != NULL) {
		res = mGioneeDefogShot->init();
		if (res != 0) {
			PRINTE("Failed to initialized mGioneeDefogShot [%ld]", res);
		}
	}

	if (mGioneeSceneDetection != NULL) {
		res = mGioneeSceneDetection->init();
		if (res != 0) {
			PRINTE("Failed to initialized mGioneeDefogShot [%ld]", res);
		}
	}

	pthread_mutex_unlock(&mMutex);
	
    return res;
}

void
GioneeCameraFeature::
deinit()
{
    pthread_mutex_lock(&mMutex);

	if (mGioneeDefogShot != NULL) {
		mGioneeDefogShot->deinit();
	}

	if (mGioneeSceneDetection != NULL) {
		mGioneeSceneDetection->deinit();
		mGioneeSceneDetection->setCameraListener(NULL);
	}

    mWidth = 0;
    mHeight = 0;
    mFormat = GN_IMG_FORMAT_NONE;
    mPreviewBufferSize = 0;
    mListener = NULL;
	
    pthread_mutex_unlock(&mMutex);
}

int32
GioneeCameraFeature::
initPreviewSize(int width, int height, GNImgFormat format)
{
    int32  res = 0;

	pthread_mutex_lock(&mMutex);

	mWidth = width;
	mHeight = height;
	mFormat = format;
	
	mPreviewData.i32Width	= width;
	mPreviewData.i32Height	= height;

	switch (format) {
		case GN_IMG_FORMAT_YV12:
			mPreviewData.u32PixelArrayFormat 	= IMG_PAF_YV12;
			mPreviewData.pi32Pitch[0] 			= width;
			mPreviewData.pi32Pitch[1] 			= width / 2;
			mPreviewData.pi32Pitch[2] 			= width / 2;
			mPreviewBufferSize 					= width * height * 3 / 2;
			break;
		case GN_IMG_FORMAT_YUV422:
			break;
		case GN_IMG_FORMAT_NV21:
			mPreviewData.u32PixelArrayFormat 	= IMG_PAF_NV21;
			mPreviewData.pi32Pitch[0] 			= width;
			mPreviewData.pi32Pitch[1] 			= width;
			mPreviewData.pi32Pitch[2] 			= 0;
			mPreviewBufferSize 					= width * height * 3 / 2;
			break;
		case GN_IMG_FORMAT_YUV420:
			break;
		default:
			mPreviewData.u32PixelArrayFormat 	= IMG_PAF_NV21;
			mPreviewData.pi32Pitch[0] 			= width;
			mPreviewData.pi32Pitch[1] 			= width;
			mPreviewData.pi32Pitch[2] 			= 0;
			mPreviewBufferSize 					= width * height * 3 / 2;
			break;
	}
	
	pthread_mutex_unlock(&mMutex);
	
    return 0;
}

int32
GioneeCameraFeature::
setCameraListener(GNCameraFeatureListener* listener)
{
    pthread_mutex_lock(&mMutex);

    mListener = listener;

	if (mGioneeSceneDetection != NULL) {
		mGioneeSceneDetection->setCameraListener(listener);
	}

    pthread_mutex_unlock(&mMutex);

    return 0;
}

int32
GioneeCameraFeature::
processPreview(void* inputBuffer, int size, int mask)
{
    int32  res = 0;
	int32 dumpFrameRef = 0;

	pthread_mutex_lock(&mMutex);

	if (inputBuffer == NULL || size == 0 || size < mPreviewBufferSize) {
		PRINTE("the inputBuffer is null or invalid.");
		pthread_mutex_unlock(&mMutex); 
		return -1;
	}

	if ((mGNCameraFeature & 0XFFFF) == 0) {
		pthread_mutex_unlock(&mMutex); 
		return res;
	}
	
	mPreviewData.ppu8Plane[0] = (uint8_t*)inputBuffer;

	switch (mFormat) {
		case GN_IMG_FORMAT_YV12:
			mPreviewData.ppu8Plane[1] = mPreviewData.ppu8Plane[0] + mPreviewData.i32Width * mPreviewData.i32Height;
			mPreviewData.ppu8Plane[2] = mPreviewData.ppu8Plane[1] + mPreviewData.i32Width * mPreviewData.i32Height / 4;
			break;
		case GN_IMG_FORMAT_YUV422:
			break;
		case GN_IMG_FORMAT_NV21:
			mPreviewData.ppu8Plane[1] = mPreviewData.ppu8Plane[0] + mPreviewData.i32Width * mPreviewData.i32Height;
			mPreviewData.ppu8Plane[2] = NULL;
			break;
		case GN_IMG_FORMAT_YUV420:
			break;
		default:
			mPreviewData.ppu8Plane[1] = mPreviewData.ppu8Plane[0] + mPreviewData.i32Width * mPreviewData.i32Height;
			mPreviewData.ppu8Plane[2] = NULL;
			break;
	}

	// dump input frame if needed.
	if (dumpFrame(&mPreviewData, size, GN_CAMERA_DUMP_FRM_PREVIEW_INPUT)) {
		dumpFrameRef ++;
	}

	if ((mGNCameraFeature & GN_CAMERA_FEATUER_SCENE_DETECTION) & mask) {
		res = processSceneDetection(&mPreviewData);
	}

	// dump output frame if needed.
	if (dumpFrame(&mPreviewData, size, GN_CAMERA_DUMP_FRM_PREVIEW_INPUT)) {
		dumpFrameRef ++;
	}

	if (dumpFrameRef) {
		mDumpCnt ++;
	}
	
	pthread_mutex_unlock(&mMutex); 
	
    return res;
}

int32
GioneeCameraFeature::
processRaw(void* inputBuffer, int size, int width, int height, GNImgFormat format, int mask)
{
    int32 res = 0;
	int yStride = 0;
    int yScanline = 0;
	int dumpFrameRef = 0;
	IMGOFFSCREEN imageData;

	if (inputBuffer == NULL || size == 0 || width == 0 || height == 0) {
		PRINTE("Invalid input buffer.");
		return -1;
	}

	pthread_mutex_lock(&mMutex);

	imageData.i32Width	= width;
	imageData.i32Height = height;

	if (ALIGN_FORMAT != ALIGN_TO_32) {
		yStride = ALIGN_TO_SIZE(width, ALIGN_FORMAT);
		yScanline = ALIGN_TO_SIZE(height, ALIGN_FORMAT);
	} else {
		yStride = width;
		yScanline = height;
	}

	switch (format) {
		case GN_IMG_FORMAT_YV12:
			imageData.u32PixelArrayFormat = IMG_PAF_YV12;
			imageData.pi32Pitch[0] = yStride;
			imageData.pi32Pitch[1] = yStride / 2;
			imageData.pi32Pitch[2] = yStride / 2;
			imageData.ppu8Plane[0] = (uint8_t*)inputBuffer;
			imageData.ppu8Plane[1] = imageData.ppu8Plane[0] + yStride * yScanline;
			imageData.ppu8Plane[2] = imageData.ppu8Plane[1] + yStride * yScanline / 4;
			break;
		case GN_IMG_FORMAT_YUYV:
			imageData.u32PixelArrayFormat = IMG_PAF_YUYV;
			imageData.pi32Pitch[0] = yStride * 2;
			imageData.pi32Pitch[1] = 0;
			imageData.pi32Pitch[2] = 0;
			imageData.ppu8Plane[0] = (uint8_t*)inputBuffer;
			imageData.ppu8Plane[1] = NULL;
			imageData.ppu8Plane[2] = NULL;
			break;
		case GN_IMG_FORMAT_NV21:
			imageData.u32PixelArrayFormat = IMG_PAF_NV21;
			imageData.pi32Pitch[0] = yStride;
			imageData.pi32Pitch[1] = yStride;
			imageData.pi32Pitch[2] = 0;
			imageData.ppu8Plane[0] = (uint8_t*)inputBuffer;
			imageData.ppu8Plane[1] = imageData.ppu8Plane[0] + yStride * yScanline;
			imageData.ppu8Plane[2] = NULL;
			break;
		default:
			imageData.u32PixelArrayFormat = IMG_PAF_YUYV;
			imageData.pi32Pitch[0] = yStride * 2;
			imageData.pi32Pitch[1] = 0;
			imageData.pi32Pitch[2] = 0;
			imageData.ppu8Plane[0] = (uint8_t*)inputBuffer;
			imageData.ppu8Plane[1] = NULL;
			imageData.ppu8Plane[2] = NULL;
			break;
	}

	// dump input yuv frame if needed.
	if (dumpFrame(&imageData, size, GN_CAMERA_DUMP_FRM_SNAPSHOT_INPUT)) {
		dumpFrameRef ++;
	}

	if ((mGNCameraFeature & GN_CAMERA_FEATURE_DEFOG_SHOT) & mask) {
		res = processDefogShot(&imageData);
		if (res != 0) {
			PRINTD("Failed to process defog");
		}
	}

	// dump output yuv frame if needed.
	if (dumpFrame(&imageData, size, GN_CAMERA_DUMP_FRM_SNAPSHOT_INPUT)) {
		dumpFrameRef ++;
	}

	if (dumpFrameRef) {
		mDumpCnt ++;
	}

	pthread_mutex_unlock(&mMutex);    

    return res;
}

int32
GioneeCameraFeature::
setDefogShot(GNDefogShot_t defogShotMode,GNImgFormat format)
{
	int32 res = 0;

	pthread_mutex_lock(&mMutex);
	
	if (defogShotMode == GN_DEFOG_SHOT_ON) {
		if (mGioneeDefogShot != NULL) {
			mGioneeDefogShot->enableDefogShot(format);
		}
		
		mGNCameraFeature |= GN_CAMERA_FEATURE_DEFOG_SHOT;
	} else {
		if (mGioneeDefogShot != NULL) {
			mGioneeDefogShot->disableDefogShot();
		}

		mGNCameraFeature &= ~GN_CAMERA_FEATURE_DEFOG_SHOT;
	}

	pthread_mutex_unlock(&mMutex);
	
	return res;
}

int32
GioneeCameraFeature::
setSceneDetection(GNSceneDetectionParam param)
{
	int32 res = 0;

	pthread_mutex_lock(&mMutex);
	
	if (param.sceneDetectionMode == GN_SCENE_DETECTION_ON) {
		if (mGioneeSceneDetection!= NULL) {
			mGioneeSceneDetection->enableSceneDetection(param);
		}
		
		mGNCameraFeature |= GN_CAMERA_FEATUER_SCENE_DETECTION;
	} else {
		if (mGioneeSceneDetection != NULL) {
			mGioneeSceneDetection->disableSceneDetection();
		}

		mGNCameraFeature &= ~GN_CAMERA_FEATUER_SCENE_DETECTION;
	}

	pthread_mutex_unlock(&mMutex);
	
	return res;
}


int32 
GioneeCameraFeature::
processDefogShot(PIMGOFFSCREEN imgSrc)
{
	int32 res = 0;

	if (mGioneeDefogShot != NULL) {
		res = mGioneeDefogShot->processDefogShot(imgSrc);
	}

	return res;
}

int32 
GioneeCameraFeature::
processSceneDetection(PIMGOFFSCREEN imgSrc)
{
	int32 res = 0;
	
	if (mGioneeSceneDetection != NULL) {
		res = mGioneeSceneDetection->processSceneDetection(imgSrc);
	}

	return res;
}

uint32_t
GioneeCameraFeature::
getPixelFormat(GNImgFormat format)
{
	uint32_t pixelFormat = 0;

	switch(format) {
		case GN_IMG_FORMAT_NV21:
			pixelFormat = IMG_PAF_NV21;
			break;
		case GN_IMG_FORMAT_YV12:
			pixelFormat = IMG_PAF_YV12;
			break;
		case GN_IMG_FORMAT_YUYV:
			pixelFormat = IMG_PAF_YUYV;
			break;
		default:
			break;
	}

	return pixelFormat;
}

bool 
GioneeCameraFeature::
dumpFrame(PIMGOFFSCREEN imageData, int size, int dump_type)
{
	bool ret = false;
	
    char value[PROPERTY_VALUE_MAX];
	property_get("persist.gionee.dumpbuffer", value, "0");
	int32_t enabled = atoi(value);

	if (enabled & GN_CAMERA_DUMP_FRM_MASK_ALL) {		
		if ((enabled & dump_type) && (imageData != NULL) 
			&& (imageData->ppu8Plane[0] != NULL) && size) {
			int width = 0;
			int height = 0;
			char fileStr[64] = {0};
			char timeStr[128] = {0};
			time_t timeCurr;
			struct tm *timeInfo = NULL;;
			
			time(&timeCurr);
			timeInfo = localtime(&timeCurr);
			strftime(timeStr, sizeof(timeStr),"/data/%Y%m%d%H%M%S", timeInfo);

			String8 filePath(timeStr);

			width = imageData->i32Width;
			height = imageData->i32Height;

			switch (dump_type) {
				case GN_CAMERA_DUMP_FRM_PREVIEW_INPUT:
					sprintf(fileStr, "p_in_%dx%d_%d.yuv", width, height, mDumpCnt);
					break;
				case GN_CAMERA_DUMP_FRM_PREVIEW_OUTPUT:
					sprintf(fileStr, "p_out_%dx%d_%d.yuv", width, height, mDumpCnt);
					break;
				case GN_CAMERA_DUMP_FRM_SNAPSHOT_INPUT:
					sprintf(fileStr, "s_in_%dx%d_%d.yuv", width, height, mDumpCnt);
					break;
				case GN_CAMERA_DUMP_FRM_SNAPSHOT_OUTPUT:
					sprintf(fileStr, "s_out_%dx%d_%d.yuv", width, height, mDumpCnt);
					break;
				case GN_CAMERA_DUMP_FRM_VIDEO_INPUT:
					sprintf(fileStr, "v_in_%dx%d_%d.yuv", width, height, mDumpCnt);
					break;
				case GN_CAMERA_DUMP_FRM_VIDEO_OUTPUT:
					sprintf(fileStr, "v_out_%dx%d_%d.yuv", width, height, mDumpCnt);
					break;
				default:
					break;
			}

			filePath.append(fileStr);

			FILE* fp = fopen(filePath.string(), "w");
			if (fp == NULL) {
				PRINTE("fail to open file to save img: %s", filePath.string());
				return false;
			}

			fwrite(imageData->ppu8Plane[0], 1, size , fp);
			
			fclose(fp);

			ret = true;
		}
	}
	
    return ret;
}

};
