#ifndef __ALGCOMMON_INCLUDE__
#define __ALGCOMMON_INCLUDE__

#include <jni.h>


#include <memory.h>

#include <stdlib.h>
#include <android/log.h>
#include <time.h>
#include <pthread.h>
#include <stddef.h>

#include <pthread.h>
#include <sched.h>
#include <stdio.h>
#include <stdlib.h>
//#include <omp.h>
#ifdef __ARM_NEON__
 #include <arm_neon.h>
#else
 #include <stdint.h>
 typedef float  float32_t;
 typedef double float64_t;
#endif
//#include <omp.h>
#undef _OMP_SURPPORT
//#define TYPICAL_THREAD_NUM 4
#define MAX_THREAD  64

#ifdef _OMP_SURPPORT
#include <omp.h>
#else
//#define TYPICAL_THREAD_NUM 4
#define MAX_THREAD  64
#endif
//using std::vector;
#define _DFGDEBUG
#ifndef MIN
#define MIN(a,b) (((a)<(b))?(a):(b))
#endif

#ifndef MAX
#define MAX(a,b) (((a)>(b))?(a):(b))
#endif

#ifndef ABS
#define ABS(a,b) (((a)>(b))?(a-b):(b-a))
#endif

#ifndef BYTE_NUM_REGD
#define BYTE_NUM_REGD  (8)
#endif

#ifndef BYTE_NUM_REGQ
#define BYTE_NUM_REGQ  (16)
#endif


#define CLIP(X,vmax,vmin) X = MAX( MIN((X),(vmax)),(vmin))


#ifndef TIMER2_INIT
#define TIMER2_INIT 	double timego2;\
						struct timeval start2;\
						struct timeval end2;
#endif



#ifndef TIMER2_START
#define TIMER2_START gettimeofday(&start2, NULL);
#endif

#ifndef TIMER2_END
#define TIMER2_END(STR) gettimeofday(&end2, NULL);\
		timego2 = ((end2.tv_sec * 1000000 + end2.tv_usec) - (start2.tv_sec * 1000000 + start2.tv_usec));\
		timego2/=1000000.0;\
		 __android_log_print(ANDROID_LOG_DEBUG,"ImageProc", "%s: %f seconds\r\n ",(STR),timego2)
#endif













#ifndef MYTIMER_INIT
#define MYTIMER_INIT 	double timego;\
						struct timeval start;\
						struct timeval end;
#endif

#ifndef MYTIMER_START
#define MYTIMER_START gettimeofday(&start, NULL);
#endif

#ifndef MYTIMER_END
#define MYTIMER_END(STR) gettimeofday(&end, NULL);\
		timego = ((end.tv_sec * 1000000 + end.tv_usec) - (start.tv_sec * 1000000 + start.tv_usec));\
		timego/=1000000.0;\
		 __android_log_print(ANDROID_LOG_DEBUG,"ImageProc", "%s: %f seconds\r\n ",(STR),timego)
#endif




#define LOG_I(TAG,fmt, args...) __android_log_print(ANDROID_LOG_INFO, TAG, fmt, ##args)
#define AINFO(TAG,fmt, args...) __android_log_print(ANDROID_LOG_INFO, TAG, fmt, ##args)

#define DEBUG(TAG,fmt, args...) __android_log_print(ANDROID_LOG_DEBUG, TAG, fmt, ##args)
#define ADEBUG(TAG,fmt, args...) __android_log_print(ANDROID_LOG_DEBUG, TAG, fmt, ##args)

#define AERROR(TAG,fmt, args...) __android_log_print( ANDROID_LOG_ERROR, TAG, fmt, ##args)
#define AWARN(TAG,fmt, args...) __android_log_print(ANDROID_LOG_WARN, TAG, fmt, ##args)
#define ASSERT(cond,TAG) ((cond)?(void)0:__android_log_assert("##cond", TAG, " "))
#define ASSERTMSG(cond, TAG,fmt, args...) ((cond)?(void)0:__android_log_assert("##cond", TAG, fmt, ##args))



// int8_t saveBMP(uint8_t* bgr888, uint32_t width,  uint32_t height,  uint8_t* PictureFileName);
#endif
