/*******************************************************
 *
 * Description: Define API for process thread.
 *
 * Author:	zhuangxiaojian
 * E-mail:	zhuangxj@gionee.com
 * Date:	2015-06-30
 *
 ******************************************************/

#include <utils/Errors.h>
#include <utils/Log.h>
#include "GNCameraCmdThread.h"

using namespace android;

namespace android {

GNCameraCmdThread::
GNCameraCmdThread() 
	: cmd_queue()
{
    cmd_pid = 0;
    cam_sem_init(&sync_sem, 0);
    cam_sem_init(&cmd_sem, 0);
}

GNCameraCmdThread::
~GNCameraCmdThread()
{
    cam_sem_destroy(&sync_sem);
    cam_sem_destroy(&cmd_sem);
}

int32_t
GNCameraCmdThread::
launch(void *(*start_routine)(void *), void* user_data)
{
    /* launch the thread */
    pthread_create(&cmd_pid,
                   NULL,
                   start_routine,
                   user_data);
    return NO_ERROR;
}

int32_t
GNCameraCmdThread::
sendCmd(camera_cmd_type_t cmd, uint8_t sync_cmd, uint8_t priority)
{
    camera_cmd_t *node = (camera_cmd_t *)malloc(sizeof(camera_cmd_t));
    if (NULL == node) {
        ALOGE("%s: No memory for camera_cmd_t", __func__);
        return NO_MEMORY;
    }
    memset(node, 0, sizeof(camera_cmd_t));
    node->cmd = cmd;

    if (priority) {
        cmd_queue.enqueueWithPriority((void *)node);
    } else {
        cmd_queue.enqueue((void *)node);
    }
    cam_sem_post(&cmd_sem);

    /* if is a sync call, need to wait until it returns */
    if (sync_cmd) {
        cam_sem_wait(&sync_sem);
    }
    return NO_ERROR;
}

camera_cmd_type_t
GNCameraCmdThread::
getCmd()
{
    camera_cmd_type_t cmd = GN_CAMERA_CMD_TYPE_NONE;
    camera_cmd_t *node = (camera_cmd_t *)cmd_queue.dequeue();
    if (NULL == node) {
        ALOGD("%s: No notify avail", __func__);
        return GN_CAMERA_CMD_TYPE_NONE;
    } else {
        cmd = node->cmd;
        free(node);
    }
    return cmd;
}

int32_t
GNCameraCmdThread::
exit()
{
    int32_t rc = NO_ERROR;

    if (cmd_pid == 0) {
        return rc;
    }

    rc = sendCmd(GN_CAMERA_CMD_TYPE_EXIT, 0, 1);
    if (NO_ERROR != rc) {
        ALOGE("%s: Error during exit, rc = %d", __func__, rc);
        return rc;
    }

    /* wait until cmd thread exits */
    if (pthread_join(cmd_pid, NULL) != 0) {
        ALOGD("%s: pthread dead already\n", __func__);
    }
    cmd_pid = 0;
    return rc;
}

}; // namespace android 
