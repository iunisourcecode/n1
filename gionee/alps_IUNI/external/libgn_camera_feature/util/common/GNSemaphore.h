/*******************************************************
 *
 * Description: Define API for process thread.
 *
 * Author:	zhuangxiaojian
 * E-mail:	zhuangxj@gionee.com
 * Date:	2015-06-30
 *
 ******************************************************/

#ifndef GN_CAMERA_SEMAPHORE_H
#define GN_CAMERA_SEMAPHORE_H

#ifdef __cplusplus
extern "C" {
#endif


typedef struct {
    int val;
    pthread_mutex_t mutex;
    pthread_cond_t cond;
} cam_semaphore_t;

static inline void cam_sem_init(cam_semaphore_t *s, int n)
{
    pthread_mutex_init(&(s->mutex), NULL);
    pthread_cond_init(&(s->cond), NULL);
    s->val = n;
}

static inline void cam_sem_post(cam_semaphore_t *s)
{
    pthread_mutex_lock(&(s->mutex));
    s->val++;
    pthread_cond_signal(&(s->cond));
    pthread_mutex_unlock(&(s->mutex));
}

static inline int cam_sem_wait(cam_semaphore_t *s)
{
    int rc = 0;
    pthread_mutex_lock(&(s->mutex));
    while (s->val == 0)
        rc = pthread_cond_wait(&(s->cond), &(s->mutex));
    s->val--;
    pthread_mutex_unlock(&(s->mutex));
    return rc;
}

static inline void cam_sem_destroy(cam_semaphore_t *s)
{
    pthread_mutex_destroy(&(s->mutex));
    pthread_cond_destroy(&(s->cond));
    s->val = 0;
}

#ifdef __cplusplus
}
#endif

#endif /* GN_CAMERA_SEMAPHORE_H */
