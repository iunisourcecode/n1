/*************************************************************************************
 * 
 * Description:
 * 	Defines Crunchfish APIs for camera HAL.
 *
 * Author : wutangzhi
 * Email  : wutz@gionee.com
 * Date   : 2013-05-08
 *
 *************************************************************************************/

#define LOG_TAG "VisidonSuperPhoto"

#include "VisidonSuperPhoto.h"

#include <cutils/properties.h>
#include <stdlib.h>
#include <cutils/log.h>
#include <time.h>

#include "GNCameraFeatureDbgTime.h"

//#define DEBUG_TIME

static int dump_count = 0;
    
namespace android {

VisidonSuperPhoto::VisidonSuperPhoto() 
	: mInitialized(false)
	, mScale(1.0)
 	, mImageNum(0)
{
	char colorFilterStrength[PROPERTY_VALUE_MAX] = {0};
	property_get("persist.filterStrength", colorFilterStrength, "3");
	mColorFilterStrength = atoi(colorFilterStrength);

	char denoiseStrength[PROPERTY_VALUE_MAX] = {0};
	property_get("persist.denoiseStrength", denoiseStrength, "2");
	mDenoiseStrength = atoi(denoiseStrength);

	char sharpen[PROPERTY_VALUE_MAX] = {0};
	property_get("persist.sharpenStrength", sharpen, "6");
	mSharpness = atoi(sharpen);

	char contrastStrength[PROPERTY_VALUE_MAX] = {0};
	property_get("persist.contrastStrength", contrastStrength, "2");
	mContrastStrength = atoi(contrastStrength);

	memset(mSrcImg, NULL, sizeof(mSrcImg));
	
}

VisidonSuperPhoto::
~VisidonSuperPhoto() 
{
	
}

int32
VisidonSuperPhoto::
init()
{
	int32 res = 0;

	mInitialized = false;

	return res;
}

void 
VisidonSuperPhoto::
deinit()
{
	disableSuperPhoto();
}

int32 
VisidonSuperPhoto::
enableSuperPhoto(float scale, GNSuperPhotoType_t type)
{
	int32 res = 0;

	mScale = scale;

	mSuperPhotoType = type;
	
	PRINTD("%s mSuperPhotoType = %d mScale = %f", __func__, mSuperPhotoType, mScale);
	
	mInitialized = true;

	return 0;
}

void
VisidonSuperPhoto::
disableSuperPhoto()
{
	for (int i = 0; i < MAX_INPUT_IMAGE_NUM - 1; i ++) {
		if (mSrcImg[i] != NULL) {
			free(mSrcImg[i]);
			mSrcImg[i] = NULL;
		}
	}

	mScale = 1.0;
	mInitialized = false;
}

int32 
VisidonSuperPhoto::
setExParameters(int32 type, void* param)
{
	switch(type) {
		case GN_TUNING_PARAMS_ISO:
			mIsoValue = *((int32*)param);
			break;
		//....
		default:
			break;
	}

	return 0;
}

int32
VisidonSuperPhoto::
processSuperPhoto(ImageParamInfo* imageData)
{
	int32 res = 0;
	int32 index = 0;
	void* engine = NULL;
	int32 outputWidth = 0;
	int32 outputHeight = 0;
	int32 srcMemSize = 0;
	GNCameraFeatureDbgTime dbgTime;
	VDSuperPhotoInitializeParams iparams = {0};
	VDSuperPhotoRuntimeParams rparams = {0};

	if (imageData == NULL || imageData->buffer== NULL || imageData->bufferSize== 0 || !mInitialized) {
		PRINTE("the inputBuffer is null or not Initialized [%d]", mInitialized);
		return -1;
	}

	PRINTE("[processImage]: mImageNum = %d", mImageNum);

	srcMemSize = getMemSize(imageData->width, imageData->height, (VDDatatype)imageData->pixelArrayFormat);

	PRINTD("%s srcMemSize = %d, bufferSize = %d", __func__, srcMemSize, imageData->bufferSize);
	
	if (mImageNum < MAX_INPUT_IMAGE_NUM - 1) {
		mSrcImg[mImageNum] = (unsigned char*)malloc(srcMemSize);
		memcpy(mSrcImg[mImageNum], imageData->buffer, srcMemSize);
	}
	
	mImageNum ++;

	if (mImageNum < MAX_INPUT_IMAGE_NUM) {
		return 0;
	}

	//initialize params
	iparams.cols = imageData->width; 
	iparams.rows = imageData->height; 
	iparams.type = (VDDatatype)imageData->pixelArrayFormat;
	iparams.extraPadCols = 0;
	iparams.extraPadRows = 0;
	iparams.totalFrames = MAX_INPUT_IMAGE_NUM - 1;

	if (mSuperPhotoType == GN_SUPER_PHOTO_TYPE_SCALE) {
		if (ALIGN_FORMAT != ALIGN_TO_64) {
			outputWidth = ALIGN_TO_SIZE((int)(imageData->width * mScale), ALIGN_TO_16);
			outputHeight = ALIGN_TO_SIZE((int)(imageData->height * mScale), ALIGN_TO_16);
		} else {
			outputWidth = imageData->width * mScale;
			outputHeight = imageData->height * mScale;
		}
		
		iparams.outputCols = outputWidth;
		iparams.outputRows = outputHeight;

		iparams.mode = VD_SCALE;
		iparams.zoom = (float)iparams.outputCols / iparams.cols;
	} else {
		iparams.mode = VD_ZOOM;
		iparams.zoom = mScale;

		iparams.outputCols = -1;
		iparams.outputRows = -1;
	}
	
	//res = VDInitializeSuperPhoto(iparams, &engine);
	res = VDInitializeSuperPhotoWithCores(iparams, PROCESSOR_CORE_NUM, &engine);
	if (res != 0) {
		PRINTD("%s Failed to init super photo", __func__);
		goto EXIT;
	}
	
	for (index = 0; index < MAX_INPUT_IMAGE_NUM - 1; index ++) {
		res = VDSuperPhoto_AddFrame(mSrcImg[index], engine);
		if (res != VD_OK) {
			PRINTE( "%s VDSuperPhoto_AddFrame(%d) failed!\n", __func__, index);
			goto UNINIT;
		}
	}
	
	res = VDSuperPhoto_AddOutputBuffer((unsigned char*)imageData->buffer, engine);
	if (res != VD_OK) {
		PRINTD("%s Failed to add output buffer", __func__);
		goto UNINIT;
	}
	PRINTD("%s VDSuperPhoto_AddOutputBuffer finish", __func__);

	rparams.noiseReductionStrength = mDenoiseStrength;
	rparams.colorFilterStrength = mColorFilterStrength;
	rparams.sharpen = mSharpness;
	rparams.contrastStretching = mContrastStrength;
	rparams.gain = -1;
	rparams.isoValue = -1;//mIsoValue;
	
	rparams.dynamicRangeBoost = 0;
	rparams.lowIntensityBoost = 0;
	rparams.colorBoost = 0;
	rparams.deghostingStrength = 60;

	PRINTD("%s noiseReductionStrength %d colorFilterStrength %d sharpen %d contrastStretching %d, isoValue = %d", 
		__func__, rparams.noiseReductionStrength, rparams.colorFilterStrength,
		rparams.sharpen, rparams.contrastStretching, rparams.isoValue);
	
	res = VDSuperPhoto_Process(rparams, engine);
	if (res != VD_OK) {
		PRINTD("%s Failed to process frame", __func__);
	}
	
	PRINTD("%s process end", __func__);

	if (iparams.mode == VD_SCALE) {
		imageData->width = iparams.outputCols;
		imageData->height = iparams.outputRows;
	} else {
		imageData->width = imageData->width;
		imageData->height = imageData->height;
	}

UNINIT:
	VDReleaseSuperPhoto(&engine);

EXIT:
	for (index = 0; index < MAX_INPUT_IMAGE_NUM - 1; index ++) {
		if (mSrcImg[index] != NULL) {
			free(mSrcImg[index]);
			mSrcImg[index] = NULL;
		}
	}
	
	mImageNum = 0;
	
	dbgTime.print(__func__);

	return res;
}

int32
VisidonSuperPhoto::
getBurstCnt()
{
	return MAX_INPUT_IMAGE_NUM - 1;
}

int32
VisidonSuperPhoto::
getMemSize(int width, int height, VDDatatype pixelArrayFormat)
{
	int memSize = 0;

	switch (pixelArrayFormat) {
		case VD_YUV_NV21:
			memSize = height * width * 3 / 2;
			break;
		case VD_YUV_YUYV:
			memSize = height * width * 2;
			break;
		default:	
			//VD_YUV_YUYV
			memSize = height * width * 2;
			break;
	}

	return memSize;
}

};
