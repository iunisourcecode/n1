/** @file VDRTSuperPhotoAPIExtension.h
@brief Contains API extension for Visidon Real-time Super Photo algorithm


Copyright (C) 2014 Visidon Oy
This file is part of Visidon RTSuperPhoto SDK

*/
#ifndef VDRTSUPERPHOTOAPIEXTENSION_H
#define VDRTSUPERPHOTOAPIEXTENSION_H

#include "VDRTSuperPhotoAPI.h"

#ifdef __cplusplus
extern "C" {
#endif

	/**
	* Initialize real-time superphoto engine with user defined number of input / output buffers to be used in the rendering pipeline.
	* @param cols Input image width in pixels
	* @param rows Input image height in pixels	
	* @param inputBuffers Number of input buffers to be used in the pipeline. Possible values are 1 (no buffering), 2, and 3. Good default value is 2.
	* @param outputBuffers Number of output buffers to be used in the pipeline. Note that output buffering is only supported with OpenGL ES 3.0 version. Possible values are 1 (no buffering), 2, and 3. Good default value is 2.
	* @param blockingMode Set engine rendering mode to non-blocking / blocking.  In non-blocking mode, it is possible to skip frames if engine is not able to process all inputs. In blocking mode, all inputs are rendered, and VDRTSuperPhoto_ProcessFrame may block until the result is valid. Possible values are integer values 0 or 1. Good default value is 0 (=non-blocking).
	* @param engine Engine instance pointer
	* @return Error code indicating if initialization was succeed (VDRTSUPERPHOTO_OK) or failed (VDRTSUPERPHOTO_NOK)
	**/
	VDRTSuperPhotoErrorCode VDRTSuperPhoto_InitializeWithBufferSize(int cols, int rows, int inputBuffers, int outputBuffers, int blockingMode, void **engine);

	/**
	* Initialize real-time superphoto engine with external display where to render output and with user defined number of input / output buffers to be used in the rendering pipeline.
	* @param cols Input image width in pixels
	* @param rows Input image height in pixels
	* @param window Output window where to render output
	* @param inputBuffers Number of input buffers to be used in the pipeline. Possible values are 1 (no buffering), 2, and 3. Good default value is 2.
	* @param outputBuffers Number of output buffers to be used in the pipeline. Note that output buffering is only supported with OpenGL ES 3.0 version. Possible values are 1 (no buffering), 2, and 3. Good default value is 2.
	* @param blockingMode Set engine rendering mode to non-blocking / blocking.  In non-blocking mode, it is possible to skip frames if engine is not able to process all inputs. In blocking mode, all inputs are rendered, and VDRTSuperPhoto_ProcessFrame may block until the result is valid. Possible values are integer values 0 or 1. Good default value is 0 (=non-blocking).
	* @param engine Engine instance pointer
	* @return Error code indicating if initialization was succeed (VDRTSUPERPHOTO_OK) or failed (VDRTSUPERPHOTO_NOK)
	**/
	VDRTSuperPhotoErrorCode VDRTSuperPhoto_InitializeWithWindowAndWithBufferSize(int cols, int rows, void *window, int inputBuffers, int outputBuffers, int blockingMode, void **engine);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif // VDRTSUPERPHOTOAPI_H
