/*************************************************************************************
 * 
 * Description:
 * 	Defines Crunchfish APIs for camera HAL.
 *
 * Author : wutangzhi
 * Email  : wutz@gionee.com
 * Date   : 2013-05-08
 *
 *************************************************************************************/
 
#ifndef ANDROID_HDR_CAMERA_FEATURE_H
#define ANDROID_HDR_CAMERA_FEATURE_H

#include <pthread.h>
#include <GNCameraFeatureBase.h>
#include <stdio.h>

extern "C"
{
#include "VDSingleShotHDRAPI.h"
}

namespace android { 

class VisidonSingleHdr : public GNCameraFeatureBase {
public:
    VisidonSingleHdr();
	~VisidonSingleHdr();
	
    static VisidonSingleHdr* createInstance();
    virtual void destroyInstance();
	
	virtual int32 init();
    virtual void  deinit();
    virtual int32 enableSingleHDR(int width, int height, GNImgFormat format);
	virtual void disableSingleHDR();
	virtual int32 processSingleHDR(ImageParamInfo* imageData, bool isPreview);
	
private:
    virtual int32 processRTStream(ImageParamInfo* stream);
	virtual int32 processImage(ImageParamInfo* imageData);

private:
	void* mEngine;
	int mWidth;
	int mHeight;
	int mIntensityLevel;
	int mContrastLevel;
	int mDenoiseStrength;
	bool mInitialized;
};
};
#endif /* ANDROID_TEST_CAMERA_FEATURE_H */
