package android.content.res;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.DisplayMetrics;

public class ThemeManager {
	static final String CTS_RES_PATH = "/system/app/Aurora_Hook/Aurora_Hook.apk";

	static final String CTS_RES_PKG_NAME = "com.aurora.apihook";

	private Resources mOriginalRes;

	private IThemeManager mService;

	public ThemeManager(Context ctx) {
		// TODO Auto-generated constructor stub
		mService = IThemeManager.Stub.asInterface(ServiceManager
				.getService(Context.THEME_MANAGER));
	}

	public boolean getBooleanConfig(String key, boolean defaultValue) {
		if(mService != null){
			try {
				return mService.getBooleanConfig(key, defaultValue);
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return defaultValue;
	}

	public int getIntConfig(String key, int defaultValue) {
		if(mService != null){
			try {
				return mService.getIntConfig(key, defaultValue);
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return defaultValue;
	}

	public float getFloatConfig(String key, float defaultValue) {
		if(mService != null){
			try {
				return mService.getFloatConfig(key, defaultValue);
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return defaultValue;
	}

	public double getDoubleConfig(String key, double defaultValue) {
		if(mService != null){
			try {
				return mService.getDoubleConfig(key, defaultValue);
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return defaultValue;
	}

	public ThemeConfig getThemeConfig(ThemeConfig oldConfig) {
		if(mService != null){
			try {
				return mService.getThemeConfig(oldConfig);
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return oldConfig;
	}

}
