package com.lbe.bootstrap;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ProviderInfo;
import android.database.Cursor;
import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable;
import android.text.TextUtils;

import com.lbe.security.utility.FileUtils;
import com.lbe.security.utility.IOUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;

import dalvik.system.PathClassLoader;

/**
 * Created by zhangyong on 15/9/5.
 */
public class BootstrapContentProvider extends ContentProvider {

    public static final String REGISTER_HIPS_PACKAGE = "register";
    public static final String AUTHORITY = "com.lbe.bootstrap";

    private static final String PREF_NAME = "bootstrap";
    private static final String KEY_EXTERNAL_LOADER_PACKAGE = "loader_package";
    private static final String KEY_HIPS_VERSION = "hips_version";

    private static final String HIPS_LOADER_PACKAGE = "secloader2.package";
    private static final String HIPS_BOOTSTRAP_LOADER_PACKAGE = "loader";
    private static final String HIPS_BOOTSTRAP_API_KEY = "api_key";
    private static final String HIPS_BOOTSTRAP_VERSION = "version";
    private static final String HIPS_BOOTSTRAP_FILE_NAMES = "files";
    private static final String HIPS_BOOTSTRAP_FDS = "fds";
    private static final String HIPS_BOOTSTRAP_UPDATED = "updated";
    private static final String HIPS_RESULT = "result";
    private static final String HIPS_MESSAGE = "message";

    private static final int BOOTSTRAP_OCCUPIED = -3;
    private static final int BOOTSTRAP_CORRUPTED = -4;

    private static final String SERVICE_JAR = "service.jar";

    private SharedPreferences preference;
    private ContentProvider delegate;
    private AuthService authService;
    private File workingDir;

    private Bundle cachedResult;

    @Override
    public boolean onCreate() {
        this.preference = getContext().getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        this.workingDir = getContext().getDir(PREF_NAME, Context.MODE_PRIVATE);
        this.authService = new AuthService();

        checkExternalLoader();
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        return null;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public Bundle call(String method, String arg, Bundle extras) {
        Bundle result = null;
        if (TextUtils.equals(REGISTER_HIPS_PACKAGE, method)) {
            result = registerHIPSPackages(extras);
        } else if (delegate != null && authService.validateCaller()) {
            result = delegate.call(method, arg, extras);
        }

        return result;
    }

    private Bundle registerHIPSPackages(Bundle parameters) {
        if (cachedResult == null) {
            cachedResult = doRegisterHIPSPackages(parameters);
        }
        return cachedResult;
    }

    private Bundle doRegisterHIPSPackages(Bundle parameters) {
        if (parameters == null) {
            return null;
        }

        if (!validateCaller(parameters.getString(HIPS_BOOTSTRAP_API_KEY))) {
            return null;
        }

        String loaderPackage = parameters.getString(HIPS_BOOTSTRAP_LOADER_PACKAGE);
        ApplicationInfo loaderPackageInfo = getApplicationInfo(loaderPackage);
        if (loaderPackageInfo == null || loaderPackageInfo.uid != Binder.getCallingUid()) {
            return null;
        }

        boolean modified = false;

        String previousLoaderPackage = preference.getString(KEY_EXTERNAL_LOADER_PACKAGE, null);

        if (!TextUtils.equals(loaderPackage, previousLoaderPackage)) {
            if (getApplicationInfo(previousLoaderPackage) != null) {
                Bundle result = new Bundle();
                result.putInt(HIPS_RESULT, BOOTSTRAP_OCCUPIED);
                result.putString(HIPS_LOADER_PACKAGE, previousLoaderPackage);
                return result;
            } else {
                modified = true;
            }
        }

        int version = parameters.getInt(HIPS_BOOTSTRAP_VERSION);
        int previousVersion = preference.getInt(KEY_HIPS_VERSION, -1);

        if (version != previousVersion) {
            if (!extractPackages(parameters)) {
                Bundle result = new Bundle();
                result.putInt(HIPS_RESULT, BOOTSTRAP_CORRUPTED);
                return result;
            }
            parameters.putBoolean(HIPS_BOOTSTRAP_UPDATED, true);
            modified = true;
        }

        if (modified) {
            SharedPreferences.Editor editor = preference.edit();
            editor.putInt(KEY_HIPS_VERSION, version);
            if (!isSystemApp(loaderPackageInfo)) {
                editor.putString(KEY_EXTERNAL_LOADER_PACKAGE, loaderPackage);
            }
            editor.commit();
        }

        Bundle result = loadServicePackage();
        if (delegate != null) {
            delegate.call(REGISTER_HIPS_PACKAGE, null, parameters);
        }

        return result;
    }

    private Bundle loadServicePackage() {
        try {
            PackageInfo packageInfo = getContext().getPackageManager().getPackageArchiveInfo(new File(workingDir, SERVICE_JAR).getAbsolutePath(), PackageManager.GET_PROVIDERS);
            ProviderInfo providerInfo = null;
            for (ProviderInfo pi : packageInfo.providers) {
                if (AUTHORITY.equals(pi.authority)) {
                    providerInfo = pi;
                    break;
                }
            }

            ClassLoader classLoader = new PathClassLoader(new File(workingDir, SERVICE_JAR).getAbsolutePath(), workingDir.getAbsolutePath(), getContext().getClassLoader());
            delegate = (ContentProvider) classLoader.loadClass(providerInfo.name).newInstance();
            delegate.attachInfo(getContext(), providerInfo);

            Bundle result = new Bundle();
            result.putInt(HIPS_RESULT, 0);
            return result;
        } catch (Throwable e) {
            Bundle result = new Bundle();
            result.putInt(HIPS_RESULT, BOOTSTRAP_CORRUPTED);
            result.putString(HIPS_MESSAGE, collectStacktrace(e));
            return result;
        }
    }

    private boolean validateCaller(String apiKey) {
        if (authService.validateCaller()) {
            return true;
        }

        if (apiKey != null) {
            return authService.registerCaller(apiKey);
        } else {
            return false;
        }
    }

    private void checkExternalLoader() {
        String extLoader = preference.getString(KEY_EXTERNAL_LOADER_PACKAGE, null);
        if (TextUtils.isEmpty(extLoader)) {
            return;
        }

        boolean exist = true;

        try {
            getContext().getPackageManager().getPackageInfo(extLoader, 0);
        } catch (Exception e) {
            exist = false;
        }

        if (!exist) {
            preference.edit().remove(KEY_EXTERNAL_LOADER_PACKAGE).commit();
        }
    }

    private String collectStacktrace(Throwable e) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        PrintStream print = new PrintStream(outputStream);
        e.printStackTrace(print);

        return new String(outputStream.toByteArray());
    }

    private ApplicationInfo getApplicationInfo(String packageName) {
        try {
            return getContext().getPackageManager().getApplicationInfo(packageName, 0);
        } catch (Exception e) {
            return null;
        }
    }

    private boolean isSystemApp(ApplicationInfo ai) {
        return (ai.flags & (ApplicationInfo.FLAG_SYSTEM | ApplicationInfo.FLAG_UPDATED_SYSTEM_APP)) != 0;
    }

    private boolean extractPackages(Bundle parameters) {
        String[] fileNames = parameters.getStringArray(HIPS_BOOTSTRAP_FILE_NAMES);
        Parcelable[] fds = parameters.getParcelableArray(HIPS_BOOTSTRAP_FDS);

        if (fileNames == null || fds == null || fileNames.length != fds.length) {
            return false;
        }

        File tempDir = getContext().getDir(PREF_NAME + "_tmp", Context.MODE_PRIVATE);
        try {
            for (int i = 0; i < fileNames.length; i++) {
                ParcelFileDescriptor fd = (ParcelFileDescriptor) fds[i];
                File file = new File(tempDir, fileNames[i]);
                InputStream in = new ParcelFileDescriptor.AutoCloseInputStream(fd);
                OutputStream out = new FileOutputStream(file);
                IOUtils.copyLarge(in, out);
                out.close();
                in.close();
                FileUtils.setPermissions(file, 00755);
            }

            for (int i=0; i < fileNames.length; i++) {
                File oldFile = new File(workingDir, fileNames[i]);
                File file = new File(tempDir, fileNames[i]);
                oldFile.delete();
                file.renameTo(oldFile);
            }

            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            for (Parcelable fd : fds) {
                try {
                    ((ParcelFileDescriptor) fd).close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            tempDir.delete();
        }
    }
}
