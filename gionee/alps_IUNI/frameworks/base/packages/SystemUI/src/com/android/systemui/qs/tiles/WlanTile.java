/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.systemui.qs.tiles;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.WifiManager;
import android.provider.Settings;
import android.provider.Settings.Global;
import android.util.Log;

import com.android.systemui.R;
import com.android.systemui.qs.GlobalSetting;
import com.android.systemui.qs.QSTile;
import com.android.systemui.qs.QSTile.ResourceIcon;

/*
 * Author:tymy
 * Date:20150911
 * Function:Quick settings tile: WlanTile
 */

public class WlanTile extends QSTile<QSTile.BooleanState> {

    private static final String TAG = "WlanTile";
    private static final boolean DEBUG = true;
			
    private final WifiManager mWifiManager;  
    private boolean mWifiOn;
    private boolean mEnable=true;  
    
    public WlanTile(Host host) {
        super(host);
        mWifiManager= (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);     
    }

    @Override
    protected BooleanState newTileState() {
        return new BooleanState();
    }

    @Override
    public void handleClick() {   	    	
    	mWifiOn=!mWifiOn;  
    	int wifiApState = mWifiManager.getWifiApState();
    	if (mWifiOn && ((wifiApState == WifiManager.WIFI_AP_STATE_ENABLING) ||
                (wifiApState == WifiManager.WIFI_AP_STATE_ENABLED))) {
            mWifiManager.setWifiApEnabled(null, false);
        }
    	mWifiManager.setWifiEnabled(mWifiOn);	   	
    }

    @Override
    public void handleLongClick() {
    	 Intent intent =  new Intent(Settings.ACTION_WIFI_SETTINGS);  
    	 intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
    	 mHost.startSettingsActivity(intent);
    }

    @Override
    protected void handleUpdateState(BooleanState state, Object arg) {
        state.visible = true;
        state.label = mContext.getString(R.string.quick_settings_wifi_label);
        state.enable = mEnable;
        if (mWifiOn) {
        	//state.color=mContext.getResources().getColor(R.color.qs_title_color_select);
        	state.color=mContext.getResources().getColor(R.color.aurora_toolbar_color_enable);
        	state.icon = ResourceIcon.get(R.drawable.aurora_toolbar_wifi_enable_svg);      	
        } else {
        	//state.color=mContext.getResources().getColor(R.color.qs_title_color_normal);
        	state.color=mContext.getResources().getColor(R.color.aurora_toolbar_color_disable);
        	state.icon = ResourceIcon.get(R.drawable.aurora_toolbar_wifi_disable_svg);
        }       
    }

    @Override
    protected String composeChangeAnnouncement() {
        if (mWifiOn) {
            return mContext.getString(R.string.accessibility_quick_settings_wifi_changed_on);
        } else {
            return mContext.getString(R.string.accessibility_quick_settings_wifi_changed_off);
        }   	
    }

    public void setListening(boolean listening) {
    	final IntentFilter filter = new IntentFilter();
        filter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);              
        mContext.registerReceiver(mReceiver, filter);
    }

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (WifiManager.WIFI_STATE_CHANGED_ACTION.equals(intent.getAction())) {           	
            	handleWifiStateChanged(intent.getIntExtra(WifiManager.EXTRA_WIFI_STATE, WifiManager.WIFI_STATE_UNKNOWN));           	
                refreshState();
            }
        }
    };
    
    private void handleWifiStateChanged(int state) {
        switch (state) {
            case WifiManager.WIFI_STATE_ENABLING:
            	mEnable=false;
                break;
            case WifiManager.WIFI_STATE_ENABLED:
            	mWifiOn=true;
            	mEnable=true;
                break;
            case WifiManager.WIFI_STATE_DISABLING:
            	mEnable=false;
                break;
            case WifiManager.WIFI_STATE_DISABLED:
            	mWifiOn=false;
            	mEnable=true;
                break;
            default:
            	mEnable=false;
        }
    }
    
}
