package com.android.systemui.statusbar.aurora;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.service.notification.StatusBarNotification;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import com.android.systemui.R;
import com.android.systemui.statusbar.NotificationData;
import com.android.systemui.statusbar.NotificationData.Entry;
import android.graphics.ColorFilter;
import android.graphics.PorterDuffColorFilter;
import android.graphics.PorterDuff;
import com.aurora.utils.Utils2Icon;
public class NotificationTemplateModifier {
	private static final String TAG = "NotificationTemplateModifier";
	private static NotificationTemplateModifier mInstance = null;
	private static Object mLock = new Object();
	private static int mSystemTemplateId = 0;
	private static int mLocalTemplateId = 0;
	private static int mGutsTemplateId = 0;
	private static int mIconId_Local = 0 ;
	private static int  mIconId_System= 0 ;
	private static int mIconId_Guts =0;
	private static Utils2Icon mUtils2Icon;
	private LruMemoryCache iconLruMemoryCache = null;
	 
	private NotificationTemplateModifier(Context context){
		init(context);
	}
	public Context mContext;
	public void setContext(Context context){
		mContext = context;
	}
	private void init(Context context) {
		mIconId_System = android.R.id.icon;
		mIconId_Guts = android.R.id.icon;
		mIconId_Local = R.id.icon;
		mSystemTemplateId = com.android.internal.R.id.status_bar_latest_event_content;
		mLocalTemplateId = R.id.status_bar_latest_event_content;
		mGutsTemplateId = R.id.notification_guts;
		
	}
	public static NotificationTemplateModifier getInstance(Context context){
		synchronized (mLock) {
			if(mInstance==null){
				mUtils2Icon = Utils2Icon.getInstance(context);
				mInstance = new NotificationTemplateModifier(context);
			}
		}
		return mInstance;
	}
	
	public void modify(NotificationData.Entry entry,StatusBarNotification sbn,View view){
		Long time = System.currentTimeMillis();
		Log.i(TAG,"sbn package  = "+sbn.getPackageName());
		if(view == null)return;
		if(needSyncToLauncher(sbn)){
			int type = getTemplateType(view);
			switch (type) {
			case 0:
				modifySystemTemplate(entry,sbn,view);
				break;
			case 1:
				modifyLocalTemplate(entry,sbn,view);
				break;
			case 2:
				modifyGutsTemplate(entry,sbn,view);
				break;
			}
		}else{
			modifyForOthers(entry,sbn,view);
		}
		Log.i(TAG,"modify time = "+(System.currentTimeMillis()-time));
	}
	private PorterDuffColorFilter mColorFilter = new PorterDuffColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
	private void modifyForOthers(Entry entry, StatusBarNotification sbn,
			View view) {
		ImageView icon = (ImageView)view.findViewById(mIconId_System);
		if(icon!=null){
			LayoutParams lp = icon.getLayoutParams();
			lp.width =getDimensionPixelSize(R.dimen.icon_system_width);
			lp.height = getDimensionPixelSize(R.dimen.icon_system_height);
			icon.setLayoutParams(lp);
			icon.setBackground(mContext.getResources().getDrawable(R.drawable.aurora_notification_template_icon_background));
			Drawable d = icon.getDrawable();
			if(d!=null){
				d.setColorFilter(mColorFilter);
			}
		}
	}
	private void modifyGutsTemplate(Entry entry, StatusBarNotification sbn,
			View view) {
		ImageView icon = (ImageView)view.findViewById(mIconId_Guts);
		if(icon!=null){
			icon.setImageDrawable(null);
		}
		if(icon!=null){
			Drawable d = getDrawable(sbn, Utils2Icon.INTER_SHADOW);//mUtils2Icon.getIconDrawable(sbn.getPackageName(), Utils2Icon.INTER_SHADOW);
			if(d!=null){
				icon.setBackground(d);
			}
		}	
	}
	
	private void modifyLocalTemplate(Entry entry, StatusBarNotification sbn,
			View view) {
		ImageView icon = (ImageView)view.findViewById(mIconId_Local);
		if(icon!=null){
			icon.setImageDrawable(null);
		}
		if(icon!=null){
			LayoutParams lp = (LayoutParams) icon.getLayoutParams();
			lp.width =getDimensionPixelSize(R.dimen.icon_system_width);
			lp.height = getDimensionPixelSize(R.dimen.icon_system_height);
			icon.setLayoutParams(lp);
			Drawable d = getDrawable(sbn, Utils2Icon.INTER_SHADOW);//mUtils2Icon.getIconDrawable(sbn.getPackageName(), Utils2Icon.INTER_SHADOW);
			if(d!=null){
				icon.setBackground(d);
			}
		}
	}
	private void modifySystemTemplate(Entry entry, StatusBarNotification sbn,
			View view) {
		ImageView icon = (ImageView)view.findViewById(mIconId_System);
		if(icon!=null){
			icon.setImageDrawable(null);
		}
		if(icon!=null){
			LayoutParams lp = (LayoutParams) icon.getLayoutParams();
			lp.width =getDimensionPixelSize(R.dimen.icon_system_width);
			lp.height = getDimensionPixelSize(R.dimen.icon_system_height);
			icon.setLayoutParams(lp);
			Drawable d = getDrawable(sbn, Utils2Icon.INTER_SHADOW);//mUtils2Icon.getIconDrawable(sbn.getPackageName(), Utils2Icon.INTER_SHADOW);
			if(d!=null){
				icon.setBackground(d);
			}
		}
	}
	private int getTemplateType(View view) {
		int type = -1;
		if(view.getId() == mSystemTemplateId){
			type = 0;
		}else if(view.getId() == mLocalTemplateId){
			type = 1;
		}else if(view.getId() == mGutsTemplateId){
			type = 2;
		}
		Log.i(TAG,"getTemplateType:"+type);
		return type;
	}
	private boolean needSyncToLauncher(StatusBarNotification sbn) {
		if((sbn.getPackageName().equals("android") || sbn.getPackageName().equals("com.android.systemui"))){
			return false;
		}
		return true;
	}
	
	private Drawable getDrawable(String key , int iconMode){
		Drawable d = null;
		if(iconLruMemoryCache == null){
			iconLruMemoryCache = LruMemoryCache.getInstance();
		}
		
		if(iconLruMemoryCache!=null){
			d = iconLruMemoryCache.getDrawable(key);
		}
		
		if(d == null){
			d = mUtils2Icon.getIconDrawable(key, iconMode);
			if(iconLruMemoryCache!=null&&d!=null){
				Log.i("Gicons","add "+key+"   ,   icon");
				iconLruMemoryCache.addToCache(key, d);
			}
		}
		return d;
	}
	//add by luolaigang for wechat icon begin
	private Drawable getDrawable(StatusBarNotification statusBarNotification , int iconMode){
		String key = statusBarNotification.getNotification().isClone()?"com.tencent.mm.clone":statusBarNotification.getPackageName();
		return getDrawable(key, iconMode);
	}
	//add by luolaigang for wechat icon end

	/**M:Hazel start to add begin*/
	public int getDimensionPixelSize(int id ){
		return mContext.getResources().getDimensionPixelSize(id)	;
	}
	/**M:Hazel start to add end*/
}
