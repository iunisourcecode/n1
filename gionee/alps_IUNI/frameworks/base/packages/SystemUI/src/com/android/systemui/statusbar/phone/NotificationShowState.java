package com.android.systemui.statusbar.phone;

public class NotificationShowState {
	/** M: Hazel add for define notification showing state begin */
	
	/**showing mode*/
	public static final int SHOW = 1;

	/**hiding mode*/
	public static final int HIDE = 2;
	
	/**allow private notification*/
	public static final int ALLOW_PRIVATE = 3;
	
	/** M: Hazel add for define notification showing state end */
}
