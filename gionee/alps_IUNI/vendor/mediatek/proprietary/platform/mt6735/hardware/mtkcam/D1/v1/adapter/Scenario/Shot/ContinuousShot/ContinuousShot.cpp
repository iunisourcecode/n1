/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#define LOG_TAG "MtkCam/Shot"
//
#include <mtkcam/Log.h>
#include <mtkcam/common.h>
//
#include <mtkcam/hwutils/CameraProfile.h>
using namespace CPTool;
//
#include <mtkcam/featureio/IHal3A.h>
using namespace NS3A;
//
#include <mtkcam/hal/IHalSensor.h>
//
#include <mtkcam/camshot/ICamShot.h>
#include <mtkcam/camshot/IMultiShot.h>
//
#include <mtkcam/exif/IBaseCamExif.h>
//
#include <Shot/IShot.h>
//
#include "ImpShot.h"
#include "ContinuousShot.h"
//
using namespace android;
using namespace NSShot;

/******************************************************************************
 *
 ******************************************************************************/
#define MY_LOGV(fmt, arg...)        CAM_LOGV("(%d)(%s)[%s] "fmt, ::gettid(), getShotName(), __FUNCTION__, ##arg)
#define MY_LOGD(fmt, arg...)        CAM_LOGD("(%d)(%s)[%s] "fmt, ::gettid(), getShotName(), __FUNCTION__, ##arg)
#define MY_LOGI(fmt, arg...)        CAM_LOGI("(%d)(%s)[%s] "fmt, ::gettid(), getShotName(), __FUNCTION__, ##arg)
#define MY_LOGW(fmt, arg...)        CAM_LOGW("(%d)(%s)[%s] "fmt, ::gettid(), getShotName(), __FUNCTION__, ##arg)
#define MY_LOGE(fmt, arg...)        CAM_LOGE("(%d)(%s)[%s] "fmt, ::gettid(), getShotName(), __FUNCTION__, ##arg)
#define MY_LOGA(fmt, arg...)        CAM_LOGA("(%d)(%s)[%s] "fmt, ::gettid(), getShotName(), __FUNCTION__, ##arg)
#define MY_LOGF(fmt, arg...)        CAM_LOGF("(%d)(%s)[%s] "fmt, ::gettid(), getShotName(), __FUNCTION__, ##arg)
//
#define MY_LOGV_IF(cond, ...)       do { if ( (cond) ) { MY_LOGV(__VA_ARGS__); } }while(0)
#define MY_LOGD_IF(cond, ...)       do { if ( (cond) ) { MY_LOGD(__VA_ARGS__); } }while(0)
#define MY_LOGI_IF(cond, ...)       do { if ( (cond) ) { MY_LOGI(__VA_ARGS__); } }while(0)
#define MY_LOGW_IF(cond, ...)       do { if ( (cond) ) { MY_LOGW(__VA_ARGS__); } }while(0)
#define MY_LOGE_IF(cond, ...)       do { if ( (cond) ) { MY_LOGE(__VA_ARGS__); } }while(0)
#define MY_LOGA_IF(cond, ...)       do { if ( (cond) ) { MY_LOGA(__VA_ARGS__); } }while(0)
#define MY_LOGF_IF(cond, ...)       do { if ( (cond) ) { MY_LOGF(__VA_ARGS__); } }while(0)


#define MAX_JPEG_QUALITY  (90)
/******************************************************************************
 *
 ******************************************************************************/
// Gionee <zhangpj> <2014-09-15> modify for CR01359934 begin
#if ORIGINAL_VERSION  
#else
bool ContinuousShot::mpostYuvBuf = false;
#endif
// Gionee <zhangpj> <2014-09-15> modify for CR01359934 end
extern "C"
sp<IShot>
createInstance_ContinuousShot(
    char const*const    pszShotName,
    uint32_t const      u4ShotMode,
    int32_t const       i4OpenId
)
{
    sp<IShot>       pShot = NULL;
    sp<ContinuousShot>  pImpShot = NULL;
    //
    //  (1.1) new Implementator.
    pImpShot = new ContinuousShot(pszShotName, u4ShotMode, i4OpenId);
    if  ( pImpShot == 0 ) {
        CAM_LOGE("[%s] new ContinuousShot", __FUNCTION__);
        goto lbExit;
    }
    //
    //  (1.2) initialize Implementator if needed.
    if  ( ! pImpShot->onCreate() ) {
        CAM_LOGE("[%s] onCreate()", __FUNCTION__);
        goto lbExit;
    }
    //
    //  (2)   new Interface.
    pShot = new IShot(pImpShot);
    if  ( pShot == 0 ) {
        CAM_LOGE("[%s] new IShot", __FUNCTION__);
        goto lbExit;
    }
    //
lbExit:
    //
    //  Free all resources if this function fails.
    if  ( pShot == 0 && pImpShot != 0 ) {
        pImpShot->onDestroy();
        pImpShot = NULL;
    }
    //
    return  pShot;
}


/******************************************************************************
 *  This function is invoked when this object is firstly created.
 *  All resources can be allocated here.
 ******************************************************************************/
bool
ContinuousShot::
onCreate()
{
#warning "[TODO] ContinuousShot::onCreate()"
    bool ret = true;
    return ret;
}


/******************************************************************************
 *  This function is invoked when this object is ready to destryoed in the
 *  destructor. All resources must be released before this returns.
 ******************************************************************************/
void
ContinuousShot::
onDestroy()
{
#warning "[TODO] ContinuousShot::onDestroy()"
}


/******************************************************************************
 *
 ******************************************************************************/
ContinuousShot::
ContinuousShot(
    char const*const pszShotName,
    uint32_t const u4ShotMode,
    int32_t const i4OpenId
)
    : ImpShot(pszShotName, u4ShotMode, i4OpenId)
    , mpMultiShot(NULL) // [CS]+
    , mu4ShotConut(0)
    , mbLastImage(false)
    , mbShotStoped(false)
    , mShotStopMtx()
    , semMShotEnd()
    , mu4GroupId(0)
    , mbCbShutterMsg(true)
    , mpuExifHeaderBuf(NULL)
{
}


/******************************************************************************
 *
 ******************************************************************************/
ContinuousShot::
~ContinuousShot()
{
    if ( mpuExifHeaderBuf != NULL )
        delete [] mpuExifHeaderBuf;
    //if ( mpCaptureBufMgr != NULL )
    //    mpCaptureBufMgr = NULL;
}


/******************************************************************************
 *
 ******************************************************************************/
bool
ContinuousShot::
sendCommand(
    uint32_t const  cmd,
    MUINTPTR const  arg1,
    uint32_t const  arg2,
    uint32_t const  arg3
)
{
    AutoCPTLog cptlog(Event_CShot_sendCmd, cmd, arg1);

    bool ret = true;
    //
    switch  (cmd)
    {
    //  This command is to reset this class. After captures and then reset,
    //  performing a new capture should work well, no matter whether previous
    //  captures failed or not.
    //
    //  Arguments:
    //          N/A
    case eCmd_reset:
        ret = onCmd_reset();
        break;

    //  This command is to perform capture.
    //
    //  Arguments:
    //          N/A
    case eCmd_capture:
        ret = onCmd_capture();
        break;

    //  This command is to perform cancel capture.
    //
    //  Arguments:
    //          N/A
    case eCmd_cancel:
        onCmd_cancel();
        break;
    //  This command is to perform set continuous shot speed.
    //
    //  Arguments:
    //          N/A
    case eCmd_setCShotSpeed:
        ret = onCmd_setCShotSpeed(arg1);
        break;
    //
    default:
        ret = ImpShot::sendCommand(cmd, arg1, arg2, arg3);
    }
    //
    return ret;
}


/******************************************************************************
 *
 ******************************************************************************/
bool
ContinuousShot::
onCmd_reset()
{
#warning "[TODO] ContinuousShot::onCmd_reset()"
    bool ret = true;
    return ret;
}

/******************************************************************************
 *
 ******************************************************************************/
bool
ContinuousShot::
onCmd_capture()
{

    bool ret = true;
    mbCbShutterMsg = true;
// Gionee <zhangpj> <2014-09-15> modify for CR01359934 begin
#if ORIGINAL_VERSION
#else
	if(mShotParam.mu4EnableYuvMsg != 0) 
	{
		mpIMemDrv =  IMemDrv::createInstance();
	    if (mpIMemDrv == NULL)
	    {
	        MY_LOGE("g_pIMemDrv is NULL \n");
	        return 0;
	    }

	    mpIImageBufAllocator =  IImageBufferAllocator::getInstance();
	    if (mpIImageBufAllocator == NULL)
	    {
	        MY_LOGE("mpIImageBufAllocator is NULL \n");
	        return 0;
	    }
		
		requestBufs();
	}
#endif
// Gionee <zhangpj> <2014-09-15> modify for CR01359934 end
    AutoCPTLog cptlog(Event_CShot_capture);
    MY_LOGD("+ ");

    {
        Mutex::Autolock lock(mShotStopMtx);

        if(mbShotStoped)
        {
            return ret;
        }
        //
        mpMultiShot = NSCamShot::IMultiShot::createInstance(static_cast<EShotMode>(mu4ShotMode), "ContinuousShot");
        //
        MUINT32 nrtype = 0;
        {
            IHal3A* p3AHal = IHal3A::createInstance(IHal3A::E_Camera_1,
                    getOpenId(),
                    LOG_TAG);
            if( p3AHal ) {
                nrtype = queryCapNRType( p3AHal->isNeedFiringFlash() ? getCaptureIso() : getPreviewIso() );
                p3AHal->destroyInstance(LOG_TAG);
            }
        }
        //
        mpMultiShot->init();
// Gionee <zhangpj> <2014-09-15> modify for CR01359934 begin
#if ORIGINAL_VERSION
#else
	    //register buffer
	    /*
		if(mShotParam.mu4EnableYuvMsg != 0) 
		{
	      mpMultiShot->registerImageBuffer(NSCamShot::ECamShot_BUF_TYPE_YUV, mpYuvSource);
		}
		*/
#endif		
// Gionee <zhangpj> <2014-09-15> modify for CR01359934 end
        // 
        mpMultiShot->enableNotifyMsg(NSCamShot::ECamShot_NOTIFY_MSG_EOF      |
                                     NSCamShot::ECamShot_NOTIFY_MSG_SHOTS_END );
        //
        EImageFormat ePostViewFmt = static_cast<EImageFormat>(mShotParam.miPostviewDisplayFormat);
        //
// Gionee <zhangpj> <2014-09-01> modify for CR01359934 begin
#if ORIGINAL_VERSION        
        MUINT32 datamsg = NSCamShot::ECamShot_DATA_MSG_JPEG;
#if 0
                        | ((mpCaptureBufMgr == NULL && ePostViewFmt != eImgFmt_UNKNOWN ) ?
                        NSCamShot::ECamShot_DATA_MSG_POSTVIEW : NSCamShot::ECamShot_DATA_MSG_NONE
                        );
#endif
#else
		//MUINT32 datamsg = (mShotParam.mu4EnableYuvMsg) ? NSCamShot::ECamShot_DATA_MSG_YUV : NSCamShot::ECamShot_DATA_MSG_JPEG;		
		MUINT32 datamsg;
		if(mShotParam.mu4EnableYuvMsg == 1 ||mShotParam.mu4EnableYuvMsg == 2)
		{
			datamsg =  NSCamShot::ECamShot_DATA_MSG_YUV;
		}
		else
		{
			datamsg =  NSCamShot::ECamShot_DATA_MSG_JPEG;
		}
		if(mShotParam.mu4EnableYuvMsg == 1)
		{
			mpostYuvBuf = true;
		}
		else 
		{		
			mpostYuvBuf = false;
		}
#endif
// Gionee <zhangpj> <2014-09-01> modify for CR01359934 end
        mpMultiShot->enableDataMsg(datamsg);
// Gionee <zhangpj> <2015-01-06> modify for CR01429061 begin
#if ORIGINAL_VERSION
#else
		 if(getOpenId() == 1 && mShotParam.mu4IsFlip == true){
		  switch(mShotParam.mu4Transform){
		   case eTransform_ROT_90:
			mShotParam.mu4Transform = eTransform_FLIP_V | eTransform_ROT_90; 
			break;
			
		   case eTransform_ROT_270:
			mShotParam.mu4Transform = eTransform_FLIP_H | eTransform_ROT_90; 
			break; 
		   case eTransform_ROT_180:
			mShotParam.mu4Transform = eTransform_FLIP_V; 
			break;
			
		   default:
			mShotParam.mu4Transform = eTransform_FLIP_H; 
			break;
		  }
		 }
#endif		 
// Gionee <zhangpj> <2015-01-06> modify for CR01429061 end

        // shot param
        NSCamShot::ShotParam rShotParam(
                eImgFmt_YUY2,                    //yuv format
                mShotParam.mi4PictureWidth,      //picutre width
                mShotParam.mi4PictureHeight,     //picture height
                mShotParam.mu4Transform,         //picture transform
                ePostViewFmt,                    //postview format
                mShotParam.mi4PostviewWidth,     //postview width
                mShotParam.mi4PostviewHeight,    //postview height
                0,                               //postview transform
                mShotParam.mu4ZoomRatio          //zoom
                );

        if( mJpegParam.mu4JpegQuality > MAX_JPEG_QUALITY ) {
                MY_LOGW("limit jpeg quality to %d", MAX_JPEG_QUALITY);
                mJpegParam.mu4JpegQuality = MAX_JPEG_QUALITY;
        }

        // jpeg param
        NSCamShot::JpegParam rJpegParam(
                NSCamShot::ThumbnailParam(mJpegParam.mi4JpegThumbWidth,
                    mJpegParam.mi4JpegThumbHeight,
                    mJpegParam.mu4JpegThumbQuality,
                    MTRUE),
                mJpegParam.mu4JpegQuality,       //Quality
                MFALSE                           //isSOI
                );


        // sensor param
        NSCamShot::SensorParam rSensorParam(
            getOpenId(),                             //sensor idx
            SENSOR_SCENARIO_ID_NORMAL_CAPTURE,       //Scenaio
            10,                                      //bit depth
            MFALSE,                                  //bypass delay
            MFALSE                                   //bypass scenario
            );
        //
        mpMultiShot->setCallbacks(fgCamShotNotifyCb, fgCamShotDataCb, this);
        //
        ret = mpMultiShot->setShotParam(rShotParam);
        //
        ret = mpMultiShot->setJpegParam(rJpegParam);
        //
        ret = mpMultiShot->setPrvBufHdl((MVOID*)mpPrvBufHandler);
        if( mu4ShotMode == eShotMode_ContinuousShotCc )
        {
            // zsd flow
            if( mpCapBufMgr != NULL ) {
                ret = mpMultiShot->setCapBufMgr((MVOID*)mpCapBufMgr);
            } else {
                MY_LOGE("mpCapBufMgr == NULL");
                ret = MFALSE;
            }
        }
        //
        ret = mpMultiShot->sendCommand( NSCamShot::ECamShot_CMD_SET_NRTYPE, nrtype, 0, 0 );
        //
        mu4ShotConut = 0;
        ::sem_init(&semMShotEnd, 0, 0);
        //
        ret = ret & mpMultiShot->start(rSensorParam, mShotParam.mu4ShotCount);
    }

    ::sem_wait(&semMShotEnd);

    {
        Mutex::Autolock lock(mShotStopMtx);
        if(!mbShotStoped)
        {
            mpMultiShot->stop();
            mbShotStoped = true;
        }
        //
        ret = mpMultiShot->uninit();
        //
        mpMultiShot->destroyInstance();
        //
        mpMultiShot = NULL;
    }
// Gionee <zhangpj> <2014-09-15> modify for CR01359934 begin
#if ORIGINAL_VERSION
#else
    //handle yuv callback, do 3rd algo 
    //handleYuvDataCallback(mpYuvSource,mpYuvSource->getBufSizeInBytes(0));
	//release   buffers
	if(mShotParam.mu4EnableYuvMsg != 0) 
	{
    	releaseBufs();
	}
#endif
// Gionee <zhangpj> <2014-09-15> modify for CR01359934 end

    MY_LOGD("- ");
    return ret;
}


/******************************************************************************
 *
 ******************************************************************************/
void
ContinuousShot::
onCmd_cancel()
{
    AutoCPTLog cptlog(Event_CShot_cancel);
    MY_LOGD("onCmd_cancel +)");

    Mutex::Autolock lock(mShotStopMtx);
    if(!mbShotStoped)
    {
        if(mpMultiShot != NULL)
        {
            MY_LOGD("real need stop MultiShot");
            mpMultiShot->stop();
        }
        else
        {
            MY_LOGD("MultiShot not created, only set mbShotStoped = true");
// Gionee <chenqiang> <2015-05-28> modify for CRCR01471658 begin
#if ORIGINAL_VERSION  
#else
            mpShotCallback->onCB_ContinuousShotEnd();
#endif	  
// Gionee <chenqiang> <2015-05-28> modify for CRCR01471658 end
        }

        mbShotStoped = true;
    }

    MY_LOGD("onCmd_cancel -)");
}

/******************************************************************************
 *
 ******************************************************************************/
bool
ContinuousShot::
onCmd_setCShotSpeed(uint32_t u4CShotSpeed)
{
    bool ret = true;

    Mutex::Autolock lock(mShotStopMtx);
    if(!mbShotStoped && mpMultiShot!=NULL)
    {
        MY_LOGD("set continuous shot speed: %d", u4CShotSpeed);
        ret = mpMultiShot->sendCommand(NSCamShot::ECamShot_CMD_SET_CSHOT_SPEED, u4CShotSpeed, 0, 0);
    }

    return ret;
}


/******************************************************************************
 *
 ******************************************************************************/
MBOOL
ContinuousShot::
fgCamShotNotifyCb(MVOID* user, NSCamShot::CamShotNotifyInfo const msg)
{
    ContinuousShot *pContinuousShot = reinterpret_cast <ContinuousShot *>(user);
    if (NULL != pContinuousShot)
    {
        pContinuousShot->handleNotifyCb(msg);
    }

    return MTRUE;
}
//[CS]+
/******************************************************************************
 *
 ******************************************************************************/
MBOOL
ContinuousShot::handleNotifyCb(NSCamShot::CamShotNotifyInfo const msg)
{
    AutoCPTLog cptlog(Event_CShot_handleNotifyCb);
    MY_LOGD("+ (msgType, ext1, ext2), (%d, %d, %d)", msg.msgType, msg.ext1, msg.ext2);

    switch(msg.msgType)
    {
        case NSCamShot::ECamShot_NOTIFY_MSG_EOF:
            if(mbCbShutterMsg)
            {
                mbCbShutterMsg = false;
                mpShotCallback->onCB_Shutter(false, mu4ShotConut);
            }
            break;

        case NSCamShot::ECamShot_NOTIFY_MSG_SHOTS_END:
            mbLastImage = true;
            break;

        default:
            break;
    }

   return MTRUE;
}
//[CS]-


/******************************************************************************
 *
 ******************************************************************************/
MBOOL
ContinuousShot::
fgCamShotDataCb(MVOID* user, NSCamShot::CamShotDataInfo const msg)
{
    ContinuousShot *pContinuousShot = reinterpret_cast<ContinuousShot *>(user);
    if (NULL != pContinuousShot)
    {
        if (NSCamShot::ECamShot_DATA_MSG_POSTVIEW == msg.msgType)
        {
            //pContinuousShot->handlePostViewData( msg.puData, msg.u4Size);
        }
        else if (NSCamShot::ECamShot_DATA_MSG_JPEG == msg.msgType)
        {
            pContinuousShot->handleJpegData(
                    (IImageBuffer*)msg.pBuffer,
                    (IImageBuffer*)msg.ext1,
                    (IDbgInfoContainer*)msg.ext2
                    );
        }
// Gionee <zhangpj> <2014-09-15> modify for CR01359934 begin
#if ORIGINAL_VERSION  
#else
		else if (NSCamShot::ECamShot_DATA_MSG_YUV == msg.msgType)
        {
        	if(true == mpostYuvBuf)
        	{
	        	pContinuousShot->handleYuvDataCallback((IImageBuffer*)msg.pBuffer, msg.pBuffer->getBufSizeInBytes(0)); 
				pContinuousShot->handleJpegData(
						(IImageBuffer*)msg.pBuffer,
						NULL,
						NULL
						);
        	}
			else
			{
				pContinuousShot->handleYuvDataCallback((IImageBuffer*)msg.pBuffer, msg.pBuffer->getBufSizeInBytes(0));  
			}
        }
#endif		
// Gionee <zhangpj> <2014-09-15> modify for CR01359934 end
    }

    return MTRUE;
}


/******************************************************************************
*
*******************************************************************************/
MBOOL
ContinuousShot::
handlePostViewData(MUINT8* const puBuf, MUINT32 const u4Size)
{
#if 0
    AutoCPTLog cptlog(Event_CShot_handlePVData);
    MY_LOGD("+ (puBuf, size) = (%p, %d)", puBuf, u4Size);
    mpShotCallback->onCB_PostviewDisplay(0,
                                         u4Size,
                                         reinterpret_cast<uint8_t const*>(puBuf)
                                        );

    MY_LOGD("-");
#endif
    return  MTRUE;
    }

/******************************************************************************
*
*******************************************************************************/
MBOOL
ContinuousShot::
handleJpegData(IImageBuffer* pJpeg, IImageBuffer* pThumb, IDbgInfoContainer* pDbg)
{
    AutoCPTLog cptlog(Event_CShot_handleJpegData);
    MUINT8* puJpegBuf = (MUINT8*)pJpeg->getBufVA(0);
// Gionee <zhangpj> <2014-09-01> modify for CR01359934 end
#if ORIGINAL_VERSION  
    MUINT32 u4JpegSize = pJpeg->getBitstreamSize();
#else	
	MUINT32 u4JpegSize;
    if(true == mpostYuvBuf)
    {
    	u4JpegSize = pJpeg->getBufSizeInBytes(0);
    } 
	else 
	{
    	u4JpegSize = pJpeg->getBitstreamSize();
	}
#endif
// Gionee <zhangpj> <2014-09-01> modify for CR01359934 end
    MUINT8* puThumbBuf = NULL;
    MUINT32 u4ThumbSize = 0;
    if( pThumb != NULL )
    {
        puThumbBuf = (MUINT8*)pThumb->getBufVA(0);
        u4ThumbSize = pThumb->getBitstreamSize();
    }

    MY_LOGD("+ (puJpgBuf, jpgSize, puThumbBuf, thumbSize, dbg) = (%p, %d, %p, %d, %p)",
            puJpegBuf, u4JpegSize, puThumbBuf, u4ThumbSize, pDbg);

    if( mpuExifHeaderBuf == NULL )
    {
        mpuExifHeaderBuf = new MUINT8[ DBG_EXIF_SIZE ];
    }
    MUINT8 *puExifHeaderBuf = mpuExifHeaderBuf;
    MUINT32 u4ExifHeaderSize = 0;

    mu4ShotConut++; // shot count from 1

    CPTLogStr(Event_CShot_handleJpegData, CPTFlagSeparator, "make exif");
    if(1 == mu4ShotConut)
    {
        timeval tv;
        ::gettimeofday(&tv, NULL);
        mu4GroupId = tv.tv_sec * 1000000 + tv.tv_usec;
    }
// Gionee <zhangpj> <2014-09-01> modify for CR01359934 begin
#if ORIGINAL_VERSION  	
    makeExifHeader(
            eAppMode_PhotoMode,
            puThumbBuf,
            u4ThumbSize,
            puExifHeaderBuf,
            u4ExifHeaderSize,
            pDbg,
            mu4ShotConut,
            mu4GroupId);
#else
	if(false == mpostYuvBuf)
	{
		makeExifHeader(
				eAppMode_PhotoMode,
				puThumbBuf,
				u4ThumbSize,
				puExifHeaderBuf,
				u4ExifHeaderSize,
				pDbg,
				mu4ShotConut,
				mu4GroupId); 
	}
#endif
// Gionee <zhangpj> <2014-09-01> modify for CR01359934 end

    MY_LOGD("(thumbbuf, size, exifHeaderBuf, size, groupId) = (%p, %d, %p, %d, %d)",
                      puThumbBuf, u4ThumbSize, puExifHeaderBuf, u4ExifHeaderSize, mu4GroupId);
    // dummy raw callback
    mpShotCallback->onCB_RawImage(0, 0, NULL);

    // Jpeg callback
    CPTLogStr(Event_CShot_handleJpegData, CPTFlagSeparator, "Jpeg callback");
    mpShotCallback->onCB_CompressedImage(0,
                                         u4JpegSize,
                                         reinterpret_cast<uint8_t const*>(puJpegBuf),
                                         u4ExifHeaderSize,                       //header size
                                         puExifHeaderBuf,                    //header buf
                                         mu4ShotConut,                       //callback index
                                         mbLastImage                        //final image
                                         );

    CPTLogStr(Event_CShot_handleJpegData, CPTFlagSeparator, "Jpeg callback end");
    if(mbLastImage)
    {
        MY_LOGD("CShot end, post end sem");
        ::sem_post(&semMShotEnd);
    }
    
    MY_LOGD("-"); 

    return MTRUE; 

}

// Gionee <zhangpj> <2014-09-15> modify for CR01359934 begin
#if ORIGINAL_VERSION
#else
MBOOL
ContinuousShot::
handleYuvDataCallback(IImageBuffer* puBuf, MUINT32 const u4Size)
{
    MY_LOGD("+ (puBuf, size) = (%p, %d)", puBuf, u4Size); 
	
    MUINT8* puYuvBuf = (MUINT8*)puBuf->getBufVA(0);
    mpShotCallback->onCB_YUVData(0, 
                                 u4Size, 
                                 reinterpret_cast<uint8_t *>(puYuvBuf)
                                ); 

    MY_LOGD("-"); 
	if(false == mpostYuvBuf)
	{
		handleProcessedYuvData(puBuf);
	}
	return  MTRUE;
}


//
MBOOL
ContinuousShot::
releaseBufs()
{
    MY_LOGD("[releaseBufs] - E.");
    deallocMem(mJpegBuf);
    deallocMem(mThumbBuf);
    //deallocMem(mpYuvSource);

    MY_LOGD("[releaseBufs] - X.");
    return  MTRUE;

}

void
ContinuousShot::
deallocMem(IImageBuffer *pBuf)
{
		pBuf->unlockBuf(LOG_TAG);
		if (pBuf->getImgFormat() == eImgFmt_JPEG)
		{
			mpIImageBufAllocator->free(pBuf);
		}
		else
		{
			pBuf->decStrong(pBuf);
			for (Vector<ImageBufferMap>::iterator it = mvImgBufMap.begin();
					it != mvImgBufMap.end();
					it++)
			{
				if (it->pImgBuf == pBuf)
				{
					mpIMemDrv->unmapPhyAddr(&it->memBuf);
					if (mpIMemDrv->freeVirtBuf(&it->memBuf))
					{
						MY_LOGE("m_pIMemDrv->freeVirtBuf() error");
					}
					else
					{
						mvImgBufMap.erase(it);
					}
					break;
				}
			}
		}
}


MBOOL
ContinuousShot::
requestBufs()
{
     	MY_LOGD("[requestBufs] - E.");
		MBOOL	ret = MTRUE;
		MBOOL bVertical = ( mShotParam.mu4Transform == NSCam::eTransform_ROT_90 || 
            mShotParam.mu4Transform == NSCam::eTransform_ROT_270 );

		mJpgWidth = bVertical? mShotParam.mi4PictureHeight: mShotParam.mi4PictureWidth;
		mJpgHeight = bVertical? mShotParam.mi4PictureWidth: mShotParam.mi4PictureHeight;

		mThumbWidth = bVertical? mJpegParam.mi4JpegThumbHeight: mJpegParam.mi4JpegThumbWidth;
		mThumbHeight = bVertical? mJpegParam.mi4JpegThumbWidth: mJpegParam.mi4JpegThumbHeight;	

		mYuvWidth = mShotParam.mi4PictureWidth;
		mYuvHeight = mShotParam.mi4PictureHeight ;
		
		MY_LOGD("[requestBufs] mJpgWidth %d mJpgHeight %d",mJpgWidth,mJpgHeight);

        mJpegBuf = allocMem(eImgFmt_JPEG, 
                                     mJpgWidth, 
                                     mJpgHeight);
        mThumbBuf = allocMem(eImgFmt_JPEG, 
                                     mThumbWidth, 
                                     mThumbHeight); 
	    //mpYuvSource = allocMem(eImgFmt_YUY2, mYuvWidth, mYuvHeight); //format base on your 3rd algorithm	   
  	    //mpYuvSource=allocMem(eImgFmt_YUY2,mJpgWidth,mJpgHeight);//format base on your 3rd algorithm
  	    
        MY_LOGD("[requestBufs] JpegBuf(0x%x),ThumbnailBuf(0x%x)", mJpegBuf,mThumbBuf);
		ret = MTRUE;
		MY_LOGD("[requestBufs] - X.");
		return	ret;
}

IImageBuffer*
ContinuousShot::
allocMem(MUINT32 fmt, MUINT32 w, MUINT32 h)
{
    IImageBuffer* pBuf;

    if( fmt != eImgFmt_JPEG )
    {
        /* To avoid non-continuous multi-plane memory, allocate ION memory and map it to ImageBuffer */
        MUINT32 plane = NSCam::Utils::Format::queryPlaneCount(fmt);
        ImageBufferMap bufMap;

        bufMap.memBuf.size = 0;
        for (int i = 0; i < plane; i++)
        {
            bufMap.memBuf.size += (NSCam::Utils::Format::queryPlaneWidthInPixels(fmt,i, w) * NSCam::Utils::Format::queryPlaneBitsPerPixel(fmt,i) / 8) * NSCam::Utils::Format::queryPlaneHeightInPixels(fmt, i, h);
        }

        if (mpIMemDrv->allocVirtBuf(&bufMap.memBuf)) {
            MY_LOGE("g_pIMemDrv->allocVirtBuf() error \n");
            return NULL;
        }
        //memset((void*)bufMap.memBuf.virtAddr, 0 , bufMap.memBuf.size);
        if (mpIMemDrv->mapPhyAddr(&bufMap.memBuf)) {
            MY_LOGE("mpIMemDrv->mapPhyAddr() error \n");
            return NULL;
        }

        MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
        MUINT32 bufStridesInBytes[3] = {0};

        for (MUINT32 i = 0; i < plane; i++)
        { 
            bufStridesInBytes[i] = NSCam::Utils::Format::queryPlaneWidthInPixels(fmt,i, w) * NSCam::Utils::Format::queryPlaneBitsPerPixel(fmt,i) / 8;
        }
        IImageBufferAllocator::ImgParam imgParam(
                fmt, 
                MSize(w,h), 
                bufStridesInBytes, 
                bufBoundaryInBytes, 
                plane
                );

        PortBufInfo_v1 portBufInfo = PortBufInfo_v1(
                                        bufMap.memBuf.memID,
                                        bufMap.memBuf.virtAddr,
                                        bufMap.memBuf.useNoncache, 
                                        bufMap.memBuf.bufSecu, 
                                        bufMap.memBuf.bufCohe);

        sp<ImageBufferHeap> pHeap = ImageBufferHeap::create(
                                                        LOG_TAG,
                                                        imgParam,
                                                        portBufInfo);
        if(pHeap == 0)
        {
            MY_LOGE("pHeap is NULL");
            return NULL;
        }
        //
        pBuf = pHeap->createImageBuffer();
        pBuf->incStrong(pBuf);

        bufMap.pImgBuf = pBuf;
        mvImgBufMap.push_back(bufMap);
    }
    else
    {
        MINT32 bufBoundaryInBytes = 0;
        IImageBufferAllocator::ImgParam imgParam(
                MSize(w,h), 
                w * h * 6 / 5,  //FIXME
                bufBoundaryInBytes
                );

        pBuf = mpIImageBufAllocator->alloc_ion(LOG_TAG, imgParam);
    }
    if (!pBuf || !pBuf->lockBuf( LOG_TAG, eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_SW_WRITE_OFTEN ) )
    {
        MY_LOGE("Null allocated or lock Buffer failed\n");
        return  NULL;
    }

    pBuf->syncCache(eCACHECTRL_INVALID);

    return pBuf;
}

MBOOL
ContinuousShot::
createJpegImg(IImageBuffer const * rSrcImgBufInfo
		  , NSCamShot::JpegParam const & rJpgParm
		  , MUINT32 const u4Transform
		  , IImageBuffer const * rJpgImgBufInfo
		  , MUINT32 & u4JpegSize)
{
		MBOOL ret = MTRUE;
		// (0). debug
		MY_LOGD("[createJpegImg] - E.");
		MY_LOGD("[createJpegImg] - rSrcImgBufInfo.eImgFmt=%d", rSrcImgBufInfo->getImgFormat());
		MY_LOGD("[createJpegImg] - u4Transform=%d", u4Transform);
		MY_LOGD("[createJpegImg]  dst imgbuf(0x%x)", rJpgImgBufInfo);

		CPTLog(Event_FBShot_JpegEncodeImg, CPTFlagStart);
		//
		// (1). Create Instance
		NSCam::NSIoPipe::NSSImager::ISImager *pISImager = NSCam::NSIoPipe::NSSImager::ISImager::createInstance(rSrcImgBufInfo);
		if(!pISImager) {
		MY_LOGE("createJpegImg can't get ISImager instance.");
		return MFALSE;
		}
	
		// init setting
		pISImager->setTargetImgBuffer(rJpgImgBufInfo);
		//
		pISImager->setTransform(u4Transform);
		//
		//pISImager->setFlip(u4Flip);
		//
		//pISImager->setResize(rJpgImgBufInfo.u4ImgWidth, rJpgImgBufInfo.u4ImgHeight);
		//
		pISImager->setEncodeParam(rJpgParm.fgIsSOI, rJpgParm.u4Quality);
		//
		//pISImager->setROI(Rect(0, 0, rSrcImgBufInfo->getImgSize().w, rSrcImgBufInfo->getImgSize().h));
		//
		pISImager->execute();
		//
		u4JpegSize = rJpgImgBufInfo->getBitstreamSize();
	
		pISImager->destroyInstance();
		CPTLog(Event_FBShot_JpegEncodeImg, CPTFlagEnd);
	
		MY_LOGD("[init] - X. ret: %d.", ret);
		return ret;
}


MBOOL
ContinuousShot::
handleProcessedYuvData(IImageBuffer* puBuf)
{
    MY_LOGD("[handleProcessedYuvData] - E.");


	//
    MUINT32 u4JpegSize = 0;
    MUINT32 u4ThumbSize = 0; 	
    //1. create jpeg
    // jpeg param 

    NSCamShot::JpegParam rJpegParam(
            mJpegParam.mu4JpegQuality,         //Quality 
            MFALSE                             //isSOI 
            );
	//main yuv has rotate in pass2
    createJpegImg(puBuf,rJpegParam,0,mJpegBuf,u4JpegSize);
	//2.create thumbnail
    if (0 != mJpegParam.mi4JpegThumbWidth && 0 != mJpegParam.mi4JpegThumbHeight)
    {
        NSCamShot::JpegParam rParam(mJpegParam.mu4JpegThumbQuality, MTRUE);
        createJpegImg(puBuf, rParam, 0, mThumbBuf, u4ThumbSize);
    }

	 mJpegBuf->syncCache(eCACHECTRL_INVALID);
     mThumbBuf->syncCache(eCACHECTRL_INVALID);
   
	//3.make exif and jpeg callback
    handleJpegData(
                    mJpegBuf,
                    mThumbBuf,
                    NULL
                    );	

	MY_LOGD("[handleProcessedYuvData] - X.");
    return MTRUE; 
}
#endif
// Gionee <zhangpj> <2014-09-15> modify for CR01359934 end
