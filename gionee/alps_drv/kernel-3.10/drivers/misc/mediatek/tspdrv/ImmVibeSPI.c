/*
** =============================================================================
**
** File: ImmVibeSPI.c
**
** Description:
**     Device-dependent functions called by Immersion TSP API
**     to control PWM duty cycle, amp enable/disable, save IVT file, etc...
**
**
** Copyright (c) 2012-2014 Immersion Corporation. All Rights Reserved.
**
** This file contains Original Code and/or Modifications of Original Code
** as defined in and that are subject to the GNU Public License v2 -
** (the 'License'). You may not use this file except in compliance with the
** License. You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA or contact
** TouchSenseSales@immersion.com.
**
** The Original Code and all software distributed under the License are
** distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
** EXPRESS OR IMPLIED, AND IMMERSION HEREBY DISCLAIMS ALL SUCH WARRANTIES,
** INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS
** FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT. Please see
** the License for the specific language governing rights and limitations
** under the License.
**
** =============================================================================
*/
//#warning ********* Compiling SPI for DRV2604L using LRA actuator ************

#ifdef IMMVIBESPIAPI
#undef IMMVIBESPIAPI
#endif
#define IMMVIBESPIAPI static

#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>

#include <linux/slab.h>
#include <linux/types.h>

#include <linux/fs.h>
#include <linux/cdev.h>

#include <linux/i2c.h>
#include <linux/semaphore.h>
#include <linux/device.h>

#include <linux/syscalls.h>
#include <asm/uaccess.h>
#include <linux/delay.h>

#include <linux/gpio.h>
#include <linux/workqueue.h>
#include "ImmVibeSPI_custom.h"

/*
** Copy ImmVibeSPI.c, autocal.seq, and init.seq from
** actuator subdirectory into the same directory as tspdrv.c
*/


static int g_nDeviceID = -1;
static struct i2c_client* g_pTheClient = NULL;
static bool g_bAmpEnabled = false;


#ifndef ACTUATOR_NAME
#define ACTUATOR_NAME "act???"
#endif

static void drv2604l_write_reg_val(const unsigned char* data, unsigned int size)
{
    int i = 0;

    if (size % 2 != 0)
        return;

    while (i < size)
    {
        i2c_smbus_write_byte_data(g_pTheClient, data[i], data[i+1]);
        i+=2;
    }
}

static unsigned char drv2604l_read_reg(unsigned char reg)
{
    return i2c_smbus_read_byte_data(g_pTheClient, reg);
}

#if SKIP_AUTOCAL == 0
static void drv2604l_poll_go_bit(void)
{
    while (drv2604l_read_reg(GO_REG) == GO)
      schedule_timeout_interruptible(msecs_to_jiffies(GO_BIT_POLL_INTERVAL));
}
#endif

static void drv2604l_set_rtp_val(char value)
{
    char rtp_val[] =
    {
        REAL_TIME_PLAYBACK_REG, value
    };
    drv2604l_write_reg_val(rtp_val, sizeof(rtp_val));
}

static void drv2604l_change_mode(char mode)
{
    unsigned char tmp[] =
    {
        MODE_REG, mode
    };
    drv2604l_write_reg_val(tmp, sizeof(tmp));
}

#if USE_DRV2604L_EN_PIN
static void drv2604l_set_en(bool enabled)
{
	mt_set_gpio_out(GPIO_AMP_EN, enabled ? GPIO_OUT_ONE : GPIO_OUT_ZERO);
}
#endif
IMMVIBESPIAPI VibeStatus ImmVibeSPI_ForceOut_AmpDisable(VibeUInt8 nActuatorIndex);
static void drv2604l_shutdown(struct i2c_client* client);
static int drv2604l_probe(struct i2c_client* client, const struct i2c_device_id* id);
static int drv2604l_remove(struct i2c_client* client);
static const struct i2c_device_id drv2604l_id[] =
{
    {DRV2604L_BOARD_NAME, 0},
    {}
};

static struct i2c_board_info info = {
  I2C_BOARD_INFO(DRV2604L_BOARD_NAME, DEVICE_ADDR),
};

static struct i2c_driver drv2604l_driver =
{
    .probe = drv2604l_probe,
    .remove = drv2604l_remove,
    .id_table = drv2604l_id,
    .shutdown = drv2604l_shutdown,
    .driver =
    {
        .name = DRV2604L_BOARD_NAME,
    },
};

static int drv2604l_probe(struct i2c_client* client, const struct i2c_device_id* id)
{
    char status;
#if SKIP_AUTOCAL == 0
    int nCalibrationCount = 0;
#endif

    if (!i2c_check_functionality(client->adapter, I2C_FUNC_I2C))
    {
        DbgOutErr(("drv2604l probe failed"));
        return -ENODEV;
    }
    g_pTheClient = client;

#if USE_DRV2604L_EN_PIN
	mt_set_gpio_mode(GPIO_AMP_EN, GPIO_MODE_00);
	mt_set_gpio_dir(GPIO_AMP_EN, GPIO_DIR_OUT);
	mt_set_gpio_out(GPIO_AMP_EN, GPIO_OUT_ONE);
	udelay(30);
#endif

#if USE_DRV2604L_STANDBY
    drv2604l_change_mode(MODE_DEVICE_READY);
#endif
    /* Wait 1000 us for chip power to stabilize */
	msleep(1);

#if SKIP_AUTOCAL
    drv2604l_write_reg_val(init_sequence, sizeof(init_sequence));
    status = drv2604l_read_reg(STATUS_REG);
#else
    /* Run auto-calibration */
    do{
        drv2604l_write_reg_val(autocal_sequence, sizeof(autocal_sequence));

        /* Wait until the procedure is done */
        drv2604l_poll_go_bit();

        /* Read status */
        status = drv2604l_read_reg(STATUS_REG);

        nCalibrationCount++;

    } while (((status & DIAG_RESULT_MASK) == AUTO_CAL_FAILED) && (nCalibrationCount < MAX_AUTOCALIBRATION_ATTEMPT));
	
    /* Check result */
    if ((status & DIAG_RESULT_MASK) == AUTO_CAL_FAILED)
    {
		DbgOutErr(("drv2604l auto-calibration failed after %d attempts.\n", nCalibrationCount));
    }
    else
    {
        /* Read calibration results */
        drv2604l_read_reg(AUTO_CALI_RESULT_REG);
        drv2604l_read_reg(AUTO_CALI_BACK_EMF_RESULT_REG);
        drv2604l_read_reg(FEEDBACK_CONTROL_REG);
    }
#endif

    /* Read device ID */
    g_nDeviceID = (status & DEV_ID_MASK);
    switch (g_nDeviceID)
    {
        case DRV2605:
            DbgOutInfo(("drv2604 driver found: drv2605.\n"));
            break;
        case DRV2604:
            DbgOutInfo(("drv2604 driver found: drv2604.\n"));
            break;
		 case DRV2604L:
            DbgOutInfo(("drv2604 driver found: drv2604L.\n"));
            break;
        default:
            DbgOutInfo(("drv2604 driver found: unknown.\n"));
            break;
    }

#if USE_DRV2604L_STANDBY
    /* Put hardware in standby */
    drv2604l_change_mode(MODE_STANDBY);
#elif USE_DRV2604L_EN_PIN
    /* enable RTP mode that will be toggled on/off with EN pin */
#endif

#if USE_DRV2604L_EN_PIN
    /* turn off chip */
   // drv2604l_set_en(false);
#endif

    DbgOutInfo(("drv2604l probe succeeded"));

  return 0;
}

static void drv2604l_shutdown(struct i2c_client* client)
{

    ImmVibeSPI_ForceOut_AmpDisable(0);

    /* Remove TS5000 driver */
    //i2c_del_driver(&drv2604l_driver);

    /* Reverse i2c_new_device */
    //i2c_unregister_device(g_pTheClient);

    DbgOutErr(("drv2604l_remove.\n"));
    return;
}


static int drv2604l_remove(struct i2c_client* client)
{
    DbgOutVerbose(("drv2604l_remove.\n"));
    return 0;
}

#if GUARANTEE_AUTOTUNE_BRAKE_TIME

#define AUTOTUNE_BRAKE_TIME 30

static VibeInt8 g_lastForce = 0;
static bool g_brake = false;

static void autotune_brake_complete(struct work_struct *work)
{
    /* new nForce value came in before workqueue terminated */
    if (g_lastForce > 0)
        return;

#if USE_DRV2604L_STANDBY
    /* Put hardware in standby */
    drv2604l_change_mode(MODE_STANDBY);
#endif

#if USE_DRV2604L_EN_PIN
    //drv2604l_set_en(false);
#endif
}

DECLARE_DELAYED_WORK(g_brake_complete, autotune_brake_complete);

static struct workqueue_struct *g_workqueue;

#endif

/*
** Called to disable amp (disable output force)
*/
IMMVIBESPIAPI VibeStatus ImmVibeSPI_ForceOut_AmpDisable(VibeUInt8 nActuatorIndex)
{
    if (g_bAmpEnabled)
    {
        DbgOutVerbose(("ImmVibeSPI_ForceOut_AmpDisable.\n"));

        /* Set the force to 0 */
        drv2604l_set_rtp_val(0);

#if GUARANTEE_AUTOTUNE_BRAKE_TIME
        /* if a brake signal arrived from daemon, let the chip stay on
         * extra time to allow it to brake */
        if (g_brake && g_workqueue)
        {
            queue_delayed_work(g_workqueue,
                               &g_brake_complete,
                               msecs_to_jiffies(AUTOTUNE_BRAKE_TIME));
        }
        else /* disable immediately (smooth effect style) */
#endif
        {
#if USE_DRV2604L_STANDBY
            /* Put hardware in standby via i2c */
            drv2604l_change_mode(MODE_STANDBY);
#endif

#if USE_DRV2604L_EN_PIN
            /* Disable hardware via pin */
            //drv2604l_set_en(false);
#endif
        }

        g_bAmpEnabled = false;
    }
    return VIBE_S_SUCCESS;
}

/*
** Called to enable amp (enable output force)
*/
IMMVIBESPIAPI VibeStatus ImmVibeSPI_ForceOut_AmpEnable(VibeUInt8 nActuatorIndex)
{
    if (!g_bAmpEnabled)
    {
        DbgOutVerbose(("ImmVibeSPI_ForceOut_AmpEnable.\n"));

#if GUARANTEE_AUTOTUNE_BRAKE_TIME
        cancel_delayed_work_sync(&g_brake_complete);
#endif

#if USE_DRV2604L_EN_PIN
        //drv2604l_set_en(true);
#endif

#if USE_DRV2604L_STANDBY
        drv2604l_change_mode(MODE_DEVICE_READY);
#endif
        // Chip requires minimum 250us power-up delay before RTP value can be set
        // If the chip is powered on <10ms after powering off, it needs 1000us
        // for the internal LDO voltage to stabilize
        msleep(1);

        /* Workaround for power issue in the DRV2604 */
        /* Restore the register settings if they have reset to the defaults */
        if(drv2604l_read_reg(RATED_VOLTAGE_REG) != init_sequence[3])
        {
            drv2604l_write_reg_val(init_sequence, sizeof(init_sequence));
        }

        drv2604l_change_mode(MODE_REAL_TIME_PLAYBACK);
        g_bAmpEnabled = true;
    }
    return VIBE_S_SUCCESS;
}

/*
** Called at initialization time.
*/
IMMVIBESPIAPI VibeStatus ImmVibeSPI_ForceOut_Initialize(void)
{
    struct i2c_adapter* adapter;
    struct i2c_client* client;

    DbgOutVerbose(("ImmVibeSPI_ForceOut_Initialize.\n"));

    adapter = i2c_get_adapter(DEVICE_BUS);

    if (adapter) {
        client = i2c_new_device(adapter, &info);

        if (client) {
            int retVal = i2c_add_driver(&drv2604l_driver);

            if (retVal) {
                return VIBE_E_FAIL;
            }

        } else {
            DbgOutVerbose(("drv2604l: Cannot create new device.\n"));
            return VIBE_E_FAIL;
        }

    } else {
        DbgOutVerbose(("ImmVibeSPI_ForceOut_AmpDisable.\n"));

        return VIBE_E_FAIL;
    }

#if GUARANTEE_AUTOTUNE_BRAKE_TIME
    g_workqueue = create_workqueue("tspdrv_workqueue");
#endif

    return VIBE_S_SUCCESS;
}

/*
** Called at termination time to disable amp, etc...
*/
IMMVIBESPIAPI VibeStatus ImmVibeSPI_ForceOut_Terminate(void)
{
    DbgOutVerbose(("ImmVibeSPI_ForceOut_Terminate.\n"));

#if GUARANTEE_AUTOTUNE_BRAKE_TIME
    if (g_workqueue)
    {
        destroy_workqueue(g_workqueue);
        g_workqueue = 0;
    }
#endif

    ImmVibeSPI_ForceOut_AmpDisable(0);

    /* Remove TS5000 driver */
    //i2c_del_driver(&drv2604l_driver);

    /* Reverse i2c_new_device */
    //i2c_unregister_device(g_pTheClient);

    return VIBE_S_SUCCESS;
}

/*
** Called by the real-time loop to set the force
*/
IMMVIBESPIAPI VibeStatus ImmVibeSPI_ForceOut_SetSamples(VibeUInt8 nActuatorIndex, VibeUInt16 nOutputSignalBitDepth, VibeUInt16 nBufferSizeInBytes, VibeInt8* pForceOutputBuffer)
{
#if GUARANTEE_AUTOTUNE_BRAKE_TIME
    VibeInt8 force = pForceOutputBuffer[0];
    if (force > 0 && g_lastForce <= 0)
    {
        g_brake = false;

        ImmVibeSPI_ForceOut_AmpEnable(nActuatorIndex);
    }
    else if (force <= 0 && g_lastForce > 0)
    {
        g_brake = force < 0;

        ImmVibeSPI_ForceOut_AmpDisable(nActuatorIndex);
    }

    if (g_lastForce != force)
    {
        /* AmpDisable sets force to zero, so need to here */
        if (force > 0)
            drv2604l_set_rtp_val(pForceOutputBuffer[0]);

        g_lastForce = force;
    }
#else
    drv2604l_set_rtp_val(pForceOutputBuffer[0]);
#endif

    return VIBE_S_SUCCESS;
}

/*
** Called to set force output frequency parameters
*/
IMMVIBESPIAPI VibeStatus ImmVibeSPI_ForceOut_SetFrequency(VibeUInt8 nActuatorIndex, VibeUInt16 nFrequencyParameterID, VibeUInt32 nFrequencyParameterValue)
{
    if (nActuatorIndex != 0) return VIBE_S_SUCCESS;

    switch (nFrequencyParameterID)
    {
        case VIBE_KP_CFG_FREQUENCY_PARAM1:
            /* Update frequency parameter 1 */
            break;

        case VIBE_KP_CFG_FREQUENCY_PARAM2:
            /* Update frequency parameter 2 */
            break;

        case VIBE_KP_CFG_FREQUENCY_PARAM3:
            /* Update frequency parameter 3 */
            break;

        case VIBE_KP_CFG_FREQUENCY_PARAM4:
            /* Update frequency parameter 4 */
            break;

        case VIBE_KP_CFG_FREQUENCY_PARAM5:
            /* Update frequency parameter 5 */
            break;

        case VIBE_KP_CFG_FREQUENCY_PARAM6:
            /* Update frequency parameter 6 */
            break;
    }
    return VIBE_S_SUCCESS;
}

/*
** Called to get the device name (device name must be returned as ANSI char)
*/
IMMVIBESPIAPI VibeStatus ImmVibeSPI_Device_GetName(VibeUInt8 nActuatorIndex, char *szDevName, int nSize)
{
    char szRevision[MAX_REVISION_STRING_SIZE];

    if ((!szDevName) || (nSize < 1)) return VIBE_E_FAIL;

    DbgOutVerbose(("ImmVibeSPI_Device_GetName.\n"));

    switch (g_nDeviceID)
    {
        case DRV2605:
            strncpy(szDevName, "DRV2605", nSize-1);
            break;
        case DRV2604:
            strncpy(szDevName, "DRV2604", nSize-1);
            break;
		case DRV2604L:
            strncpy(szDevName, "DRV2604l", nSize-1);
            break;
        default:
            strncpy(szDevName, "Unknown", nSize-1);
            break;
    }

    /* Append revision number to the device name */
    sprintf(szRevision, "r%d %s", (drv2604l_read_reg(SILICON_REVISION_REG) & SILICON_REVISION_MASK), ACTUATOR_NAME);
    if ((strlen(szRevision) + strlen(szDevName)) < nSize-1)
        strcat(szDevName, szRevision);

    szDevName[nSize - 1] = '\0'; /* make sure the string is NULL terminated */

    return VIBE_S_SUCCESS;
}
