/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ANDROID_CWMCU_SENSOR_H
#define ANDROID_CWMCU_SENSOR_H

#include <stdint.h>
#include <errno.h>
#include <sys/cdefs.h>
#include <sys/types.h>

#include "sensors.h"
#include "SensorBase.h"
#include "InputEventReader.h"
/*
#define	SAVE_PATH_ACC				"/data/data/com.cywee.calibrator/cw_calibrator_acc.ini"
#define	SAVE_PATH_MAG				"/data/data/com.cywee.calibrator/cw_calibrator_mag.ini"
#define	SAVE_PATH_GYRO				"/data/data/com.cywee.calibrator/cw_calibrator_gyro.ini"
#define	SAVE_PATH_LIGHT				"/data/data/com.cywee.calibrator/cw_calibrator_light.ini"
#define	SAVE_PATH_PROXIMITY			"/data/data/com.cywee.calibrator/cw_calibrator_proximity.ini"
*/
/*
#define	SAVE_PATH_ACC				"/data/system/cw_calibrator_acc.ini"
#define	SAVE_PATH_MAG			    "/data/system/cw_calibrator_mag.ini"
#define	SAVE_PATH_GYRO				"/data/system/cw_calibrator_gyro.ini"
#define	SAVE_PATH_LIGHT				"/data/system/cw_calibrator_light.ini"
#define	SAVE_PATH_PROXIMITY			"/data/system/cw_calibrator_proximity.ini"
*/
#define	SAVE_PATH_ACC				"/data/misc/cywee/cw_calibrator_acc.ini"
#define	SAVE_PATH_MAG				"/data/misc/cywee/cw_calibrator_mag.ini"
#define	SAVE_PATH_GYRO				"/data/misc/cywee/cw_calibrator_gyro.ini"
#define	SAVE_PATH_LIGHT				"/data/misc/cywee/cw_calibrator_light.ini"
#define	SAVE_PATH_PROXIMITY			"/data/misc/cywee/cw_calibrator_proximity.ini"

#define	NS         (int64_t)1000000ll
#define	US         1000ll
/*****************************************************************************/
struct input_event;

typedef enum {
	RmcuWap = 0
	,RapWmcu
}CalibratorCmd;
typedef enum {
	CALIBRATOR_STATUS_OUT_OF_RANGE= -2,
	CALIBRATOR_STATUS_FAIL= -1,
	CALIBRATOR_STATUS_NON = 0,
	CALIBRATOR_STATUS_INPROCESS = 1,
	CALIBRATOR_STATUS_PASS = 2,
} CALIBRATOR_STATUS;

typedef struct {
    uint8_t      ready;
	uint16_t	u16Time;
	uint32_t	u32Time;
	int64_t	i64Time;
    int64_t TimeOffset;
    int64_t TimeOffsetBias;
	int64_t	reportTime;
}CwTime_t, *pCwTime_t;

//#define        BOOT_MODE_PATH                                "sys/class/htc_sensorhub/cywee_sensorhub/mcu_mode"
#define BOOT_MODE_PATH "/sys/class/cywee_sensorhub/sensor_hub/mcu_mode"

class CwMcuSensor : public SensorBase {
	InputEventCircularReader mInputReader;
	sensors_event_t mPendingEvents[SENSORS_ID_END];
	sensors_event_t mPendingEvents_flush;
	bool mHasPendingEvent;
    bool mHasPendingFlushEvent[SENSORS_ID_END];
	uint64_t mPendingMask;
    char fixed_sysfs_path[PATH_MAX];
    int fixed_sysfs_path_len;
	int64_t mEnabledTime[SENSORS_ID_END];
	int flush_event;
	int setInitialState();
	int write_to_file(int type,int data);
    char mDevPath[PATH_MAX];
    char mTriggerName[PATH_MAX];

	bool init_trigger_done;
	uint64_t mag_en;

	uint32_t PrePedometerCount[HANDLE_ID_END];
    uint8_t uPackageSensors[SENSORS_ID_END][12];
	uint8_t uPackage[2][24];
/***************************HTC*/
	int64_t	PreEnableTime;
        
	pthread_t sync_time_thread;
    int64_t sync_time_thread_time;
	double	Slope;
	double	PreSlope;
	int64_t	TimeOffset;
	int64_t	TimeOffsetBias;
	int64_t	TimeOffSyncTime;
	CwTime_t McuTime[SENSORS_ID_END];
    int64_t SenPeriodNs[SENSORS_ID_END];
    int64_t SenTimeout[SENSORS_ID_END];
/***************************/
	int sysfs_set_input_attr(char *devpath, const char *attr, char *value, int len);
	int sysfs_set_input_attr_by_int(char *devpath, const char *attr, int value);
public:
	uint64_t mEnabled;
	CwMcuSensor();
	virtual ~CwMcuSensor();
	virtual int readEvents(sensors_event_t* data, int count);
	virtual bool hasPendingEvents() const;
	virtual int setDelay(int32_t handle, int64_t ns);
	virtual int setEnable(int32_t handle, int enabled);
    virtual int getEnable(int32_t handle);
	virtual int batch(int handle, int flags, int64_t period_ns, int64_t timeout);
	virtual int flush(int handle);
	int find_sensor(int handle,int *data);
	int find_handle(int handle,int id);
	int processEvent(uint8_t *event);
	void Uint16FloatToFloat(uint16_t *u16float, float *data);
	int McuInitial(void);
	int check_sensors_type( int id);

	int cw_read_calibrator_file(char *path, int *calib);
	int cw_save_calibrator_file(char *path, int *calib);
	int cw_read_calibrator_sysfs(char *path, int *calib);
	int cw_save_calibrator_sysfs(char *path, int *calib);
	int cw_set_command_sysfs(int cmd,int id, char *path);
	int cw_calibrator(CalibratorCmd cmd, int id);
	int GetMcutimestamp(uint32_t *mcu_time);
	int SyncMcutimestamp(int handle, int id, uint16_t mcu_time);
    int UpdateMcuTimestamp(uint8_t *event);
	int SyncMcuWakeTriggertimestamp(int handle, int id, uint32_t mcu_time);
	int SyncSlopeOffset(void);
	int cw_read_sysfs(char *path, char *data,int size);
	int64_t calculate_offset(int handle,int64_t time);
    int GetMetaData(uint8_t *event);
};

/*****************************************************************************/

#endif  // ANDROID_CWMCU_SENSOR_H
