/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ANDROID_SENSORS_H
#define ANDROID_SENSORS_H

#include <stdint.h>
#include <errno.h>
#include <sys/cdefs.h>
#include <sys/types.h>

#include <linux/input.h>

#include <hardware/hardware.h>
#include <hardware/sensors.h>

__BEGIN_DECLS

/*****************************************************************************/

#define ARRAY_SIZE(a) (sizeof(a) / sizeof(a[0]))


/*****************************************************************************/

/*
 * The SENSORS Module
 */

/* the GP2A is a binary proximity sensor that triggers around 5 cm on
 * this hardware */
#define PROXIMITY_THRESHOLD_GP2A  5.0f

#define CONVERT_A        0.01f
#define CONVERT_M        0.01f
#define CONVERT_GYRO     0.01f
#define CONVERT_PS       1.0f
#define CONVERT_O        0.1f
#define CONVERT_ALL      0.01f
#define CONVERT_PRESSURE 100
#define CONVERT_RV       10000

#define CONVERT_1			1.0f
#define CONVERT_10			0.1f
#define CONVERT_100			0.01f
#define CONVERT_1000		0.001f
#define CONVERT_10000		0.0001f

#define RANGE_A                     (4*GRAVITY_EARTH)

#define SENSOR_STATE_MASK           (0x7FFF)
#define SENSOR_TYPE_PRIVATE_SENSOR_A	5566
/*****************************************************************************/

typedef enum {
    ACCELERATION            =0
    ,MAGNETIC
    ,GYRO
    ,LIGHT
    ,PROXIMITY
    ,PROXIMITY_GESTURE
    ,MOTION
    ,ORIENTATION
    ,ROTATIONVECTOR
    ,LINEARACCELERATION
    ,GRAVITY
    ,MAGNETIC_UNCALIBRATED
    ,GYROSCOPE_UNCALIBRATED
    ,GAME_ROTATION_VECTOR
    ,GEOMAGNETIC_ROTATION_VECTOR
    ,STEP_DETECTOR
    ,STEP_COUNTER
    ,SIGNIFICANT_MOTION
    ,TILT
    ,PDR
    ,STATIC_MOTION
    ,PRIVATE_PEDOMETER
    ,PICKUP
    ,BRUSH
	,ACCELERATION_WAKE_UP
	,MAGNETIC_WAKE_UP
	,GYRO_WAKE_UP
	,ORIENTATION_WAKE_UP
	,ROTATIONVECTOR_WAKE_UP
	,LINEARACCELERATION_WAKE_UP
	,GRAVITY_WAKE_UP
	,MAGNETIC_UNCALIBRATED_WAKE_UP
	,GYROSCOPE_UNCALIBRATED_WAKE_UP
	,GAME_ROTATION_VECTOR_WAKE_UP
	,GEOMAGNETIC_ROTATION_VECTOR_WAKE_UP
	,STEP_DETECTOR_WAKE_UP
	,STEP_COUNTER_WAKE_UP
    ,PRIVATE_PEDOMETER_WAKE_UP
	,SENSORS_ID_END
} SENSORS_ID;

#define 	SPECIAL_ID_START 	100

typedef enum {
	TimestampSync = SPECIAL_ID_START
	,FLASH_DATA
	,META_DATA
	,MAGNETIC_UNCALIBRATED_BIAS
	,GYRO_UNCALIBRATED_BIAS
	,ERROR_MSG
	,BATCH_TIMEOUT
	,BATCH_FULL
	,ACCURACY_UPDATE
	,CALIBRATOR_UPDATE
	,MCU_REINITIAL
	,CW_ABS_TIMEDIFF
	,CW_ABS_ACCURACY
	,CW_ABS_TIMEBASE
}MCU_TO_CPU_EVENT_TYPE;
typedef enum {
    CALIB_EN = 0,
    CALIB_CHECK_STATUS,
    CALIB_DATA_WRITE,
    CALIB_DATA_READ,
    CALIB_FLASH_WRITE,
    CALIB_FLASH_READ,
} CALIBRATOR_CMD;

typedef enum {
	NonWakeUpHandle= 0,
	WakeUpHandle=1,
	InternalHandle=2,
	HANDLE_ID_END
} HANDLE_ID;

__END_DECLS

#endif  // ANDROID_SENSORS_H
