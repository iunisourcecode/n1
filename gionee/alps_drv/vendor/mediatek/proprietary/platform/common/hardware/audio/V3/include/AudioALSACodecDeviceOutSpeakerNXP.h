#ifndef ANDROID_AUDIO_ALSA_CODEC_DEVICE_OUT_SPEAKER_NXP_H
#define ANDROID_AUDIO_ALSA_CODEC_DEVICE_OUT_SPEAKER_NXP_H

#include "AudioType.h"

#include "AudioALSACodecDeviceBase.h"


namespace android
{

class AudioALSACodecDeviceOutSpeakerNXP : public AudioALSACodecDeviceBase
{
    public:
        virtual ~AudioALSACodecDeviceOutSpeakerNXP();

        static AudioALSACodecDeviceOutSpeakerNXP *getInstance();


        /**
         * open/close codec driver
         */
        status_t open();
        status_t open(const uint32_t SampleRate);
        status_t close();
        //Gionee zhangliu 2015-04-18 add for re-calibrate IF for NXP SmartPA by CR01466869,begin
        #if defined(CONFIG_GN_BSP_MTK_NXP_RESET_SUPPORT) && defined(NXP_SMARTPA_SUPPORT)
        status_t resetNxpMtpEx();
        #endif
        //Gionee zhangliu 2014-04-18 add for re-calibrate IF for NXP SmartPA by CR01466869,end



    protected:
        AudioALSACodecDeviceOutSpeakerNXP();



    private:
        /**
         * singleton pattern
         */
        static AudioALSACodecDeviceOutSpeakerNXP *mAudioALSACodecDeviceOutSpeakerNXP;

};

} // end namespace android

#endif // end of ANDROID_AUDIO_ALSA_CODEC_DEVICE_OUT_SPEAKER_NXP_H
