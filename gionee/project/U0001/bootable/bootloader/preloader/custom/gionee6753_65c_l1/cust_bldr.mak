###################################################################
# Include Project Feautre  (cust_bldr.h)
###################################################################

#ifeq ("$(MTK_EMMC_SUPPORT)","yes")
ifdef MTK_EMMC_SUPPORT
CFG_BOOT_DEV :=BOOTDEV_SDMMC
else
CFG_BOOT_DEV :=BOOTDEV_NAND
endif

## Gionee BSP1 yangqb modify for CBL7503 basecode start
CFG_UART_LOG :=UART4
CFG_UART_META :=UART1
## Gionee BSP1 yangqb modify for CBL7503 basecode end
