/*
** =============================================================================
** Copyright (c) 2009-2014  Immersion Corporation. All rights reserved.
**                          Immersion Corporation Confidential and Proprietary.
**
** File:
**     EffectHandle.java
**
** Description:
**     Java EffectHandle class declaration.
**
** Merge:
**    Gionee BSP1 ningyd 20150303 modify for CR01452914 immersion vibrate
** =============================================================================
*/

package com.immersion;

/**
 * Encapsulates an effect handle representing a playing or paused effect.
 */
public class EffectHandle
{
    protected int deviceHandle;
    protected int effectHandle;

    /**
     * Initializes this effect handle object.
     */
    protected EffectHandle(int deviceHandle, int effectHandle)
    {
        this.deviceHandle = deviceHandle;
        this.effectHandle = effectHandle;
    }

    /**
     * Modifies this playing MagSweep effect.
     *
     * @param   definition      Definition of the effect parameters.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>One or more of the arguments is invalid;
     *                                  for example,
     *                                  <ul>
     *                                      <li>The device has been closed by a
     *                                          previous call to
     *                                          {@link Device#close Device.close}.</li>
     *                                      <li>The value of one or more of the
     *                                          effect parameters is
     *                                          invalid.</li>
     *                                  </ul>
     *                              </dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error modifying this effect.</dd>
     *                              <dt>"VIBE_E_INCOMPATIBLE_EFFECT_TYPE"</dt>
     *                              <dd>This effect handle does not represent a
     *                                  MagSweep effect.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public void modifyPlayingMagSweepEffect(MagSweepEffectDefinition definition)
    {
        ImmVibe.getInstance().modifyPlayingMagSweepEffect(deviceHandle,
                                                          effectHandle,
                                                          definition.getDuration(),
                                                          definition.getMagnitude(),
                                                          definition.getStyle(),
                                                          definition.getAttackTime(),
                                                          definition.getAttackLevel(),
                                                          definition.getFadeTime(),
                                                          definition.getFadeLevel());
    }

    /**
     * Modifies this playing Periodic effect.
     *
     * @param   definition      Definition of the effect parameters.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>One or more of the arguments is invalid;
     *                                  for example,
     *                                  <ul>
     *                                      <li>The device has been closed by a
     *                                          previous call to
     *                                          {@link Device#close Device.close}.</li>
     *                                      <li>The value of one or more of the
     *                                          effect parameters is
     *                                          invalid.</li>
     *                                  </ul>
     *                              </dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error modifying this effect.</dd>
     *                              <dt>"VIBE_E_INCOMPATIBLE_EFFECT_TYPE"</dt>
     *                              <dd>This effect handle does not represent a
     *                                  Periodic effect.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public void modifyPlayingPeriodicEffect(PeriodicEffectDefinition definition)
    {
         ImmVibe.getInstance().modifyPlayingPeriodicEffect(deviceHandle,
                                                           effectHandle,
                                                           definition.getDuration(),
                                                           definition.getMagnitude(),
                                                           definition.getPeriod(),
                                                           definition.getStyleAndWaveType(),
                                                           definition.getAttackTime(),
                                                           definition.getAttackLevel(),
                                                           definition.getFadeTime(),
                                                           definition.getFadeLevel());
    }

    /**
     * Appends PCM data to this playing Waveform effect.
     * <p>
     * If this Waveform effect is no longer playing, this method starts playing
     * a new Waveform effect and is equivalent to
     * {@link Device#playWaveformEffect Device.playWaveformEffect}.
     * <p>
     * This function is supported only in the 5000 API edition. To get the
     * API edition level, call
     * {@link Device#getCapabilityInt32 Device.getCapabilityInt32} with the
     * {@link ImmVibe#VIBE_DEVCAPTYPE_EDITION_LEVEL} device capability type.
     *
     * @param   definition      Definition of the effect parameters containing
     *                          the PCM data to append. The sample rate and
     *                          bit depth specified in the effect parameters
     *                          must be the same as the values passed to
     *                          {@link Device.playWaveformEffect Device.playWaveformEffect}
     *                          to play the effect. There may be a performance
     *                          penalty when specifying a different magnitude
     *                          from the value that was passed to
     *                          {@link Device#playWaveformEffect Device.playWaveformEffect}
     *                          to play the Waveform effect.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>One or more of the arguments is invalid;
     *                                  for example,
     *                                  <ul>
     *                                      <li>The device has been closed by a
     *                                          previous call to
     *                                          {@link Device#close Device.close}.</li>
     *                                      <li>The value of one or more of the
     *                                          effect parameters is
     *                                          invalid.</li>
     *                                      <li>The sample rate or bit depth
     *                                          specified in the effect
     *                                          parameters does not match the
     *                                          value that was passed to
     *                                          {@link Device#playWaveformEffect Device.playWaveformEffect}
     *                                          to play the effect.</li>
     *                                  </ul>
     *                              </dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error appending PCM data to the effect.</dd>
     *                              <dt>"VIBE_E_INCOMPATIBLE_EFFECT_TYPE"</dt>
     *                              <dd>This effect is not a Waveform effect.</dd>
     *                              <dt>"VIBE_E_NOT_ENOUGH_MEMORY"</dt>
     *                              <dd>The API cannot allocate memory to
     *                                  complete the request. This happens when
     *                                  the system runs low in memory.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                              <dt>"VIBE_E_NOT_SUPPORTED"</dt>
     *                              <dd>This function is not supported in the
     *                                  3000 and 4000 API editions. To get the
     *                                  API edition level, call
     *                                  {@link Device#getDeviceCapabilityInt32 Device.getDeviceCapabilityInt32}
     *                                  with the
     *                                  {@link Device#VIBE_DEVCAPTYPE_EDITION_LEVEL}
     *                                  device capability type.</dd>
     *                          </dl>
     */
    public void appendWaveformEffect(WaveformEffectDefinition definition)
    {
        effectHandle = ImmVibe.getInstance().appendWaveformEffect(deviceHandle,
                                                                  effectHandle,
                                                                  definition.getData(),
                                                                  definition.getDataSize(),
                                                                  definition.getSampleRate(),
                                                                  definition.getBitDepth(),
                                                                  definition.getMagnitude());
    }

    /**
     * Modifies the playing interpolated effect.
     *
     * @param   interpolant      Interpolant value for the playing interpolated effect with 
     *                           {@link Device#playIVTInterpolatedEffect}.
     *                           The interpolant value must be greater than or equal to 0
     *                           and less than or equal to {@link ImmVibeAPI#VIBE_MAX_INTERPOLANT}.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>One or more of the arguments is invalid;
     *                                  for example,
     *                                  <ul>
     *                                      <li>The device has been closed by a
     *                                          previous call to
     *                                          {@link Device#close Device.close}.</li>
     *                                      <li>The value of one or more of the
     *                                          effect parameters is
     *                                          invalid.</li>
     *                                  </ul>
     *                              </dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error modifying this effect.</dd>
     *                              <dt>"VIBE_E_INCOMPATIBLE_EFFECT_TYPE"</dt>
     *                              <dd>This effect handle does not represent a
     *                                  Periodic effect.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public void modifyPlayingInterpolatedEffectInterpolant(int interpolant)
    {
         ImmVibe.getInstance().modifyPlayingInterpolatedEffectInterpolant(
                 deviceHandle, effectHandle, interpolant);
    }

    /**
     * Retrieves the status of this effect; whether playing, not playing, or paused.
     *
     * @return                  Status of this effect. The effect status is one of
     *                          {@link ImmVibe#VIBE_EFFECT_STATE_NOT_PLAYING},
     *                          {@link ImmVibe#VIBE_EFFECT_STATE_PLAYING}, or
     *                          {@link ImmVibe#VIBE_EFFECT_STATE_PAUSED}.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>The device has been closed by a previous
     *                                  call to
     *                                  {@link Device#close Device.close}.</dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error getting the status of this
     *                                  effect.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public int getState()
    {
        return ImmVibe.getInstance().getEffectState(deviceHandle, effectHandle);
    }

    /**
     * Gets whether this effect is playing.
     *
     * @return                  <code>true</code> if this effect is playing,
     *                          <code>false</code> otherwise.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>The device has been closed by a previous call to
     *                                  {@link Device#close Device.close}.</dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error getting the status of this
     *                                  effect.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public boolean isPlaying()
    {
        return (getState() == ImmVibe.VIBE_EFFECT_STATE_PLAYING);
    }

    /**
     * Gets whether this effect is paused.
     *
     * @return                  <code>true</code> if this effect is paused,
     *                          <code>false</code> otherwise.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>The device has been closed by a previous call to
     *                                  {@link Device#close Device.close}.</dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error getting the status of this
     *                                  effect.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public boolean isPaused()
    {
        return (getState() == ImmVibe.VIBE_EFFECT_STATE_PAUSED);
    }

    /**
     * Pauses this playing effect.
     *
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>One or more of the arguments is invalid;
     *                                  for example,
     *                                  <ul>
     *                                      <li>The device has been closed by a
     *                                          previous call to
     *                                          {@link Device#close Device.close}.</li>
     *                                      <li>This effect is no longer playing.</li>
     *                                  </ul>
     *                              </dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error pausing this effect.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public void pause()
    {
        ImmVibe.getInstance().pausePlayingEffect(deviceHandle, effectHandle);
    }

    /**
     * Resumes this paused effect from the point where it was paused.
     * <p>
     * Depending on how many effects are already playing, it is possible that
     * some simple effects from a paused Timeline effect or Streaming effect
     * could not be resumed. This method will not throw an exception when it
     * cannot resume all of the simple effects.
     *
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>The device has been closed by a previous
     *                                  call to {@link Device#close Device.close}.</dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error resuming this effect.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public void resume()
    {
        ImmVibe.getInstance().resumePausedEffect(deviceHandle, effectHandle);
    }

    /**
     * Stops this playing or paused effect.
     *
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>The device has been closed by a previous
     *                                  call to {@link Device#close Device.close}.</dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error stopping this effect.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public void stop()
    {
        ImmVibe.getInstance().stopPlayingEffect(deviceHandle, effectHandle);
    }

    /**
     * Plays a Streaming Sample.
     *
     * @param   streamingSample Streaming Sample data.
     * @param   size            Streaming Sample data size in bytes. The size
     *                          must be greater than zero and less than or equal
     *                          to
     *                          {@link ImmVibe#VIBE_MAX_STREAMING_SAMPLE_SIZE}.
     *                          The size of the data may be less than the length
     *                          of the <code>streamingSample</code> buffer if
     *                          the same buffer is used to store different
     *                          Streaming Samples of different sizes.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>One or more of the arguments is invalid;
     *                                  for example,
     *                                  <ul>
     *                                      <li>The device has been closed by a
     *                                          previous call to
     *                                          {@link Device#close Device.close}.</li>
     *                                      <li>This effect handle does not
     *                                          represent a Streaming effect.</li>
     *                                      <li>The <code>size</code>
     *                                          parameter less than or equal to
     *                                          zero or greater than
     *                                          {@link ImmVibe#VIBE_MAX_STREAMING_SAMPLE_SIZE}.</li>
     *                                  </ul>
     *                              </dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error playing the Streaming Sample.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public void playStreamingSample(byte[] streamingSample, int size)
    {
        ImmVibe.getInstance().playStreamingSample(deviceHandle, effectHandle, streamingSample, size);
    }

    /**
     * Plays a Streaming Sample with a time offset.
     *
     * @param   streamingSample Streaming Sample data.
     * @param   size            Streaming Sample data size in bytes. The size
     *                          must be greater than zero and less than or equal
     *                          to
     *                          {@link ImmVibe#VIBE_MAX_STREAMING_SAMPLE_SIZE}.
     *                          The size of the data may be less than the length
     *                          of the <code>streamingSample</code> buffer if
     *                          the same buffer is used to store different
     *                          Streaming Samples of different sizes.
     * @param   offsetTime      For <code>offsetTime</code> values that are
     *                          greater than zero, playback is delayed for
     *                          <code>offsetTime</code> in milliseconds. For
     *                          <code>offsetTime</code> values that are less
     *                          than zero, sample playback begins in offset time
     *                          in milliseconds into the Streaming Sample.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>One or more of the arguments is invalid;
     *                                  for example,
     *                                  <ul>
     *                                      <li>The device has been closed by a
     *                                          previous call to
     *                                          {@link Device#close Device.close}.</li>
     *                                      <li>This effect handle does not
     *                                          represent a Streaming effect.</li>
     *                                      <li>The <code>size</code>
     *                                          parameter less than or equal to
     *                                          zero or greater than
     *                                          {@link ImmVibe#VIBE_MAX_STREAMING_SAMPLE_SIZE}.</li>
     *                                  </ul>
     *                              </dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error playing the Streaming Sample.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public void playStreamingSampleWithOffset(byte[] streamingSample, int size, int offsetTime)
    {
        ImmVibe.getInstance().playStreamingSampleWithOffset(deviceHandle, effectHandle, streamingSample, size, offsetTime);
    }

    /**
     * Destroys this Streaming effect.
     *
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>The device has been closed by a previous
     *                                  call to
     *                                  {@link Device#close Device.close}.</dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error destroying this Streaming
     *                                  effect.</dd>
     *                              <dt>"VIBE_E_INCOMPATIBLE_EFFECT_TYPE"</dt>
     *                              <dd>This effect handle does not represent a
     *                                  Streaming effect.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public void destroyStreamingEffect()
    {
        ImmVibe.getInstance().destroyStreamingEffect(deviceHandle, effectHandle);
    }

    /**
     * Appends Enhanced Waveform data to this playing Waveform effect.
     * <p>
     * If this Waveform effect is no longer playing, this method starts playing
     * a new Waveform effect and is equivalent to
     * {@link Device#playEnhancedWaveformEffect Device.playEnhancedWaveformEffect}.
     * <p>
     * This function is supported only with Enhanced Waveform feature. To get the
     * the supported feature of the TouchSense Player, call
     * {@link #getCapabilityInt32 getCapabilityInt32} with the
     * {@link ImmVibeAPI#VIBE_DEVCAPTYPE_SUPPORTED_FEATURES} device capability type.
     *
     * @param   definition      Definition of the effect parameters containing
     *                          the PCM data to append. The sample rate and
     *                          bit depth specified in the effect parameters
     *                          must be the same as the values passed to
     *                          {@link Device.playWaveformEffect Device.playWaveformEffect}
     *                          to play the effect. There may be a performance
     *                          penalty when specifying a different magnitude
     *                          from the value that was passed to
     *                          {@link Device#playWaveformEffect Device.playWaveformEffect}
     *                          to play the Waveform effect.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>One or more of the arguments is invalid;
     *                                  for example,
     *                                  <ul>
     *                                      <li>The device has been closed by a
     *                                          previous call to
     *                                          {@link Device#close Device.close}.</li>
     *                                      <li>The value of one or more of the
     *                                          effect parameters is
     *                                          invalid.</li>
     *                                      <li>The sample rate or bit depth
     *                                          specified in the effect
     *                                          parameters does not match the
     *                                          value that was passed to
     *                                          {@link Device#playWaveformEffect Device.playWaveformEffect}
     *                                          to play the effect.</li>
     *                                  </ul>
     *                              </dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error appending PCM data to the effect.</dd>
     *                              <dt>"VIBE_E_INCOMPATIBLE_EFFECT_TYPE"</dt>
     *                              <dd>This effect is not a Waveform effect.</dd>
     *                              <dt>"VIBE_E_NOT_ENOUGH_MEMORY"</dt>
     *                              <dd>The API cannot allocate memory to
     *                                  complete the request. This happens when
     *                                  the system runs low in memory.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                              <dt>"VIBE_E_NOT_SUPPORTED"</dt>
     *                              <dd>This function is not supported in the
     *                                  3000 and 4000 API editions. To get the
     *                                  API edition level, call
     *                                  {@link Device#getDeviceCapabilityInt32 Device.getDeviceCapabilityInt32}
     *                                  with the
     *                                  {@link Device#VIBE_DEVCAPTYPE_EDITION_LEVEL}
     *                                  device capability type.</dd>
     *                          </dl>
     */
    public void appendEnhancedWaveformEffect(EnhancedWaveformEffectDefinition definition)
    {
        effectHandle = ImmVibe.getInstance().appendEnhancedWaveformEffect(deviceHandle,
                                                                  effectHandle,
                                                                  definition.getData(),
                                                                  definition.getSampleRate(),
                                                                  definition.getFormat(),
                                                                  definition.getMagnitude(),
                                                                  definition.getSecureMode());
    }

    /**
     * Replaces Enhanced Waveform data in this playing Waveform effect.
     * <p>
     * If this Waveform effect is no longer playing, this method starts playing
     * a new Waveform effect and is equivalent to
     * {@link Device#playEnhancedWaveformEffect Device.playEnhancedWaveformEffect}.
     * <p>
     * This function is supported only with Enhanced Waveform feature. To get the
     * the supported feature of the TouchSense Player, call
     * {@link ImmVibeAPI#getCapabilityInt32 getCapabilityInt32} with the
     * {@link ImmVibeAPI#VIBE_DEVCAPTYPE_SUPPORTED_FEATURES} device capability type.
     *
     * @param   definition      Definition of the effect parameters containing
     *                          the PCM data to append. The sample rate and
     *                          bit depth specified in the effect parameters
     *                          must be the same as the values passed to
     *                          {@link Device.playWaveformEffect Device.playWaveformEffect}
     *                          to play the effect. There may be a performance
     *                          penalty when specifying a different magnitude
     *                          from the value that was passed to
     *                          {@link Device#playWaveformEffect Device.playWaveformEffect}
     *                          to play the Waveform effect.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>One or more of the arguments is invalid;
     *                                  for example,
     *                                  <ul>
     *                                      <li>The device has been closed by a
     *                                          previous call to
     *                                          {@link Device#close Device.close}.</li>
     *                                      <li>The value of one or more of the
     *                                          effect parameters is
     *                                          invalid.</li>
     *                                      <li>The sample rate or bit depth
     *                                          specified in the effect
     *                                          parameters does not match the
     *                                          value that was passed to
     *                                          {@link Device#playWaveformEffect Device.playWaveformEffect}
     *                                          to play the effect.</li>
     *                                  </ul>
     *                              </dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error appending PCM data to the effect.</dd>
     *                              <dt>"VIBE_E_INCOMPATIBLE_EFFECT_TYPE"</dt>
     *                              <dd>This effect is not a Waveform effect.</dd>
     *                              <dt>"VIBE_E_NOT_ENOUGH_MEMORY"</dt>
     *                              <dd>The API cannot allocate memory to
     *                                  complete the request. This happens when
     *                                  the system runs low in memory.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                              <dt>"VIBE_E_NOT_SUPPORTED"</dt>
     *                              <dd>This function is not supported in the
     *                                  3000 and 4000 API editions. To get the
     *                                  API edition level, call
     *                                  {@link Device#getDeviceCapabilityInt32 Device.getDeviceCapabilityInt32}
     *                                  with the
     *                                  {@link Device#VIBE_DEVCAPTYPE_EDITION_LEVEL}
     *                                  device capability type.</dd>
     *                          </dl>
     */
    public void replaceEnhancedWaveformEffect(EnhancedWaveformEffectDefinition definition)
    {
        effectHandle = ImmVibe.getInstance().replaceEnhancedWaveformEffect(deviceHandle,
                                                                  effectHandle,
                                                                  definition.getData(),
                                                                  definition.getSampleRate(),
                                                                  definition.getFormat(),
                                                                  definition.getMagnitude(),
                                                                  definition.getSecureMode());
    }

    /**
     * Retrieves the remaining duration of this effect in milliseconds; whether playing,
     * not playing, or paused.
     *
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>The device has been closed by a previous
     *                                  call to
     *                                  {@link Device#close Device.close}.</dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error getting the status of this
     *                                  effect.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public int getRemainingDuration()
    {
        return ImmVibe.getInstance().getEffectRemainingDuration(deviceHandle, effectHandle);
    }

}
