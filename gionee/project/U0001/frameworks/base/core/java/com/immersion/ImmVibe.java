/*
** =============================================================================
** Copyright (c) 2009-2013  Immersion Corporation. All rights reserved.
**                          Immersion Corporation Confidential and Proprietary.
**
** File:
**     ImmVibe
**
** Description: 
**     Java ImmVibe abstract class declaration.
**
** Merge:
**    Gionee BSP1 ningyd 20150303 modify for CR01452914 immersion vibrate
** =============================================================================
*/

package com.immersion;

/**
 * Abstract base class for classes that implement the {@link ImmVibeAPI}
 * interface. This class defines a {@link #getInstance getInstance} method to
 * get the single instance of the class.
 */
public abstract class ImmVibe implements ImmVibeAPI
{
    static private ImmVibe sInstance = null;
    
    /**
     * Returns an instance of an ImmVibe by specifying its package name
     */
    private static ImmVibe newImmVibeInstanceForName(String immVibeClassName)
    {
        ImmVibe immVibe = null;
        
        try
        {
            Class t = Class.forName(immVibeClassName);
            
            immVibe = (ImmVibe)t.newInstance();
        }
        catch(Exception e)
        {
            // Ignore ClassNotFoundException - return null instead.
        }
        return immVibe;
    }
    
    /**
     * Returns an appropriate instance of an ImmVibe class for the active platform
     */
    private static ImmVibe newImmVibeInstance()
    {
        ImmVibe immVibe = null;
        
        immVibe = newImmVibeInstanceForName("com.immersion.android.ImmVibe");

        if (immVibe == null)
        {
            immVibe = newImmVibeInstanceForName("com.immersion.J2ME.ImmVibe");
        }
        return immVibe;
    }
    /**
     * Returns the single instance of the class.
     */
    public static ImmVibe getInstance()
    {
        ImmVibe result = null;
         
        if (sInstance == null) 
        {
            result = newImmVibeInstance();
            
            if (result == null)
            {
                throw new java.lang.RuntimeException("VIBE_E_FAIL");
            }
            result.initialize();
            sInstance = result;
        }
        else
        {
            result = sInstance;
        }
        return result;
    }
    
    /**
     * Resets the single instance of the class.
     */
    public void terminate()
    {
        sInstance = null;
    }
}
