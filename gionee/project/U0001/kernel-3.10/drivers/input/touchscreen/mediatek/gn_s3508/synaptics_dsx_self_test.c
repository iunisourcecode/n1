/*
 * Synaptics DSX touchscreen driver
 *
 * Copyright (C) 2012 Synaptics Incorporated
 *
 * Copyright (C) 2012 Alexandra Chin <alexandra.chin@tw.synaptics.com>
 * Copyright (C) 2012 Scott Lin <scott.lin@tw.synaptics.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/i2c.h>
#include <linux/interrupt.h>
#include <linux/delay.h>
#include <linux/input.h>
#include <linux/ctype.h>
#include <linux/hrtimer.h>
#include "synaptics_dsx.h"
#include "synaptics_dsx_core.h"
#include "synaptics_dsx_self_test.h"

/*
#define RAW_HEX
#define HUMAN_READABLE
*/

#define HAVE_0D_TOUCH_KEY
#ifdef HAVE_0D_TOUCH_KEY
#define KEY_RX_COUNT 1
#define KEY_TX_COUNT 3
#endif

#define CFG_F54_TXCOUNT 19
#define CFG_F54_RXCOUNT 29

#define F54_HR_RX_OFFSET 400
#define F54_HR_TX_OFFSET 400
#define F54_HR_MIN       -400

#define RAW_DATA_MAX 1200
#define RAW_DATA_MIN 600

static short UpperLimit[17][30] ={	
1457,1447,1356,1347,1338,1356,1317,1304,1314,1288,1275,1266,1255,1246,1239,1232,1224,1221,1216,1212,1209,1205,1199,1202,1201,1203,1205,1209,1214,1256,
1397,1382,1372,1381,1356,1348,1334,1322,1314,1304,1290,1280,1271,1261,1251,1245,1237,1233,1226,1222,1218,1212,1205,1205,1202,1202,1202,1201,1201,1195,
1386,1367,1386,1352,1344,1352,1323,1310,1303,1291,1277,1267,1259,1249,1240,1233,1225,1222,1215,1209,1205,1200,1193,1194,1189,1188,1189,1188,1186,1173,
1352,1362,1353,1348,1355,1330,1316,1302,1295,1284,1270,1261,1251,1241,1232,1225,1216,1212,1207,1201,1195,1191,1184,1183,1180,1179,1179,1178,1177,1155,
1356,1358,1349,1343,1335,1325,1312,1298,1288,1278,1263,1253,1244,1233,1225,1217,1208,1220,1197,1192,1187,1182,1174,1175,1171,1172,1170,1170,1172,1142,
1358,1354,1345,1338,1330,1318,1305,1290,1281,1271,1255,1244,1234,1223,1215,1206,1197,1193,1187,1198,1177,1171,1165,1165,1159,1161,1160,1160,1159,1129,
1358,1353,1343,1334,1325,1314,1315,1285,1275,1264,1247,1236,1226,1215,1206,1197,1188,1184,1177,1172,1167,1161,1153,1154,1151,1151,1150,1151,1152,1116,
1358,1350,1340,1332,1321,1310,1295,1278,1269,1256,1240,1229,1218,1205,1197,1187,1178,1175,1168,1162,1174,1152,1144,1144,1146,1144,1144,1145,1140,1105,
1369,1349,1338,1330,1318,1311,1291,1274,1265,1251,1234,1238,1211,1199,1190,1181,1171,1167,1159,1155,1150,1143,1136,1138,1134,1135,1137,1135,1133,1095,
1360,1346,1336,1327,1315,1302,1285,1269,1259,1244,1228,1215,1204,1191,1182,1173,1163,1159,1167,1146,1141,1135,1129,1129,1126,1126,1126,1125,1124,1085,
1360,1346,1334,1324,1312,1298,1281,1263,1254,1239,1221,1209,1199,1185,1174,1166,1155,1152,1144,1139,1134,1145,1122,1124,1119,1118,1119,1119,1116,1076,
1362,1344,1332,1323,1308,1296,1278,1261,1250,1250,1217,1205,1193,1179,1170,1159,1150,1146,1138,1133,1128,1123,1117,1117,1115,1113,1114,1114,1111,1069,
1364,1342,1330,1320,1307,1292,1275,1257,1245,1231,1213,1200,1194,1174,1165,1155,1144,1141,1133,1127,1123,1118,1111,1129,1109,1109,1110,1109,1107,1063,
1370,1343,1331,1320,1306,1293,1274,1255,1244,1229,1211,1197,1185,1171,1162,1151,1142,1138,1130,1125,1121,1115,1108,1110,1123,1107,1107,1108,1105,1058,
1380,1349,1335,1324,1310,1296,1277,1258,1246,1230,1211,1198,1184,1172,1162,1152,1142,1139,1131,1130,1122,1116,1127,1111,1108,1109,1109,1109,1107,1058,
1404,1364,1366,1337,1323,1321,1288,1269,1257,1241,1222,1206,1194,1179,1169,1175,1150,1147,1140,1136,1131,1125,1119,1120,1117,1118,1119,1120,1120,1093,
1552,1440,1423,1409,1393,1384,1356,1338,1323,1306,1302,1270,1256,1241,1231,1220,1211,1206,1199,1194,1189,1184,1177,1178,1176,1175,1175,1176,1175,1125,
};

static short LowerLimit[17][30] = {	
785, 789, 730, 725, 720, 725, 709, 702, 708, 694, 687, 682, 676, 671, 667, 663, 659, 658, 655, 653, 651, 649, 646, 647, 647, 648, 649, 651, 654, 671,
752, 744, 739, 743, 730, 726, 718, 712, 708, 702, 695, 690, 685, 679, 674, 671, 666, 664, 660, 658, 656, 653, 649, 649, 647, 647, 647, 646, 646, 643,
746, 736, 731, 728, 723, 727, 712, 705, 701, 695, 687, 682, 677, 672, 667, 663, 659, 658, 654, 651, 649, 646, 642, 642, 640, 639, 640, 639, 638, 631,
728, 733, 728, 725, 729, 716, 708, 701, 697, 691, 684, 678, 673, 668, 663, 659, 654, 653, 650, 646, 643, 641, 637, 637, 635, 635, 634, 634, 633, 622,
730, 731, 726, 723, 718, 713, 706, 698, 693, 688, 680, 674, 669, 664, 659, 655, 650, 657, 644, 642, 639, 636, 632, 633, 630, 631, 630, 630, 631, 615,
731, 729, 724, 720, 716, 710, 703, 695, 690, 684, 676, 670, 664, 659, 654, 649, 644, 642, 639, 645, 633, 630, 627, 627, 624, 625, 625, 625, 624, 607,
731, 728, 723, 718, 713, 707, 708, 692, 686, 680, 671, 666, 660, 654, 649, 644, 640, 637, 634, 631, 628, 625, 621, 621, 619, 619, 618, 618, 619, 601,
731, 727, 721, 717, 711, 705, 697, 688, 683, 676, 668, 662, 656, 649, 644, 639, 634, 632, 628, 626, 632, 620, 616, 616, 617, 616, 616, 616, 614, 594,
737, 726, 720, 716, 709, 706, 695, 686, 681, 673, 664, 666, 652, 645, 640, 636, 630, 628, 624, 622, 619, 615, 612, 612, 609, 609, 610, 609, 608, 589,
732, 724, 719, 714, 708, 701, 692, 683, 677, 670, 661, 654, 648, 641, 637, 631, 626, 624, 628, 617, 614, 611, 608, 608, 606, 606, 606, 606, 605, 584, 
732, 724, 718, 713, 706, 699, 689, 680, 675, 667, 657, 651, 645, 638, 632, 627, 622, 620, 616, 613, 610, 616, 604, 605, 602, 602, 602, 602, 601, 579,
733, 723, 717, 712, 704, 698, 688, 679, 673, 673, 655, 648, 642, 635, 630, 624, 619, 617, 612, 610, 607, 605, 601, 601, 600, 599, 600, 599, 598, 575,
734, 723, 716, 711, 704, 695, 686, 676, 670, 662, 653, 646, 643, 632, 627, 622, 616, 614, 610, 607, 604, 602, 598, 607, 597, 597, 597, 597, 596, 572,
738, 723, 716, 711, 703, 696, 686, 675, 670, 662, 652, 644, 638, 630, 626, 620, 615, 613, 608, 605, 603, 600, 597, 597, 604, 596, 596, 596, 595, 570,
743, 726, 719, 713, 705, 698, 687, 677, 670, 662, 652, 645, 638, 631, 625, 620, 614, 613, 609, 608, 604, 601, 606, 598, 596, 597, 597, 597, 596, 569,
756, 734, 735, 720, 712, 711, 693, 683, 677, 668, 658, 649, 643, 635, 629, 632, 619, 617, 613, 611, 609, 606, 602, 603, 601, 602, 603, 603, 603, 589, 
835, 775, 766, 758, 750, 750, 730, 720, 712, 703, 701, 683, 676, 668, 663, 657, 652, 649, 645, 643, 640, 637, 634, 634, 633, 633, 633, 633, 632, 605, 
};




static struct rim4_f54_data *f54_data = NULL;
static short ImageArray[CFG_F54_TXCOUNT][CFG_F54_RXCOUNT];
static short HighResistance[HIGH_RESISTANCE_DATA_SIZE/2];
static void free_control_mem(void)
{
	struct f54_control control = f54_data->control;

	kfree(control.reg_0);
	kfree(control.reg_1);
	kfree(control.reg_2);
	kfree(control.reg_3);
	kfree(control.reg_4__6);
	kfree(control.reg_7);
	kfree(control.reg_8__9);
	kfree(control.reg_10);
	kfree(control.reg_11);
	kfree(control.reg_12__13);
	kfree(control.reg_14);
	kfree(control.reg_15);
	kfree(control.reg_16);
	kfree(control.reg_17);
	kfree(control.reg_18);
	kfree(control.reg_19);
	kfree(control.reg_20);
	kfree(control.reg_21);
	kfree(control.reg_22__26);
	kfree(control.reg_27);
	kfree(control.reg_28);
	kfree(control.reg_29);
	kfree(control.reg_30);
	kfree(control.reg_31);
	kfree(control.reg_32__35);
	kfree(control.reg_36);
	kfree(control.reg_37);
	kfree(control.reg_38);
	kfree(control.reg_39);
	kfree(control.reg_40);
	kfree(control.reg_41);
	kfree(control.reg_57);

	return;
}

static int synaptics_rmi4_f54_set_ctrl(void)
{
	unsigned char length;
	unsigned char reg_num = 0;
	unsigned char num_of_sensing_freqs;
	unsigned short reg_addr = f54_data->control_base_addr;
	struct f54_control *control = &f54_data->control;
	struct synaptics_rmi4_data *rmi4_data = f54_data->rmi4_data;

	num_of_sensing_freqs = f54_data->query.number_of_sensing_frequencies;

	/* control 0 */
	//attrs_ctrl_regs_exist[reg_num] = true;
	control->reg_0 = kzalloc(sizeof(*(control->reg_0)),
			GFP_KERNEL);
	if (!control->reg_0)
		goto exit_no_mem;
	control->reg_0->address = reg_addr;
	reg_addr += sizeof(control->reg_0->data);
	reg_num++;

	/* control 1 */
	if ((f54_data->query.touch_controller_family == 0) ||
			(f54_data->query.touch_controller_family == 1)) {
		//attrs_ctrl_regs_exist[reg_num] = true;
		control->reg_1 = kzalloc(sizeof(*(control->reg_1)),
				GFP_KERNEL);
		if (!control->reg_1)
			goto exit_no_mem;
		control->reg_1->address = reg_addr;
		reg_addr += sizeof(control->reg_1->data);
	}
	reg_num++;

	/* control 2 */
	//attrs_ctrl_regs_exist[reg_num] = true;
	control->reg_2 = kzalloc(sizeof(*(control->reg_2)),
			GFP_KERNEL);
	if (!control->reg_2)
		goto exit_no_mem;
	control->reg_2->address = reg_addr;
	reg_addr += sizeof(control->reg_2->data);
	reg_num++;

	/* control 3 */
	if (f54_data->query.has_pixel_touch_threshold_adjustment == 1) {
		//attrs_ctrl_regs_exist[reg_num] = true;
		control->reg_3 = kzalloc(sizeof(*(control->reg_3)),
				GFP_KERNEL);
		if (!control->reg_3)
			goto exit_no_mem;
		control->reg_3->address = reg_addr;
		reg_addr += sizeof(control->reg_3->data);
	}
	reg_num++;

	/* controls 4 5 6 */
	if ((f54_data->query.touch_controller_family == 0) ||
			(f54_data->query.touch_controller_family == 1)) {
		//attrs_ctrl_regs_exist[reg_num] = true;
		control->reg_4__6 = kzalloc(sizeof(*(control->reg_4__6)),
				GFP_KERNEL);
		if (!control->reg_4__6)
			goto exit_no_mem;
		control->reg_4__6->address = reg_addr;
		reg_addr += sizeof(control->reg_4__6->data);
	}
	reg_num++;

	/* control 7 */
	if (f54_data->query.touch_controller_family == 1) {
		//attrs_ctrl_regs_exist[reg_num] = true;
		control->reg_7 = kzalloc(sizeof(*(control->reg_7)),
				GFP_KERNEL);
		if (!control->reg_7)
			goto exit_no_mem;
		control->reg_7->address = reg_addr;
		reg_addr += sizeof(control->reg_7->data);
	}
	reg_num++;

	/* controls 8 9 */
	if ((f54_data->query.touch_controller_family == 0) ||
			(f54_data->query.touch_controller_family == 1)) {
		//attrs_ctrl_regs_exist[reg_num] = true;
		control->reg_8__9 = kzalloc(sizeof(*(control->reg_8__9)),
				GFP_KERNEL);
		if (!control->reg_8__9)
			goto exit_no_mem;
		control->reg_8__9->address = reg_addr;
		reg_addr += sizeof(control->reg_8__9->data);
	}
	reg_num++;

	/* control 10 */
	if (f54_data->query.has_interference_metric == 1) {
		//attrs_ctrl_regs_exist[reg_num] = true;
		control->reg_10 = kzalloc(sizeof(*(control->reg_10)),
				GFP_KERNEL);
		if (!control->reg_10)
			goto exit_no_mem;
		control->reg_10->address = reg_addr;
		reg_addr += sizeof(control->reg_10->data);
	}
	reg_num++;

	/* control 11 */
	if (f54_data->query.has_ctrl11 == 1) {
		//attrs_ctrl_regs_exist[reg_num] = true;
		control->reg_11 = kzalloc(sizeof(*(control->reg_11)),
				GFP_KERNEL);
		if (!control->reg_11)
			goto exit_no_mem;
		control->reg_11->address = reg_addr;
		reg_addr += sizeof(control->reg_11->data);
	}
	reg_num++;

	/* controls 12 13 */
	if (f54_data->query.has_relaxation_control == 1) {
		//attrs_ctrl_regs_exist[reg_num] = true;
		control->reg_12__13 = kzalloc(sizeof(*(control->reg_12__13)),
				GFP_KERNEL);
		if (!control->reg_12__13)
			goto exit_no_mem;
		control->reg_12__13->address = reg_addr;
		reg_addr += sizeof(control->reg_12__13->data);
	}
	reg_num++;

	/* controls 14 15 16 */
	if (f54_data->query.has_sensor_assignment == 1) {
		//attrs_ctrl_regs_exist[reg_num] = true;

		control->reg_14 = kzalloc(sizeof(*(control->reg_14)),
				GFP_KERNEL);
		if (!control->reg_14)
			goto exit_no_mem;
		control->reg_14->address = reg_addr;
		reg_addr += sizeof(control->reg_14->data);

		control->reg_15 = kzalloc(sizeof(*(control->reg_15)),
				GFP_KERNEL);
		if (!control->reg_15)
			goto exit_no_mem;
		control->reg_15->length = f54_data->query.num_of_rx_electrodes;
		control->reg_15->data = kzalloc(control->reg_15->length *
				sizeof(*(control->reg_15->data)), GFP_KERNEL);
		if (!control->reg_15->data)
			goto exit_no_mem;
		control->reg_15->address = reg_addr;
		reg_addr += control->reg_15->length;

		control->reg_16 = kzalloc(sizeof(*(control->reg_16)),
				GFP_KERNEL);
		if (!control->reg_16)
			goto exit_no_mem;
		control->reg_16->length = f54_data->query.num_of_tx_electrodes;
		control->reg_16->data = kzalloc(control->reg_16->length *
				sizeof(*(control->reg_16->data)), GFP_KERNEL);
		if (!control->reg_16->data)
			goto exit_no_mem;
		control->reg_16->address = reg_addr;
		reg_addr += control->reg_16->length;
	}
	reg_num++;

	/* controls 17 18 19 */
	if (f54_data->query.has_sense_frequency_control == 1) {
		//attrs_ctrl_regs_exist[reg_num] = true;

		length = num_of_sensing_freqs;

		control->reg_17 = kzalloc(sizeof(*(control->reg_17)),
				GFP_KERNEL);
		if (!control->reg_17)
			goto exit_no_mem;
		control->reg_17->length = length;
		control->reg_17->data = kzalloc(length *
				sizeof(*(control->reg_17->data)), GFP_KERNEL);
		if (!control->reg_17->data)
			goto exit_no_mem;
		control->reg_17->address = reg_addr;
		reg_addr += length;

		control->reg_18 = kzalloc(sizeof(*(control->reg_18)),
				GFP_KERNEL);
		if (!control->reg_18)
			goto exit_no_mem;
		control->reg_18->length = length;
		control->reg_18->data = kzalloc(length *
				sizeof(*(control->reg_18->data)), GFP_KERNEL);
		if (!control->reg_18->data)
			goto exit_no_mem;
		control->reg_18->address = reg_addr;
		reg_addr += length;

		control->reg_19 = kzalloc(sizeof(*(control->reg_19)),
				GFP_KERNEL);
		if (!control->reg_19)
			goto exit_no_mem;
		control->reg_19->length = length;
		control->reg_19->data = kzalloc(length *
				sizeof(*(control->reg_19->data)), GFP_KERNEL);
		if (!control->reg_19->data)
			goto exit_no_mem;
		control->reg_19->address = reg_addr;
		reg_addr += length;
	}
	reg_num++;

	/* control 20 */
	//attrs_ctrl_regs_exist[reg_num] = true;
	control->reg_20 = kzalloc(sizeof(*(control->reg_20)),
			GFP_KERNEL);
	if (!control->reg_20)
		goto exit_no_mem;
	control->reg_20->address = reg_addr;
	reg_addr += sizeof(control->reg_20->data);
	reg_num++;

	/* control 21 */
	if (f54_data->query.has_sense_frequency_control == 1) {
		//attrs_ctrl_regs_exist[reg_num] = true;
		control->reg_21 = kzalloc(sizeof(*(control->reg_21)),
				GFP_KERNEL);
		if (!control->reg_21)
			goto exit_no_mem;
		control->reg_21->address = reg_addr;
		reg_addr += sizeof(control->reg_21->data);
	}
	reg_num++;

	/* controls 22 23 24 25 26 */
	if (f54_data->query.has_firmware_noise_mitigation == 1) {
		//attrs_ctrl_regs_exist[reg_num] = true;
		control->reg_22__26 = kzalloc(sizeof(*(control->reg_22__26)),
				GFP_KERNEL);
		if (!control->reg_22__26)
			goto exit_no_mem;
		control->reg_22__26->address = reg_addr;
		reg_addr += sizeof(control->reg_22__26->data);
	}
	reg_num++;

	/* control 27 */
	if (f54_data->query.has_iir_filter == 1) {
		//attrs_ctrl_regs_exist[reg_num] = true;
		control->reg_27 = kzalloc(sizeof(*(control->reg_27)),
				GFP_KERNEL);
		if (!control->reg_27)
			goto exit_no_mem;
		control->reg_27->address = reg_addr;
		reg_addr += sizeof(control->reg_27->data);
	}
	reg_num++;

	/* control 28 */
	if (f54_data->query.has_firmware_noise_mitigation == 1) {
		//attrs_ctrl_regs_exist[reg_num] = true;
		control->reg_28 = kzalloc(sizeof(*(control->reg_28)),
				GFP_KERNEL);
		if (!control->reg_28)
			goto exit_no_mem;
		control->reg_28->address = reg_addr;
		reg_addr += sizeof(control->reg_28->data);
	}
	reg_num++;

	/* control 29 */
	if (f54_data->query.has_cmn_removal == 1) {
		//attrs_ctrl_regs_exist[reg_num] = true;
		control->reg_29 = kzalloc(sizeof(*(control->reg_29)),
				GFP_KERNEL);
		if (!control->reg_29)
			goto exit_no_mem;
		control->reg_29->address = reg_addr;
		reg_addr += sizeof(control->reg_29->data);
	}
	reg_num++;

	/* control 30 */
	if (f54_data->query.has_cmn_maximum == 1) {
		//attrs_ctrl_regs_exist[reg_num] = true;
		control->reg_30 = kzalloc(sizeof(*(control->reg_30)),
				GFP_KERNEL);
		if (!control->reg_30)
			goto exit_no_mem;
		control->reg_30->address = reg_addr;
		reg_addr += sizeof(control->reg_30->data);
	}
	reg_num++;

	/* control 31 */
	if (f54_data->query.has_touch_hysteresis == 1) {
		//attrs_ctrl_regs_exist[reg_num] = true;
		control->reg_31 = kzalloc(sizeof(*(control->reg_31)),
				GFP_KERNEL);
		if (!control->reg_31)
			goto exit_no_mem;
		control->reg_31->address = reg_addr;
		reg_addr += sizeof(control->reg_31->data);
	}
	reg_num++;

	/* controls 32 33 34 35 */
	if (f54_data->query.has_edge_compensation == 1) {
		//attrs_ctrl_regs_exist[reg_num] = true;
		control->reg_32__35 = kzalloc(sizeof(*(control->reg_32__35)),
				GFP_KERNEL);
		if (!control->reg_32__35)
			goto exit_no_mem;
		control->reg_32__35->address = reg_addr;
		reg_addr += sizeof(control->reg_32__35->data);
	}
	reg_num++;

	/* control 36 */
	if ((f54_data->query.curve_compensation_mode == 1) ||
			(f54_data->query.curve_compensation_mode == 2)) {
		//attrs_ctrl_regs_exist[reg_num] = true;

		if (f54_data->query.curve_compensation_mode == 1) {
			length = max(f54_data->query.num_of_rx_electrodes,
					f54_data->query.num_of_tx_electrodes);
		} else if (f54_data->query.curve_compensation_mode == 2) {
			length = f54_data->query.num_of_rx_electrodes;
		}

		control->reg_36 = kzalloc(sizeof(*(control->reg_36)),
				GFP_KERNEL);
		if (!control->reg_36)
			goto exit_no_mem;
		control->reg_36->length = length;
		control->reg_36->data = kzalloc(length *
				sizeof(*(control->reg_36->data)), GFP_KERNEL);
		if (!control->reg_36->data)
			goto exit_no_mem;
		control->reg_36->address = reg_addr;
		reg_addr += length;
	}
	reg_num++;

	/* control 37 */
	if (f54_data->query.curve_compensation_mode == 2) {
		//attrs_ctrl_regs_exist[reg_num] = true;

		control->reg_37 = kzalloc(sizeof(*(control->reg_37)),
				GFP_KERNEL);
		if (!control->reg_37)
			goto exit_no_mem;
		control->reg_37->length = f54_data->query.num_of_tx_electrodes;
		control->reg_37->data = kzalloc(control->reg_37->length *
				sizeof(*(control->reg_37->data)), GFP_KERNEL);
		if (!control->reg_37->data)
			goto exit_no_mem;

		control->reg_37->address = reg_addr;
		reg_addr += control->reg_37->length;
	}
	reg_num++;

	/* controls 38 39 40 */
	if (f54_data->query.has_per_frequency_noise_control == 1) {
		//attrs_ctrl_regs_exist[reg_num] = true;

		control->reg_38 = kzalloc(sizeof(*(control->reg_38)),
				GFP_KERNEL);
		if (!control->reg_38)
			goto exit_no_mem;
		control->reg_38->length = num_of_sensing_freqs;
		control->reg_38->data = kzalloc(control->reg_38->length *
				sizeof(*(control->reg_38->data)), GFP_KERNEL);
		if (!control->reg_38->data)
			goto exit_no_mem;
		control->reg_38->address = reg_addr;
		reg_addr += control->reg_38->length;

		control->reg_39 = kzalloc(sizeof(*(control->reg_39)),
				GFP_KERNEL);
		if (!control->reg_39)
			goto exit_no_mem;
		control->reg_39->length = num_of_sensing_freqs;
		control->reg_39->data = kzalloc(control->reg_39->length *
				sizeof(*(control->reg_39->data)), GFP_KERNEL);
		if (!control->reg_39->data)
			goto exit_no_mem;
		control->reg_39->address = reg_addr;
		reg_addr += control->reg_39->length;

		control->reg_40 = kzalloc(sizeof(*(control->reg_40)),
				GFP_KERNEL);
		if (!control->reg_40)
			goto exit_no_mem;
		control->reg_40->length = num_of_sensing_freqs;
		control->reg_40->data = kzalloc(control->reg_40->length *
				sizeof(*(control->reg_40->data)), GFP_KERNEL);
		if (!control->reg_40->data)
			goto exit_no_mem;
		control->reg_40->address = reg_addr;
		reg_addr += control->reg_40->length;
	}
	reg_num++;

	/* control 41 */
	if (f54_data->query.has_signal_clarity == 1) {
		//attrs_ctrl_regs_exist[reg_num] = true;
		control->reg_41 = kzalloc(sizeof(*(control->reg_41)),
				GFP_KERNEL);
		if (!control->reg_41)
			goto exit_no_mem;
		control->reg_41->address = reg_addr;
		reg_addr += sizeof(control->reg_41->data);
	}
	reg_num++;

	/* control 42 */
	if (f54_data->query.has_variance_metric == 1)
		reg_addr += CONTROL_42_SIZE;

	/* controls 43 44 45 46 47 48 49 50 51 52 53 54 */
	if (f54_data->query.has_multi_metric_state_machine == 1)
		reg_addr += CONTROL_43_54_SIZE;

	/* controls 55 56 */
	if (f54_data->query.has_0d_relaxation_control == 1)
		reg_addr += CONTROL_55_56_SIZE;

	/* control 57 */
	if (f54_data->query.has_0d_acquisition_control == 1) {
		//attrs_ctrl_regs_exist[reg_num] = true;
		control->reg_57 = kzalloc(sizeof(*(control->reg_57)),
				GFP_KERNEL);
		if (!control->reg_57)
			goto exit_no_mem;
		control->reg_57->address = reg_addr;
		reg_addr += sizeof(control->reg_57->data);
	}
	reg_num++;

	/* control 58 */
	if (f54_data->query.has_0d_acquisition_control == 1)
		reg_addr += CONTROL_58_SIZE;

	/* control 59 */
	if (f54_data->query.has_h_blank == 1)
		reg_addr += CONTROL_59_SIZE;

	/* controls 60 61 62 */
	if ((f54_data->query.has_h_blank == 1) ||
			(f54_data->query.has_v_blank == 1) ||
			(f54_data->query.has_long_h_blank == 1))
		reg_addr += CONTROL_60_62_SIZE;

	/* control 63 */
	if ((f54_data->query.has_h_blank == 1) ||
			(f54_data->query.has_v_blank == 1) ||
			(f54_data->query.has_long_h_blank == 1) ||
			(f54_data->query.has_slew_metric == 1) ||
			(f54_data->query.has_slew_option == 1) ||
			(f54_data->query.has_noise_mitigation2 == 1))
		reg_addr += CONTROL_63_SIZE;

	/* controls 64 65 66 67 */
	if (f54_data->query.has_h_blank == 1)
		reg_addr += CONTROL_64_67_SIZE * 7;
	else if ((f54_data->query.has_v_blank == 1) ||
			(f54_data->query.has_long_h_blank == 1))
		reg_addr += CONTROL_64_67_SIZE;

	/* controls 68 69 70 71 72 73 */
	if ((f54_data->query.has_h_blank == 1) ||
			(f54_data->query.has_v_blank == 1) ||
			(f54_data->query.has_long_h_blank == 1))
		reg_addr += CONTROL_68_73_SIZE;

	/* control 74 */
	if (f54_data->query.has_slew_metric == 1)
		reg_addr += CONTROL_74_SIZE;

	/* control 75 */
	if (f54_data->query.has_enhanced_stretch == 1)
		reg_addr += num_of_sensing_freqs;

	/* control 76 */
	if (f54_data->query.has_startup_fast_relaxation == 1)
		reg_addr += CONTROL_76_SIZE;

	/* controls 77 78 */
	if (f54_data->query.has_esd_control == 1)
		reg_addr += CONTROL_77_78_SIZE;

	/* controls 79 80 81 82 83 */
	if (f54_data->query.has_noise_mitigation2 == 1)
		reg_addr += CONTROL_79_83_SIZE;

	/* controls 84 85 */
	if (f54_data->query.has_energy_ratio_relaxation == 1)
		reg_addr += CONTROL_84_85_SIZE;

	/* control 86 */
	if ((f54_data->query.has_query13 == 1) && (f54_data->query.has_ctrl86 == 1))
		reg_addr += CONTROL_86_SIZE;

	/* control 87 */
	if ((f54_data->query.has_query13 == 1) && (f54_data->query.has_ctrl87 == 1))
		reg_addr += CONTROL_87_SIZE;

	/* control 88 */
	if (f54_data->query.has_ctrl88 == 1) {
		control->reg_88 = kzalloc(sizeof(*(control->reg_88)),
				GFP_KERNEL);
		if (!control->reg_88)
			goto exit_no_mem;
		control->reg_88->address = reg_addr;
		reg_addr += sizeof(control->reg_88->data);
	}

	return 0;

exit_no_mem:
	printk("%s: Failed to alloc mem for control registers\n",__func__);
	return -ENOMEM;
}

static int do_preparation(void)
{
	int retval;
	unsigned char value;
	unsigned char command;
	unsigned char timeout_count;
	struct synaptics_rmi4_data *rmi4_data = f54_data->rmi4_data;

	mutex_lock(&f54_data->control_mutex);

	if (f54_data->query.touch_controller_family == 1) {
		value = 0;
		retval = synaptics_rmi4_reg_write(rmi4_data,
				f54_data->control.reg_7->address,
				&value,
				sizeof(f54_data->control.reg_7->data));
		if (retval < 0) {
			printk("%s: Failed to disable CBC\n",__func__);
			mutex_unlock(&f54_data->control_mutex);
			return retval;
		}
	} else if (f54_data->query.has_ctrl88 == 1) {
		retval = synaptics_rmi4_reg_read(rmi4_data,
				f54_data->control.reg_88->address,
				f54_data->control.reg_88->data,
				sizeof(f54_data->control.reg_88->data));
		if (retval < 0) {
			printk("%s: Failed to disable CBC (read ctrl88)\n",__func__);
			mutex_unlock(&f54_data->control_mutex);
			return retval;
		}
		f54_data->control.reg_88->cbc_polarity = 0;
		f54_data->control.reg_88->cbc_tx_carrier_selection = 0;
		retval = synaptics_rmi4_reg_write(rmi4_data,
				f54_data->control.reg_88->address,
				f54_data->control.reg_88->data,
				sizeof(f54_data->control.reg_88->data));
		if (retval < 0) {
			printk("%s: Failed to disable CBC (write ctrl88)\n",__func__);
			mutex_unlock(&f54_data->control_mutex);
			return retval;
		}
	}

	if (f54_data->query.has_0d_acquisition_control) {
		value = 0;
		retval = synaptics_rmi4_reg_write(rmi4_data,
				f54_data->control.reg_57->address,
				&value,
				sizeof(f54_data->control.reg_57->data));
		if (retval < 0) {
			printk("%s: Failed to disable 0D CBC\n",__func__);
			mutex_unlock(&f54_data->control_mutex);
			return retval;
		}
	}

	if (f54_data->query.has_signal_clarity) {
		value = 1;
		retval = synaptics_rmi4_reg_write(rmi4_data,
				f54_data->control.reg_41->address,
				&value,
				sizeof(f54_data->control.reg_41->data));
		if (retval < 0) {
			printk("%s: Failed to disable signal clarity\n",__func__);
			mutex_unlock(&f54_data->control_mutex);
			return retval;
		}
	}

	mutex_unlock(&f54_data->control_mutex);

	command = (unsigned char)COMMAND_FORCE_UPDATE;

	retval = synaptics_rmi4_reg_write(rmi4_data,
			f54_data->command_base_addr,
			&command,
			sizeof(command));
	if (retval < 0) {
		printk("%s: Failed to write force update command\n",__func__);
		return retval;
	}

	timeout_count = 0;
	do {
		retval = synaptics_rmi4_reg_read(rmi4_data,
				f54_data->command_base_addr,
				&value,
				sizeof(value));
		if (retval < 0) {
			printk("%s: Failed to read command register\n",__func__);
			return retval;
		}

		if (value == 0x00)
			break;

		msleep(100);
		timeout_count++;
	} while (timeout_count < FORCE_TIMEOUT_100MS);

	if (timeout_count == FORCE_TIMEOUT_100MS) {
		printk("%s: Timed out waiting for force update\n",__func__);
		return -ETIMEDOUT;
	}

	command = (unsigned char)COMMAND_FORCE_CAL;

	retval = synaptics_rmi4_reg_write(rmi4_data,
			f54_data->command_base_addr,
			&command,
			sizeof(command));
	if (retval < 0) {
		printk("%s: Failed to write force cal command\n",__func__);
		return retval;
	}

	timeout_count = 0;
	do {
		retval = synaptics_rmi4_reg_read(rmi4_data,
				f54_data->command_base_addr,
				&value,
				sizeof(value));
		if (retval < 0) {
			printk("%s: Failed to read command register\n",__func__);
			return retval;
		}

		if (value == 0x00)
			break;

		msleep(100);
		timeout_count++;
	} while (timeout_count < FORCE_TIMEOUT_100MS);

	if (timeout_count == FORCE_TIMEOUT_100MS) {
		printk("%s: Timed out waiting for force cal\n",__func__);
		return -ETIMEDOUT;
	}

	return 0;
}

static int synaptics_rmi4_f54_sw_reset(struct synaptics_rmi4_data *rmi4_data)
{
	int retval;
	unsigned char command = 0x01;

	retval = synaptics_rmi4_reg_write(rmi4_data,
			rmi4_data->f01_cmd_base_addr,
			&command,
			sizeof(command));
	if (retval < 0)
		return retval;

	msleep(rmi4_data->hw_if->board_data->reset_delay_ms);

	if (rmi4_data->hw_if->ui_hw_init) {
		retval = rmi4_data->hw_if->ui_hw_init(rmi4_data);
		if (retval < 0)
			return retval;
	}

	return 0;
}

static void set_report_size(void)
{
	int retval;
	unsigned char rx = f54_data->rx_assigned;
	unsigned char tx = f54_data->tx_assigned;
	//struct synaptics_rmi4_data *rmi4_data = f54_data->rmi4_data;

	switch (f54_data->report_type) {
	case F54_8BIT_IMAGE:
		f54_data->report_size = rx * tx;
		break;
	case F54_16BIT_IMAGE:
	case F54_RAW_16BIT_IMAGE:
	case F54_TRUE_BASELINE:
	case F54_FULL_RAW_CAP:
	case F54_FULL_RAW_CAP_RX_COUPLING_COMP:
	case F54_SENSOR_SPEED:
		f54_data->report_size = 2 * rx * tx;
		break;
	case F54_HIGH_RESISTANCE:
		f54_data->report_size = HIGH_RESISTANCE_DATA_SIZE;
		break;
	case F54_TX_TO_TX_SHORT:
	case F54_TX_OPEN:
	case F54_TX_TO_GROUND:
		f54_data->report_size = (tx + 7) / 8;
		break;
	case F54_RX_TO_RX1:
	case F54_RX_OPENS1:
		if (rx < tx)
			f54_data->report_size = 2 * rx * rx;
		else
			f54_data->report_size = 2 * rx * tx;
		break;
	case F54_FULL_RAW_CAP_MIN_MAX:
		f54_data->report_size = FULL_RAW_CAP_MIN_MAX_DATA_SIZE;
		break;
	case F54_RX_TO_RX2:
	case F54_RX_OPENS2:
		if (rx <= tx)
			f54_data->report_size = 0;
		else
			f54_data->report_size = 2 * rx * (rx - tx);
		break;
	case F54_ADC_RANGE:
		//not support 
		f54_data->report_size = 2 * rx * tx;
		break;
	case F54_TREX_OPENS:
	case F54_TREX_TO_GND:
	case F54_TREX_SHORTS:
		f54_data->report_size = TREX_DATA_SIZE;
		break;
	default:
		f54_data->report_size = 0;
	}

	return;
}

static bool is_report_type_valid(enum f54_report_types report_type)
{
	switch (report_type) {
	case F54_8BIT_IMAGE:
	case F54_16BIT_IMAGE:
	case F54_RAW_16BIT_IMAGE:
	case F54_HIGH_RESISTANCE:
	case F54_TX_TO_TX_SHORT:
	case F54_RX_TO_RX1:
	case F54_TRUE_BASELINE:
	case F54_FULL_RAW_CAP_MIN_MAX:
	case F54_RX_OPENS1:
	case F54_TX_OPEN:
	case F54_TX_TO_GROUND:
	case F54_RX_TO_RX2:
	case F54_RX_OPENS2:
	case F54_FULL_RAW_CAP:
	case F54_FULL_RAW_CAP_RX_COUPLING_COMP:
	case F54_SENSOR_SPEED:
	case F54_ADC_RANGE:
	case F54_TREX_OPENS:
	case F54_TREX_TO_GND:
	case F54_TREX_SHORTS:
		return true;
		break;
	default:
		f54_data->report_type = INVALID_REPORT_TYPE;
		f54_data->report_size = 0;
		return false;
	}
}

static int synaptics_f54_set_report_type(unsigned long val)
{
	enum f54_report_types type;
	type = (enum f54_report_types)val;
	if(f54_data == NULL)
		return -1;
	if(is_report_type_valid(type)) {
		f54_data->report_type = type;
	}else
		return -1;
	printk("%s,report_type = %d \n",__func__, f54_data->report_type);
	return 0;
}

//EXPORT_SYMBOL(synaptics_f54_set_report_type);

static int synaptics_f54_get_report_type()
{
	if(f54_data == NULL)
		return -1;
	return f54_data->report_type;
}
//EXPORT_SYMBOL(synaptics_f54_get_report_type);
static int synaptics_rmi4_f54_check_full_raw_data()
{
	int i,j;
	int max=0;
	int min=65535;
#ifdef HAVE_0D_TOUCH_KEY	
	for (i = 0; i < f54_data->tx_assigned-KEY_TX_COUNT; i++) {   
		for (j = 0; j < f54_data->rx_assigned-KEY_RX_COUNT; j++)
#else
	for (i = 0; i < f54_data->tx_assigned; i++) {   
	   for (j = 0; j < f54_data->rx_assigned; j++)
#endif
	   {		
		    if(ImageArray[i][j] < RAW_DATA_MIN || ImageArray[i][j] > RAW_DATA_MAX) {
				printk("%s, CTP ITO maybe broken. AmageArray[%d][%d]=%d\n",__func__,i,j,ImageArray[i][j]);
				return 0;

			}
	   }
   	}
	return 1;
}

static int synaptics_rmi4_f54_check_high_resistance_data()
{
	if(HighResistance[0] > F54_HR_RX_OFFSET)
		return 0;
	if(HighResistance[1] > F54_HR_TX_OFFSET)
		return 0;
	if(HighResistance[2] < F54_HR_MIN)
		return 0;
	return 1;
}
int synaptics_rmi4_f54_get_report_data(void)
{
	int retval;
	unsigned char report_index[2];
	unsigned short command;
	unsigned short length;
	unsigned int patience = 250;
	int i, j, k =0;
	short *data;
	
	if(f54_data == NULL){
		printk("%s: f54_data is null \n",__func__);
		return EINVAL;
	}
	struct synaptics_rmi4_data *rmi4_data = f54_data->rmi4_data;
	
	 // Set report mode to to run the AutoScan
	if(!is_report_type_valid(f54_data->report_type)) {
		printk("%s: report_type is invalid \n",__func__);
		retval = -EINVAL;
		goto error_exit;
	}
	command = f54_data->report_type;
	retval = synaptics_rmi4_reg_write(rmi4_data,
	 		f54_data->data_base_addr,
	 		&command, sizeof(command));
	if (retval < 0) {
		printk("%s: Failed to write command=0x%x\n",__func__, command);
		retval = -EINVAL;
		goto error_exit;
	}

	// Set the GetReport bit to run the AutoScan
	command = 0x01;
	retval = synaptics_rmi4_reg_write(rmi4_data,
			f54_data->command_base_addr,
			&command, sizeof(command));
	if (retval < 0) {
		printk("%s: Failed to write command 0x01\n",__func__);
		retval = -EINVAL;
		goto error_exit;
	}
	do {	
		retval = synaptics_rmi4_reg_read(rmi4_data,
			f54_data->command_base_addr, &command, sizeof(command));

		if(retval < 0) {
			printk("%s: Failed to read command 0x01 \n",__func__);
			retval = -EINVAL;
			goto error_exit;
		} 
		if(command & COMMAND_GET_REPORT)
			msleep(20);
		else
			break;
	} while (--patience > 0);
	//mdelay(20);
	if(command & COMMAND_GET_REPORT) {
		printk("%s: Time out waiting for report ready \n",__func__);
		retval = -EINVAL;
		goto error_exit;
	}

	set_report_size();
	if (f54_data->report_size == 0) {
		printk("%s: Report data size = 0\n",__func__);
		retval = -EINVAL;
		goto error_exit;
	}

	if (f54_data->data_buffer_size < f54_data->report_size) {
		mutex_lock(&f54_data->data_mutex);
		if (f54_data->data_buffer_size)
			kfree(f54_data->report_data);
		f54_data->report_data = kzalloc(f54_data->report_size, GFP_KERNEL);
		if (!f54_data->report_data) {
			printk("%s: Failed to alloc mem for data buffer\n",__func__);
			f54_data->data_buffer_size = 0;
			mutex_unlock(&f54_data->data_mutex);
			retval = -ENOMEM;
			goto error_exit;
		}
		f54_data->data_buffer_size = f54_data->report_size;
		mutex_unlock(&f54_data->data_mutex);
	}

	report_index[0] = 0;
	report_index[1] = 0;
	retval = synaptics_rmi4_reg_write(rmi4_data,
			f54_data->data_base_addr + DATA_REPORT_INDEX_OFFSET,
			report_index,
			sizeof(report_index));
	if (retval < 0) {
		printk("%s: Failed to write index\n",__func__);
		retval = -EINVAL;
		goto error_exit;
	}
	
	
	//to read data
	retval = synaptics_rmi4_reg_read(rmi4_data,
			f54_data->data_base_addr + DATA_REPORT_DATA_OFFSET,
			f54_data->report_data,f54_data->report_size);
	if (retval < 0) {
		printk("%s: Failed to read image data\n",__func__);
		retval = -EINVAL;
		goto error_exit;
	}

	if(f54_data->report_type == F54_HIGH_RESISTANCE) {
		printk("yaoyc[");
		for(i = 0; i < HIGH_RESISTANCE_DATA_SIZE; i+=2) {
			HighResistance[i/2] = (short) (f54_data->report_data[i]|(f54_data->report_data[i+1] << 8));
			printk("%d ,",HighResistance[i/2]);
		}
		printk("]\n");
		return 0;
	}
	
	for (i = 0; i < f54_data->tx_assigned; i++)
   {
       for (j = 0; j < f54_data->rx_assigned; j++)
	   {
		 ImageArray[i][j] =(short) (f54_data->report_data[k]|(f54_data->report_data[k+1] << 8));
		 k = k + 2;
	   }  	  
   }

	printk("%s: query_base_addr:0x%04x; data_base_addr:0x%04x; tx:%d; rx:%d \n",__func__,f54_data->query_base_addr,
			f54_data->data_base_addr,f54_data->tx_assigned,f54_data->rx_assigned);
   // Check against test limits
   		printk("\nThe test result:\n");
   	if(f54_data->report_type == F54_16BIT_IMAGE) {
		for (j = 0; j < f54_data->rx_assigned; j++)
		{   
		   printk("yaoyc[ ");
		   for (i = 0; i < f54_data->tx_assigned; i++)
		   {
				printk("(%d),",ImageArray[i][j]);
		   }
		   printk("]\n");
		}
   	}else {
		for (i = 0; i < f54_data->tx_assigned; i++)
		{   
		   printk("yaoyc[ ");
		   for (j = 0; j < f54_data->rx_assigned; j++)
		   {
				printk("(%d),",ImageArray[i][j]);
		   }
		   printk("]\n");
		}
	}
   
	return 0;
error_exit:

	return retval;
}


int synaptics_rmi4_f54_command_func(int command, int value)
{
	int retval = 0;
	switch (command) {
		case F54_SELF_TEST:
			synaptics_f54_set_report_type(F54_FULL_RAW_CAP_RX_COUPLING_COMP);
			retval = do_preparation();
			if (retval < 0) {
				printk("%s: Failed to do preparation\n",__func__);
				return retval;
			}
			retval = synaptics_rmi4_f54_get_report_data();
			if(retval < 0) {
				synaptics_rmi4_f54_sw_reset(f54_data->rmi4_data);
				return retval;
			}
			
			synaptics_f54_set_report_type(F54_HIGH_RESISTANCE);
			retval = synaptics_rmi4_f54_get_report_data();
			if(retval < 0) {
				synaptics_rmi4_f54_sw_reset(f54_data->rmi4_data);
				return retval;
			}
			synaptics_rmi4_f54_sw_reset(f54_data->rmi4_data);
			
			retval = (synaptics_rmi4_f54_check_full_raw_data() & synaptics_rmi4_f54_check_high_resistance_data());
			break;
		case F54_GET_REPORT_TYPE:
			retval = synaptics_f54_get_report_type();
			break;
		case F54_SET_REPORT_TYPE:
			retval = synaptics_f54_set_report_type(value);
			break;
		case F54_GET_REPORT_DATA:
			retval = synaptics_rmi4_f54_get_report_data();
			break;
		default:
			retval = -1;
			break;
	}
	return retval;
}
EXPORT_SYMBOL(synaptics_rmi4_f54_command_func);



static void synaptics_rmi4_f54_data_set_regs(struct synaptics_rmi4_data *rmi4_data,
		struct synaptics_rmi4_fn_desc *fd,	unsigned char page)
{
	f54_data->query_base_addr = fd->query_base_addr | (page << 8);
	f54_data->control_base_addr = fd->ctrl_base_addr | (page << 8);
	f54_data->data_base_addr = fd->data_base_addr | (page << 8);
	f54_data->command_base_addr = fd->cmd_base_addr | (page << 8);
	return;
}
int synaptics_rmi4_f54_data_init(struct synaptics_rmi4_data *rmi4_data)
{
	int retval;
	unsigned short pdt_entry_addr;
	unsigned char page_number;
	unsigned char rx_tx[2];

	bool f54found = false;
	
	struct synaptics_rmi4_fn_desc rmi_fd;
	if(f54_data == NULL) {
		f54_data = kzalloc(sizeof(*f54_data), GFP_KERNEL);
		if (!f54_data) {
			printk("%s: Failed to alloc mem for f54\n",__func__);
			retval = -ENOMEM;
			goto exit;
		}
	}
	f54_data->rmi4_data = rmi4_data;
	/* Scan the page description tables of the pages to service */
	for (page_number = 0; page_number < PAGES_TO_SERVICE; page_number++) {
		for (pdt_entry_addr = PDT_START; pdt_entry_addr > PDT_END;
				pdt_entry_addr -= PDT_ENTRY_SIZE) {
			pdt_entry_addr |= (page_number << 8);

			retval = synaptics_rmi4_reg_read(rmi4_data,
					pdt_entry_addr,
					(unsigned char *)&rmi_fd,
					sizeof(rmi_fd));
			if (retval < 0)
				return retval;

			pdt_entry_addr &= ~(MASK_8BIT << 8);

			if (rmi_fd.fn_number == 0) {
				printk("%s: Reached end of PDT\n",__func__);
				break;
			}

			printk("%s: F%02x found (page %d)\n",__func__, rmi_fd.fn_number,
					page_number);

			switch (rmi_fd.fn_number) {
			case SYNAPTICS_RMI4_F54:
				synaptics_rmi4_f54_data_set_regs(rmi4_data,
						&rmi_fd, page_number);
				f54found = true;
				break;
			default:
				break;
			}

			if (f54found)
				goto pdt_done;

		}
	}

	if (!f54found) {
		retval = -ENODEV;
		goto exit_free_f54;
	}

pdt_done:
	retval = synaptics_rmi4_reg_read(rmi4_data,
			f54_data->query_base_addr,
			f54_data->query.data,
			sizeof(f54_data->query.data));
	if (retval < 0) {
		printk("%s: Failed to read f54 query registers\n",__func__);
		goto exit;
	}

	retval = synaptics_rmi4_f54_set_ctrl();
	if (retval < 0) {
		printk("%s: Failed to set up f54 control registers\n",__func__);
		goto exit_free_control;
	}
	
	mutex_init(&f54_data->data_mutex);
	mutex_init(&f54_data->control_mutex);

	f54_data->rx_assigned =CFG_F54_RXCOUNT ;// rx_tx[0];
	f54_data->tx_assigned =CFG_F54_TXCOUNT ;//rx_tx[1];
	f54_data->report_size = f54_data->rx_assigned * f54_data->tx_assigned *2;
	f54_data->report_type = 0;
	return 0;

exit_free_control:
	free_control_mem();	
exit_free_f54:
	kfree(f54_data);
	f54_data = NULL;

exit:
	return retval;
}
EXPORT_SYMBOL(synaptics_rmi4_f54_data_init);

MODULE_AUTHOR("Synaptics, Inc.");
MODULE_DESCRIPTION("Synaptics DSX Test Reporting Module");
MODULE_LICENSE("GPL v2");

