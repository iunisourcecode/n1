#ifndef BUILD_LK
#include <linux/string.h>
#endif
#include "lcm_drv.h"

#ifdef BUILD_LK
#include <platform/mt_gpio.h>
#include <platform/mt_pmic.h>
#elif defined(BUILD_UBOOT)
#include <asm/arch/mt_gpio.h>
#else
#include <mach/mt_pm_ldo.h>
#include <mach/mt_gpio.h>
#endif

#include <cust_gpio_usage.h>

#ifdef BUILD_LK
#define	LCD_DEBUG(format, ...)   printf("lk " format "\n", ## __VA_ARGS__)
#elif defined(BUILD_UBOOT)
#define	LCD_DEBUG(format, ...)   printf("uboot " format "\n", ## __VA_ARGS__)
#else
#define	LCD_DEBUG(format, ...)   printk("kernel " format "\n", ## __VA_ARGS__)
#endif

// ---------------------------------------------------------------------------
//  Local Variables
// ---------------------------------------------------------------------------
#ifdef CONFIG_GN_BSP_MTK_AMOLED_FEATURE_SUPPORT
int acl=0;
int hbm=0;
#endif

#define SET_RESET_PIN(v)    								(lcm_util.set_reset_pin((v)))
#define MDELAY(n) 											(lcm_util.mdelay(n))
#define UDELAY(n) 											(lcm_util.udelay(n))

// ---------------------------------------------------------------------------
//  Local Functions
// ---------------------------------------------------------------------------
#define dsi_set_cmdq_V2(cmd, count, ppara, force_update)	lcm_util.dsi_set_cmdq_V2(cmd, count, ppara, force_update)
#define dsi_set_cmdq(pdata, queue_size, force_update)		lcm_util.dsi_set_cmdq(pdata, queue_size, force_update)
#define wrtie_cmd(cmd)										lcm_util.dsi_write_cmd(cmd)
#define write_regs(addr, pdata, byte_nums)					lcm_util.dsi_write_regs(addr, pdata, byte_nums)
#define read_reg(cmd)										lcm_util.dsi_dcs_read_lcm_reg(cmd)
#define read_reg_v2(cmd, buffer, buffer_size)   			lcm_util.dsi_dcs_read_lcm_reg_v2(cmd, buffer, buffer_size)

// ---------------------------------------------------------------------------
//  Local Constants
// ---------------------------------------------------------------------------
#define LCM_DSI_CMD_MODE									0
#define FRAME_WIDTH  										(720)
#define FRAME_HEIGHT 										(1280)

static LCM_UTIL_FUNCS lcm_util;
static LCM_DSI_MODE_SWITCH_CMD lcm_switch_mode_cmd;

#ifndef TRUE
    #define TRUE 1
#endif

#ifndef FALSE
    #define FALSE 0
#endif

// ---------------------------------------------------------------------------
//  LCM Driver Implementations
// ---------------------------------------------------------------------------

static void lcm_set_util_funcs(const LCM_UTIL_FUNCS *util)
{
   LCD_DEBUG("lcm_set_util_funcs\n");
    memcpy(&lcm_util, util, sizeof(LCM_UTIL_FUNCS));
}

static void lcm_get_params(LCM_PARAMS *params)
{
	memset(params, 0, sizeof(LCM_PARAMS));

	params->type   = LCM_TYPE_DSI;

	params->width  = FRAME_WIDTH;
	params->height = FRAME_HEIGHT;

#if (LCM_DSI_CMD_MODE)
    params->dsi.mode   = CMD_MODE;
#else
    params->dsi.mode   = BURST_VDO_MODE;
#endif
	//params->dsi.switch_mode = CMD_MODE;

	//params->dsi.switch_mode_enable = 0;

	// DSI
	/* Command mode setting */
	params->dsi.LANE_NUM				= LCM_FOUR_LANE;
	//The following defined the fomat for data coming from LCD engine.
	params->dsi.data_format.color_order 	= LCM_COLOR_ORDER_RGB;
	params->dsi.data_format.trans_seq   	= LCM_DSI_TRANS_SEQ_MSB_FIRST;
	params->dsi.data_format.padding     	= LCM_DSI_PADDING_ON_LSB;
	params->dsi.data_format.format      	= LCM_DSI_FORMAT_RGB888;

	// Highly depends on LCD driver capability.
	params->dsi.packet_size=256;

	//video mode timing
	params->dsi.PS=LCM_PACKED_PS_24BIT_RGB888;

	params->dsi.vertical_sync_active			= 2;
	params->dsi.vertical_backporch				= 8;
	params->dsi.vertical_frontporch				= 14;//20
	params->dsi.vertical_active_line			= FRAME_HEIGHT;

	params->dsi.horizontal_sync_active			= 10;
	params->dsi.horizontal_backporch			= 14;
	params->dsi.horizontal_frontporch			= 48;//30
	params->dsi.horizontal_active_pixel			= FRAME_WIDTH;

	params->dsi.PLL_CLOCK 						= 221; 	//this value must be in MTK suggested table
	params->dsi.cont_clock                  	= 1;
	params->dsi.horizontal_bllp            		= 60;
	params->dsi.HS_TRAIL						= 5; 
	params->dsi.HS_PRPR	 						= 4;
	params->dsi.ssc_disable 	       			= 1;

	//params->dsi.clk_lp_per_line_enable = 0;
	params->dsi.esd_check_enable = 0;
	params->dsi.customization_esd_check_enable = 0;
}

static void init_lcm_registers(void)
{
	LCD_DEBUG("init_lcm_registers\n");
	unsigned int data_array[16];

	data_array[0] = 0x00110500;		// Sleep Out
	dsi_set_cmdq(data_array, 1, 1);
	MDELAY(20);

	data_array[0] = 0x00351500;		//TE
	dsi_set_cmdq(data_array, 1, 1);

	data_array[0] = 0x00033902;		// Unlock
	data_array[1] = 0x005A5AF0;
	dsi_set_cmdq(data_array, 2, 1);

	data_array[0] = 0x00043902;		//PENTILE BLOCK SET
	data_array[1] = 0x20D8D8C0;
	dsi_set_cmdq(data_array, 2, 1);

	data_array[0] = 0x00033902;		// Lock
	data_array[1] = 0x00A5A5F0;
	dsi_set_cmdq(data_array, 2, 1);

	data_array[0] = 0x20531500;		// hbm off
	dsi_set_cmdq(data_array, 1, 1);

#ifdef CONFIG_GN_BSP_MTK_AMOLED_FEATURE_SUPPORT
	if(acl == 1)
	{
		data_array[0] = 0x02551500;		// acl on
		dsi_set_cmdq(data_array, 1, 1);
	}
	else
	{
		data_array[0] = 0x00551500;		// acl off
		dsi_set_cmdq(data_array, 1, 1);
	}
#else
	data_array[0] = 0x00551500;		// acl off
	dsi_set_cmdq(data_array, 1, 1);
#endif

	data_array[0] = 0x00511500;		// Brightness
	dsi_set_cmdq(data_array, 1, 1);
	//MDELAY(120);

	data_array[0] = 0x00290500;		// Display On
	dsi_set_cmdq(&data_array, 1, 1);
}

static void lcm_init(void)
{
	LCD_DEBUG("lcm_init\n");

	SET_RESET_PIN(1);
    MDELAY(10);
    SET_RESET_PIN(0);
    MDELAY(20);
    SET_RESET_PIN(1);
    MDELAY(10);

	init_lcm_registers();
}

static void lcm_init_power(void)
{
	LCD_DEBUG("lcm_init_power\n");
#ifdef BUILD_LK
	pmic_set_register_value(PMIC_RG_VGP1_VOSEL,6);	//6:3.0V 
	pmic_set_register_value(PMIC_RG_VGP1_EN,1);
#else
	hwPowerOn(MT6328_POWER_LDO_VGP1, VOL_3000, "LCM_DRV");
#endif	
	MDELAY(5);
}

static void lcm_resume_power(void)
{
	lcm_init_power();
}

#ifndef BUILD_LK
static bool first_suspend = TRUE;
#endif
static void lcm_suspend_power(void)
{
	LCD_DEBUG("lcm_suspend_power\n");
#ifdef BUILD_LK
	pmic_set_register_value(PMIC_RG_VGP1_EN,0);
#else
	if(first_suspend == TRUE)
	{
		hwPowerOn(MT6328_POWER_LDO_VGP1, VOL_3000, "LCM_DRV");
		first_suspend = FALSE;
	}
	hwPowerDown(MT6328_POWER_LDO_VGP1, "LCM_DRV");	
#endif	
}

static void lcm_resume(void)
{
	LCD_DEBUG("lcm_resume\n");
	lcm_init();
}

static void lcm_suspend(void)
{
	LCD_DEBUG("lcm_suspend\n");
	unsigned int data_array[16];

	data_array[0] = 0x00280500;		// Display off
	dsi_set_cmdq(data_array, 1, 1);
	MDELAY(20);

	data_array[0] = 0x00100500;		// sleep in
	dsi_set_cmdq(data_array, 1, 1);
	MDELAY(120);

	//SET_RESET_PIN(0);
	//MDELAY(5);
}

static unsigned int lcm_compare_id(void)
{
    unsigned int id=0;
	unsigned char buffer[5];
	unsigned int array[16];
	int i;
	unsigned int lcd_id = 0;

	SET_RESET_PIN(1);
    MDELAY(10);
    SET_RESET_PIN(0);
    MDELAY(10);
    SET_RESET_PIN(1);
    MDELAY(10);

	array[0] = 0x00033700;// read id return two byte,version and id
	dsi_set_cmdq(array, 1, 1);

	read_reg_v2(0x004, buffer, 3);
	MDELAY(20);
	//lcd_id = (buffer[2] << 8 )| buffer[3];

    LCD_DEBUG("SMD S6E8AA5X01 ID1 = 0x%08x,D2 = 0x%08x,D3 = 0x%08x\n", buffer[0],buffer[1],buffer[2]);

	return 1;
}

static void lcm_setbacklight(unsigned int level)
{
	unsigned int cmd = 0x51;

	if(level > 255)
		level = 255;

	if(level >= 10)
		level = level*230/255;
	else if(level > 0)
	{
		level = 5;
	}
	else 
		level = 0;
	
	LCD_DEBUG("backlight: level = %d\n",level);
	dsi_set_cmdq_V2(cmd, 1, &level, 1);
}

static void* lcm_switch_mode(int mode)
{
	LCD_DEBUG("lcm_switch_mode\n");
#ifndef BUILD_LK
	//customization: 1. V2C config 2 values, C2V config 1 value; 2. config mode control register
	if(mode == 0)
	{//V2C
		lcm_switch_mode_cmd.mode = CMD_MODE;
		lcm_switch_mode_cmd.addr = 0xBB;// mode control addr
		lcm_switch_mode_cmd.val[0]= 0x13;//enabel GRAM firstly, ensure writing one frame to GRAM
		lcm_switch_mode_cmd.val[1]= 0x10;//disable video mode secondly
	}
	else
	{//C2V
		lcm_switch_mode_cmd.mode = SYNC_PULSE_VDO_MODE;
		lcm_switch_mode_cmd.addr = 0xBB;
		lcm_switch_mode_cmd.val[0]= 0x03;//disable GRAM and enable video mode
	}
	return (void*)(&lcm_switch_mode_cmd);
#else
	return NULL;
#endif
}

#if (LCM_DSI_CMD_MODE)
static void lcm_update(unsigned int x, unsigned int y, unsigned int width, unsigned int height)
{
	LCD_DEBUG("lcm_update\n");
	unsigned int x0 = x;
	unsigned int y0 = y;
	unsigned int x1 = x0 + width - 1;
	unsigned int y1 = y0 + height - 1;

	unsigned char x0_MSB = ((x0>>8)&0xFF);
	unsigned char x0_LSB = (x0&0xFF);
	unsigned char x1_MSB = ((x1>>8)&0xFF);
	unsigned char x1_LSB = (x1&0xFF);
	unsigned char y0_MSB = ((y0>>8)&0xFF);
	unsigned char y0_LSB = (y0&0xFF);
	unsigned char y1_MSB = ((y1>>8)&0xFF);
	unsigned char y1_LSB = (y1&0xFF);

	unsigned int data_array[16];

	data_array[0]= 0x00053902;
	data_array[1]= (x1_MSB<<24)|(x0_LSB<<16)|(x0_MSB<<8)|0x2a;
	data_array[2]= (x1_LSB);
	dsi_set_cmdq(data_array, 3, 1);

	data_array[0]= 0x00053902;
	data_array[1]= (y1_MSB<<24)|(y0_LSB<<16)|(y0_MSB<<8)|0x2b;
	data_array[2]= (y1_LSB);
	dsi_set_cmdq(data_array, 3, 1);

	data_array[0]= 0x002c3909;
	dsi_set_cmdq(data_array, 1, 0);
}
#endif

#ifdef CONFIG_GN_BSP_MTK_AMOLED_FEATURE_SUPPORT
void lcm_setacl_cmdq(unsigned int level)
{
	LCD_DEBUG("lcm_setacl_cmdq level= %d\n", level);

	unsigned int cmd = 0x55;
	unsigned int count =1;
	unsigned int value = 0;

	if(level == 1)
	{
		acl = 1;
		value = 0x02;
		dsi_set_cmdq_V2(cmd, count, &value, 1);
	}
	else
	{
		acl = 0;
		value = 0x00;
		dsi_set_cmdq_V2( cmd, count, &value, 1);
	}
	
}

void lcm_sethbm_cmdq(unsigned int level)
{
	LCD_DEBUG("lcm_sethbm_cmdq level= %d\n",level);

	unsigned int cmd = 0x53;
	unsigned int count =1;
	unsigned int value = 0;

	if(level == 1)
	{
		hbm = 1;
		value = 0x60;
		dsi_set_cmdq_V2(cmd, count, &value, 1);
	}
	else
	{
		hbm = 0;
		value = 0x20;
		dsi_set_cmdq_V2(cmd, count, &value, 1);
	}
}

void lcm_setpartical_cmdq(unsigned int level)
{
	LCD_DEBUG("lcm_setpartical_cmdq level= %d\n",level);
	if(level == 1)
	{				
		unsigned int cmd = 0x12;
		unsigned int count =0;
		unsigned char paralist1[1] = {0x00};

		dsi_set_cmdq_V2(cmd, count, paralist1, 1);
		
		cmd = 0x30;
		count = 4;
		unsigned char paralist2[4] = {0x03,0x1F,0x04,0xFF};

		dsi_set_cmdq_V2(cmd, count, paralist2, 1);
	}
	else
	{		
		unsigned int cmd = 0x13;
		unsigned int count =0;
		unsigned char paralist[1] = {0x00};

		dsi_set_cmdq_V2(cmd, count, paralist, 1);
	}
}
#endif

LCM_DRIVER gn_smd_s6e8aa5_lcm_drv=
{
	.name           	= "gn_smd_s6e8aa5_hd",
	.set_util_funcs 	= lcm_set_util_funcs,
	.get_params     	= lcm_get_params,
	.init           	= lcm_init,
	.suspend        	= lcm_suspend,
	.resume         	= lcm_resume,
	//.compare_id     	= lcm_compare_id,
	.init_power		= lcm_init_power,
	.resume_power 		= lcm_resume_power,
	.suspend_power 	= lcm_suspend_power,
	.set_backlight 	= lcm_setbacklight,
#if (LCM_DSI_CMD_MODE)
	.update         	= lcm_update,
#endif
	//.switch_mode		= lcm_switch_mode,
};
