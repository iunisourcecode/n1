#include <linux/types.h>
#include <mach/mt_pm_ldo.h>
#include <cust_alsps_apds9960.h>
#include <mach/upmu_common.h>

static struct alsps_hw cust_alsps_hw = {
    .i2c_num    = 2,
	.polling_mode_ps =0,
	.polling_mode_als =1,
    .power_id   = MT65XX_POWER_NONE,    /*LDO is not used*/
    .power_vol  = VOL_DEFAULT,          /*LDO is not used*/
    //.i2c_addr   = {0x0C, 0x48, 0x78, 0x00},
//Gionee BSP1 yangqb 20140912 modify for CR01380122 LCM MODULE AAL SUPPORT start
#ifdef MTK_AAL_SUPPORT
    .als_level  = { 6, 150, 360, 740, 1950, 4350, 6600, 9050, 11100, 14010, 18040, 21000, 21000, 21000, 21000},
    .als_value  = { 1, 145, 335, 690, 1810, 4010, 6120, 8800, 10040, 13080, 17060, 19200, 19200, 19200, 19200, 19200},
#else
    .als_level  = { 6, 20, 100, 800, 1500, 2500,  4000,  6000,  65535},
    .als_value  = { 1, 10, 100, 100,  100,  100,  1000, 10000, 100000},
#endif
 //Gionee BSP1 yangqb 20140912 modify for CR01380122 LCM MODULE AAL SUPPORT end
//Gionee yang_yang 20140802 CR01341808 begin
#ifdef GN_MTK_BSP_PS_DYNAMIC_CALIBRATION
	.ps_cali_noise       = { 100, 200},
	.ps_cali_offset_high = { 85,  50},
	.ps_cali_offset_low  = { 80,  40},
#endif
//Gionee yang_yang 20140802 CR01341808 end
    .ps_threshold_high = 230,
    .ps_threshold_low = 200,
};
struct alsps_hw *get_cust_alsps_hw(void) {
    return &cust_alsps_hw;
}

int APDS9930_CMM_PPCOUNT_VALUE = 0x08;
int APDS9930_CMM_CONTROL_VALUE = 0xE4;
int ZOOM_TIME = 4;

int pmic_ldo_suspend_enable(int enable)
{
	//0 for disable suspend, 1 for enable suspend
	//upmu_set_vio18_lp_sel(enable);
	//upmu_set_vio28_lp_sel(enable);
	return 0;
}

