
###add by aurora hujianwei 20150609 start
APK_DEFAULT_TOUCHPAL_SUPPORT = no
APK_DEFAULT_SINA_WEIBO_SUPPORT = no
APK_DEFAULT_DANGERDASH_SUPPORT = no
###add by aurora hujianwei 20150609 end

#################GLOBAL_DEFINE BEGIN####################
TARGET_PRODUCT=full_gionee6753_65c_l1

GN_KERNEL_CFG_FILE_ENG=kernel-3.10/arch/arm64/configs/gionee6753_65c_l1_debug_defconfig
GN_KERNEL_CFG_FILE=kernel-3.10/arch/arm64/configs/gionee6753_65c_l1_defconfig

AUTO_ADD_GLOBAL_DEFINE_BY_NAME+= CONFIG_GN_BSP_PS_STATIC_CALIBRATION
AUTO_ADD_GLOBAL_DEFINE_BY_NAME_VALUE+= CONFIG_GN_BSP_MTK CONFIG_GN_BSP_AUDIO_SUPPORT CONFIG_GN_BSP_GNTORCH_SUPPORT MTK_LCM_PHYSICAL_ROTATION_HW GN_CYWEE_SENSORHUB_SUPPORT CONFIG_GN_BSP_MTK_AMOLED_FEATURE_SUPPORT MTK_ULTRA_DIMMING_SUPPORT
AUTO_ADD_GLOBAL_DEFINE_BY_VALUE+= 
#################GLOBAL_DEFINE END######################

#################BSP BEGIN##############################
CONFIG_GN_BSP_MTK=yes

#################PL BEGIN###############################
# Gionee BSP1 add BQ24192 bedin
MTK_BQ24296_SUPPORT=yes
CONFIG_GN_BSP_MTK_BQ24296_SUPPORT=yes
CONFIG_MTK_PUMP_EXPRESS_PLUS_SUPPORT=no
# Gionee BSP1 add BQ24192 end
#################PL END#################################

#################LK BEGIN###############################
# Gionee BSP1 yangqb modify for CBL7503 basecode start
CUSTOM_LK_LCM="gn_smd_s6e8aa5"
BOOT_LOGO := hd720
# Gionee BSP1 yangqb modify for CBL7503 basecode end

#################LK END#################################

#################KERNEL BEGIN###########################
# Gionee BSP1 yangqb modify for CBL7503 basecode start
CONFIG_CUSTOM_KERNEL_LCM="gn_smd_s6e8aa5"
CONFIG_LCM_HEIGHT="1280"
CONFIG_LCM_WIDTH="720"
CONFIG_MTK_AAL_SUPPORT=yes
CONFIG_MTK_LCM_PHYSICAL_ROTATION_HW=yes
CONFIG_GN_BSP_MTK_AMOLED_FEATURE_SUPPORT=yes

CONFIG_GN_BSP_MTK_DEVICE_CHECK=yes

CONFIG_MTK_BQ24296_SUPPORT=yes

CONFIG_MTK_NFC=no
CONFIG_NFC_MT6605=no
CONFIG_HID_GYRATION=no
# Gionee BSP1 yangqb modify for CBL7503 basecode end

# Gionee BSP1 yangqb 20150324 modify for CBL7503 basecode start
CONFIG_GN_BSP_MTK_LED_AWINIC_AW2013=yes
# Gionee BSP1 yangqb 20150116 modify for CBL7503 basecode end

# Gionee BSP1 yangqb 20150324 modify for CBL7503 basecode start
CONFIG_GN_BSP_MTK_HALL_KEY=yes
# Gionee BSP1 yangqb 20150116 modify for CBL7503 basecode end

# Gionee BSP1 yangqb 20150324 modify for CBL7503 basecode start
CONFIG_GN_BSP_MTK_VIBRATOR_DRV2604L=yes
# Gionee BSP1 yangqb 20150116 modify for CBL7503 basecode end

#Gionee BSP1 zhangliu 2015-03-25 add for Smart PA begin
CONFIG_MTK_SOUND=yes
CUSTOM_KERNEL_SOUND=gn_nxp_tfa9887
#Gionee BSP1 zhangliu 2015-03-25 add for Smart PA end

#Gionee litao 20150324 modify for camera begin
CONFIG_CUSTOM_KERNEL_IMGSENSOR="s5k3m2_mipi_raw ov8858R2A_mipi_raw"
#Gionee litao 20150324 modify for camera end

#Gionee yaoyc 20150407 add for synaptics tp begin
CONFIG_TOUCHSCREEN_SYNAPTICS_S3508=yes
CONFIG_TOUCHSCREEN_MTK_GT1151=no
#Gionee yaoyc 20150407 add for synaptics tp end
#################KERNEL END#############################

#################BSP END################################

#################ADNROID BEGIN##########################
# Gionee BSP1 yangqb modify for GBL7318 basecode start
LCM_HEIGHT=1280
LCM_WIDTH=720
BOOT_LOGO=hd720
MTK_AAL_SUPPORT=yes
MTK_LCM_PHYSICAL_ROTATION_HW=yes
MTK_ULTRA_DIMMING_SUPPORT=yes
# Gionee BSP1 yangqb modify for GBL7318 basecode end

# Gionee BSP1 yangqb modify for GBL7503 basecode start
MTK_IPOH_SUPPORT = no
MTK_IPO_SUPPORT = no
# Gionee BSP1 yangqb modify for GBL7503 basecode end

#Gionee litao 20150324 modify for camera begin
CUSTOM_HAL_IMGSENSOR = s5k3m2_mipi_raw ov8858R2A_mipi_raw
CUSTOM_HAL_MAIN_IMGSENSOR = s5k3m2_mipi_raw
CUSTOM_HAL_SUB_IMGSENSOR = ov8858R2A_mipi_raw
CUSTOM_KERNEL_IMGSENSOR = s5k3m2_mipi_raw ov8858R2A_mipi_raw
CUSTOM_KERNEL_MAIN_IMGSENSOR = s5k3m2_mipi_raw
CUSTOM_KERNEL_SUB_IMGSENSOR = ov8858R2A_mipi_raw
#Gionee litao 20150324 modify for camera end

#Gionee yanghanming  20150306
MTK_LTE_SUPPORT=yes
MTK_ENABLE_MD1=yes
MTK_MD1_SUPPORT=6
MTK_SHARE_MODEM_CURRENT=2
MTK_SHARE_MODEM_SUPPORT=2
MTK_WORLD_PHONE=yes
MTK_UMTS_TDD128_MODE=yes
CUSTOM_MODEM=CBL7503A01_A_LTTG_MODEM CBL7503A01_A_LWG_MODEM
MTK_SIM_HOT_SWAP = yes
MTK_SIM_HOT_SWAP_COMMON_SLOT = yes
OPTR_SPEC_SEG_DEF = NONE
#Gionee yanghanming  20150306

#Gionee BSP1 yang_yang modify for CBL7503 sensor start
MTK_SENSOR_SUPPORT=no
CONFIG_MTK_SENSOR_SUPPORT=no
CONFIG_IIO=yes
CONFIG_IIO_BUFFER=yes
CONFIG_IIO_TRIGGER=yes
CONFIG_IIO_BUFFER_CB=yes
CONFIG_IIO_TRIGGERED_BUFFER=yes
CONFIG_IIO_KFIFO_BUF=yes
CONFIG_INPUT_POLLDEV=yes
CONFIG_CUSTOM_KERNEL_SENSORHUB="ST"
GN_CYWEE_SENSORHUB_SUPPORT=yes
CONFIG_GN_BSP_PS_STATIC_CALIBRATION=yes
#Gionee BSP1 yang_yang modify for CBL7503 sensor end

#Gionee BSP1 zhangliu 2015-03-25 add for Smart PA begin
NXP_SMARTPA_SUPPORT=tfa9890
CONFIG_GN_BSP_AUDIO_SUPPORT=yes
CONFIG_GN_BSP_MTK_NXP_RESET_SUPPORT=yes
#Gionee BSP1 zhangliu 2015-03-25 add for Smart PA end

#Gionee BSP1 zhangliu 2015-05-05 add for Audio begin
MTK_DUAL_MIC_SUPPORT=no
MTK_INCALL_NORMAL_DMNR=no
MTK_BESLOUDNESS_SUPPORT=no
MTK_MAGICONFERENCE_SUPPORT=no
MTK_AUDIO_MIC_INVERSE=no
#Gionee BSP1 zhangliu 2015-05-05 add for Audio end

# Gionee BSP1 yangqb modify for GBL7503 basecode start
MTK_NFC_SUPPORT=no
MTK_BEAM_PLUS_SUPPORT=no
MTK_NFC_ADDON_SUPPORT=no
MTK_NFC_FW_MT6605=no
MTK_NFC_HCE_SUPPORT=no
MTK_NFC_MT6605=no
MTK_NFC_OMAAC_GEMALTO=no
MTK_NFC_OMAAC_SUPPORT=no
MTK_WIFIWPSP2P_NFC_SUPPORT=no
MTK_NFC_GSMA_SUPPORT=no
# Gionee BSP1 yangqb modify for GBL703 basecode end
#################ADNROID END############################

################################ APP BEGIN ################################
#################### GiONEE MTK APP CONFIG BEGIN ####################
MTK_PRODUCT_LOCALES = zh_CN en_US
GN_RODUCT_AAPT_CONFIG = hdpi xhdpi xxhdpi

#################### GiONEE MTK APP CONFIG END ######################
GN_APK_MMITEST_SUPPORT = yes
GN_RW_GN_MMI_LTETDDANT = no
GN_RW_GN_MMI_KEYTEST_CAMERA = no
GN_RW_GN_MMI_KEYTEST_FOCUS = no
GN_RW_GN_MMI_KEYTEST_SEARCH = no
GN_RW_GN_MMI_KEYTEST_MENU = no
GN_RW_GN_MMI_TP_CROSS = no
GN_RW_GN_MMI_FLASH = no
GN_RW_GN_MMI_FLASH2 = no
GN_RW_GN_MMI_FINGERPRINTS = no
GN_RW_GN_MMI_TP_TEN = no
GN_RW_GN_MMI_IRTEST = no
GN_RW_GN_MMI_NFC = no
GN_RW_GN_MMI_NFC2 = no
GN_RW_GN_MMI_DOUBLETONE = no
GN_RW_GN_MMI_SETCOLOR = no
GN_RW_GN_MMI_FLASHLIGHT = no


GN_RO_GN_GNVERNUMBERREL = 01
GN_RO_BUILD_DISPLAY_ID = SV1.0
GN_RO_PRODUCT_MODEL = IUNI  U0001
GN_RO_PRODUCT_BRAND = IUNI
GN_RO_PRODUCT_NAME = IUNI U0001
GN_RO_PRODUCT_DEVICE = IUNI
GN_RO_PRODUCT_MANUFACTURER = IUNI
GN_RO_GN_EXTMODEL = IUNI U0001
GN_RO_GN_NFC_SUPPORT = no
GN_RO_GN_GNPRODUCTID = CBL7503A02_A
GN_RO_GN_GNPROJECTID = CBL7503
GN_RO_GN_GNVERNUMBER = 7503
#Gionee <others> <wangmeng> <20150311> add for android navigationbar controller.begin
GN_CONFIG_SHOW_NAVIGATIONBAR_SUPPORT = no
#Gionee <others> <wangmeng> <20150311> add for android navigationbar controller.end

################################## APP END ################################
################################## CTA BEGIN  ##############################
MTK_CTA_SUPPORT = yes
OPTR_SPEC_SEG_DEF=NONE

MTK_HOTKNOT_SUPPORT = no
MTK_SYSTEM_UPDATE_SUPPORT = no
MTK_WEATHER_PROVIDER_APP = no
MTK_WEATHER_WIDGET_APP = no
#MTK_BUILD_ROOT=yes

#Gionee <Amigo_Inputmethod> <wangmeng> add for inputmethod of chinese.
GN_3RD_XUNFEI_INPUTMETHOD_SUPPORT = yes

ORIGION_MTK_USELESS_APK = yes

################################## CTA END  ################################
