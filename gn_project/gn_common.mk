# Copyright Statement:
#
#GIONEE Inc. (C) 2011. All rights reserved.
#


# This is a generic product that isn't specialized for a specific device.
# It includes the base Android platform.

ifeq ($(strip $(GN_APK_AMIGO_APPSIPPER_SUPPORT)),yes) 
	 PRODUCT_PACKAGES += Amigo_AppSipper
endif
ifeq ($(strip $(GN_APK_AMIGO_CONTACTS_SUPPORT)),yes) 
	 PRODUCT_PACKAGES += Contacts
  	 PRODUCT_PACKAGES += com.gionee.simcontacts.SimContacts
endif
#Gionee lixiaohu added for CR01473561 begin
#ifeq ($(strip $(GN_APK_AMIGO_MMS_SUPPORT)), yes)
ifneq ($(filter yes%,$(GN_APK_AMIGO_MMS_SUPPORT)),)
	 PRODUCT_PACKAGES += Mms
	 PRODUCT_PACKAGES += YuloreFrameWork
endif
#Gionee lixiaohu added for CR01473561 end
ifeq ($(strip $(GN_APK_AMIGO_EMAIL_SUPPORT)),yes) 
	 PRODUCT_PACKAGES += Email
endif
ifeq ($(strip $(GN_APK_AMIGO_EXCHANGE_SUPPORT)),yes) 
	 PRODUCT_PACKAGES += Exchange
endif
ifeq ($(strip $(GN_APK_GN_NUMLOCATION_SUPPORT)),yes) 
	 PRODUCT_PACKAGES += GN_Numlocation
endif
ifeq ($(strip $(GN_APK_AMIGO_SYSTEMMANAGER_SUPPORT)), yes)
  PRODUCT_PACKAGES += Amigo_SystemManager
  PRODUCT_PACKAGES += com.gionee.softmanagerplugin.SoftManagerPlugin
  PRODUCT_PACKAGES += Amigo_SystemManager_SDK
  PRODUCT_PACKAGES += liuliangbao_sdk
  PRODUCT_PACKAGES += Amigo_PowerSaveLauncher
endif

#GIONEE: luohui 2015-2-6 modify for Oversea start->
ifeq ($(strip $(GN_APK_AMIGO_SYSTEMMANAGER_SUPER_SUPPORT)), yes)
  PRODUCT_PACKAGES += Amigo_SystemManager
  PRODUCT_PACKAGES += com.gionee.softmanagerplugin.SoftManagerPlugin
  PRODUCT_PACKAGES += Amigo_SystemManager_SDK
  PRODUCT_PACKAGES += liuliangbao_sdk
  PRODUCT_PACKAGES += Amigo_PowerSaveLauncher
endif
#GIONEE: luohui 2015-2-6 modify for Oversea end<-
ifeq ($(strip $(GN_APK_AMIGO_SETTINGS_SUPPORT)),yes) 
	 PRODUCT_PACKAGES += Amigo_Settings
endif
#GIONEE: wangfei 2015-4-28 modify for Oversea start->
ifeq ($(strip $(GN_APK_AMIGO_SETTINGS_SUPPORT)),yes_6_1_1) 
	 PRODUCT_PACKAGES += Amigo_Settings
endif
#GIONEE: wangfei 2015-4-28 modify for Oversea end->
#Gionee 20150428 chenrui modify for CR01467387 start
#ifeq ($(strip $(GN_APK_AMIGO_SYSTEMUI_SUPPORT)),yes) 
ifneq ($(filter yes%,$(GN_APK_AMIGO_SYSTEMUI_SUPPORT)),) 
	 PRODUCT_PACKAGES += Amigo_SystemUI
endif
#Gionee 20150428 chenrui modify for CR01467387 end
ifeq ($(strip $(GN_APK_AMIGO_DOWNLOADPROVIDER_SUPPORT)),yes) 
#Gionee futao 20150210 modify for CR01446916
  PRODUCT_PACKAGES += Amigo_DownloadProvider
  PRODUCT_PACKAGES += Amigo_DownloadProviderUi
endif
ifeq ($(strip $(GN_APK_AMIGO_PACKAGEINSTALLER_SUPPORT)),yes) 
	 PRODUCT_PACKAGES += Amigo_PackageInstaller
endif
ifeq ($(strip $(GN_APK_AMIGO_SCHAIRPLANE_SUPPORT)),yes) 
	 PRODUCT_PACKAGES += Amigo_SchAirPlane
endif
ifeq ($(strip $(GN_APK_AMIGO_NAVIL_SUPPORT)),yes) 
	 PRODUCT_PACKAGES += Amigo_Navi_Launcher
endif
ifeq ($(strip $(GN_APK_AMIGO_REACTOR_SUPPORT)),yes) 
	 PRODUCT_PACKAGES += Amigo_Reactor
endif
ifeq ($(strip $(GN_APK_AMIGO_CALENDAR_SUPPORT)),yes) 
	 PRODUCT_PACKAGES += Amigo_Calendar
endif
ifeq ($(strip $(GN_APK_AMIGO_CALENDARPROVIDER_SUPPORT)),yes) 
	 PRODUCT_PACKAGES += Amigo_CalendarProvider
endif
ifeq ($(strip $(GN_APK_AMIGO_NAVIKEYGUARD_SUPPORT)),yes) 
	 PRODUCT_PACKAGES += Amigo_NaviKeyguard
endif
ifeq ($(strip $(GN_APK_AMIGO_FLOATINGTOUCH_SUPPORT)),yes) 
	 PRODUCT_PACKAGES += Amigo_FloatingTouch
endif
ifeq ($(strip $(GN_APK_AMIGO_MUSIC_SUPPORT)),yes) 
	 PRODUCT_PACKAGES += Amigo_Music
endif
ifeq ($(strip $(GN_APK_AMIGO_MUSIC_4_0_SUPPORT)),yes) 
	 PRODUCT_PACKAGES += Amigo_Music
endif
ifeq ($(strip $(GN_APK_AMIGO_VIDEO_SUPPORT)),yes) 
	 PRODUCT_PACKAGES += Amigo_Video
endif
ifeq ($(strip $(GN_APK_AMIGO_RINGTONE_SUPPORT)),yes) 
	 PRODUCT_PACKAGES += Amigo_RingTone
endif
ifeq ($(strip $(GN_APK_AMIGO_CAMERA_SUPPORT)),yes) 
	 PRODUCT_PACKAGES += Amigo_Camera
endif
ifeq ($(strip $(GN_APK_AMIGO_CAMERA_SUPPORT)),yes_4_0_temp) 
	 PRODUCT_PACKAGES += Amigo_Camera
endif
#Gionee  <zhangpj> <2015-01-13> add for CR01429061 begin
ifeq ($(strip $(GN_APK_AMIGO_CAMERA_PLUGIN_SUPPORT)),yes) 
     PRODUCT_PACKAGES += PluginProSetting
     PRODUCT_PACKAGES += PluginHDR
     PRODUCT_PACKAGES += PluginPanorama
     PRODUCT_PACKAGES += PluginPicNote
     PRODUCT_PACKAGES += PluginNight
     PRODUCT_PACKAGES += PluginFilter
     PRODUCT_PACKAGES += PluginSmartBurst
     PRODUCT_PACKAGES += SmartBurst
     PRODUCT_PACKAGES += PluginMagicFocus
     PRODUCT_PACKAGES += PluginFaceBeauty
endif
#Gionee  <zhangpj> <2015-01-13> add for CR01429061 end
ifeq ($(strip $(GN_APK_AMIGO_NEWGALLERY_SUPPORT)),yes) 
	 PRODUCT_PACKAGES += Amigo_NewGallery
endif
ifeq ($(strip $(GN_APK_AMIGO_NEWGALLERY_SUPPORT)),yes_5_1_2) 
	 PRODUCT_PACKAGES += Amigo_NewGallery
endif
ifeq ($(strip $(GN_APK_AMIGO_COMPASS_SUPPORT)),yes) 
	 PRODUCT_PACKAGES += Amigo_Compass
endif
ifeq ($(strip $(GN_APK_AMIGO_CALCULATOR_SUPPORT)),yes) 
	 PRODUCT_PACKAGES += Amigo_Calculator
endif
ifeq ($(strip $(GN_APK_AMIGO_FLASHLIGHT_SUPPORT)),yes) 
	 PRODUCT_PACKAGES += Amigo_FlashLight
endif
ifeq ($(strip $(GN_APK_AMIGO_SOUNDRECORDER_SUPPORT)),yes) 
	 PRODUCT_PACKAGES += Amigo_SoundRecorder
endif
ifeq ($(strip $(GN_APK_AMIGO_NOTE_SUPPORT)),yes) 
	 PRODUCT_PACKAGES += Amigo_Note
endif
ifeq ($(strip $(GN_APK_AMIGO_VOICEHELPER_SUPPORT)),yes) 
	 PRODUCT_PACKAGES += Amigo_VoiceHelper
endif
ifeq ($(strip $(GN_APK_AMIGO_DESKCLOCK_SUPPORT)),yes) 
	 PRODUCT_PACKAGES += Amigo_DeskClock
endif
ifeq ($(strip $(GN_APK_AMIGO_FILEMANAGER_SUPPORT)),yes) 
	 PRODUCT_PACKAGES += Amigo_FileManager
endif
#GIONEE: wangfei add begin
ifeq ($(strip $(GN_APK_AMIGO_FILEMANAGER_SUPPORT)),yes_1_5_1) 
	 PRODUCT_PACKAGES += Amigo_FileManager
endif
#GIONEE: wangfei add end
ifeq ($(strip $(GN_APK_AMIGO_BROWSER_SUPPORT)),yes) 
	 PRODUCT_PACKAGES += Amigo_Browser
endif
ifeq ($(strip $(GN_APK_AMIGO_FAN_FAN_SUPPORT)),yes) 
	 PRODUCT_PACKAGES += Amigo_Fan_Fan
endif
ifeq ($(strip $(GN_APK_AMIGO_ACCOUNT_SUPPORT)),yes) 
	 PRODUCT_PACKAGES += Amigo_Account
endif
ifeq ($(strip $(GN_APK_AMIGO_SERVICE_SUPPORT)),yes) 
	 PRODUCT_PACKAGES += Amigo_Service
endif
ifeq ($(strip $(GN_APK_AMIGO_DATAGHOST_SUPPORT)),yes) 
	 PRODUCT_PACKAGES += Ami_DataGhost
endif
ifeq ($(strip $(GN_APK_AMIGO_SYNCHRONIZER_SUPPORT)),yes) 
	 PRODUCT_PACKAGES += Amigo_Synchronizer
endif
ifeq ($(strip $(GN_APK_AMIGO_ANTISTOLEN_SUPPORT)),yes) 
	 PRODUCT_PACKAGES += Amigo_AntiStolen
endif
ifeq ($(strip $(GN_APK_AMIGO_GAMEHALL_SUPPORT)),yes) 
	 PRODUCT_PACKAGES += Amigo_GameHall
endif
#Gionee zhangxiaowei 20141229 add for mmi test start
ifeq ($(strip $(GN_APK_MMITEST_SUPPORT)), yes)
     PRODUCT_PACKAGES += GN_MMITest
endif
ifeq ($(strip $(GN_APK_AUTOMMI_SUPPORT)),yes)
   PRODUCT_PACKAGES += AutoMMI
endif
#Gionee zhangxiaowei 20141229 add for mmi test end
ifeq ($(strip $(GN_APK_GN_GOU_SUPPORT)),yes) 
	 PRODUCT_PACKAGES += GN_Gou-gionee
endif
ifeq ($(strip $(GN_APK_AMIGO_DYMANICWEATHER_SUPPORT)),yes) 
	 PRODUCT_PACKAGES += Amigo_DynamicWeather
endif
ifeq ($(strip $(GN_APK_AMIGO_CHANGER_SUPPORT)),yes) 
	 PRODUCT_PACKAGES += Amigo_Changer
	 PRODUCT_PACKAGES += VLEngine
endif
#Gionee wanghaiyan 2015-4-28 add for CR01472442 start
ifeq ($(strip $(GN_APK_AMIGO_CHANGER_SUPPORT)),yes_6_1_1) 
	 PRODUCT_PACKAGES += Amigo_Changer
	 PRODUCT_PACKAGES += VLEngine
endif
#Gionee wanghaiyan 2015-4-28 add for CR01472442 end
ifeq ($(strip $(GN_APK_AMIGO_SETTINGUPDATE_SUPPORT)),yes) 
	 PRODUCT_PACKAGES += Amigo_SettingUpdate
endif
ifeq ($(strip $(GN_APK_AMIGO_TELEPATH_SUPPORT)),yes) 
	 PRODUCT_PACKAGES += Amigo_Telepath
endif
ifeq ($(strip $(GN_APK_AMIGO_SETUPWIZARD_SUPPORT)),yes) 
	 PRODUCT_PACKAGES += Amigo_SetupWizard
endif
ifeq ($(strip $(GN_APK_GN_PUSH_SUPPORT)),yes) 
	 PRODUCT_PACKAGES += GN_Push
endif
ifeq ($(strip $(GN_APK_AMIGO_PAY_SUPPORT)),yes) 
	 PRODUCT_PACKAGES += Amigo_pay
endif
ifeq ($(strip $(GN_APK_GN_WIFIDIRECT_SUPPORT)),yes) 
	 PRODUCT_PACKAGES += GN_WifiDirect
endif
ifeq ($(strip $(GN_APK_AMIGO_SCHEDULEPOWERONOFF_SUPPORT)),yes) 
	 PRODUCT_PACKAGES += Amigo_SchedulePowerOnOff
endif
ifeq ($(strip $(GN_APK_AMIGO_SETTINGSPROVIDER_SUPPORT)),yes) 
	 PRODUCT_PACKAGES += Amigo_SettingsProvider
endif
ifeq ($(strip $(GN_APK_AMIGO_WIFI_SETTINGS_SUPPORT)),yes) 
	 PRODUCT_PACKAGES += Amigo_Wifi_Settings
endif
ifeq ($(strip $(GN_APK_AMIGO_BLUETOOTH_SUPPORT)), yes)
  PRODUCT_PACKAGES += Amigo_Bluetooth
endif
ifeq ($(strip $(GN_APK_AMIGO_SEARCH_SUPPORT)),yes) 
	 PRODUCT_PACKAGES += Amigo_Search
endif
ifeq ($(strip $(GN_APK_AMIGO_CUSTOMERHELPER_SUPPORT)),yes) 
	 PRODUCT_PACKAGES += Amigo_CustomerHelper
endif
ifeq ("$(GN_SMARTSTAY_SUPPORT)","yes")
  PRODUCT_PACKAGES += GN_SmartStay
endif
ifeq ($(strip $(GN_RO_GN_QUICKOPERATE_SUPPORT)), yes)
   PRODUCT_PACKAGES += GN_WakeUp
endif
#Gionee 20150409 chenrui modify for CR01448300 start
#ifeq ($(strip $(GN_APK_AMIGO_CAREFREELAUNCHER_SUPPORT)), yes)
ifneq ($(filter yes%,$(GN_APK_AMIGO_CAREFREELAUNCHER_SUPPORT)),)
   PRODUCT_PACKAGES += Amigo_CarefreeLauncher
endif
#Gionee 20150409 chenrui modify for CR01448300 end
ifeq ($(strip $(GN_APK_AMIGO_CHAMELEON_SUPPORT)), yes)
   PRODUCT_PACKAGES += Amigo_Chameleon
endif
ifeq ($(strip $(GN_APK_AMIGO_COLORFULLIVEWALLPAPER_SUPPORT)), yes)
   PRODUCT_PACKAGES += Amigo_ColorfulLiveWallpaper
endif
ifeq ($(strip $(GN_APK_AMIGO_SYSTEMSERVICE_SUPPORT)), yes)
   PRODUCT_PACKAGES += Amigo_SystemService
endif

    PRODUCT_PACKAGES += InCallUI

#Gionee <fingerprint> <qudw> <20150121> modify for CR01440177 begin
ifeq ($(strip $(GN_FINGERPRINT_SUPPORT)), FPC)
   PRODUCT_PACKAGES += libcom_fingerprints_sensor
   PRODUCT_PACKAGES += FingerprintService
   PRODUCT_PACKAGES += libcom_fingerprints_service
endif
#Gionee <fingerprint> <qudw> <20150121> modify for CR01440177 end

ifeq ($(strip $(GN_APK_AMIGO_PLAY_SUPPORT)), yes)
   PRODUCT_PACKAGES += Amigo_Play
endif

ifeq ($(strip $(GN_APK_AMIGO_NFCTAG_SUPPORT)), yes)
   PRODUCT_PACKAGES += Amigo_NfcTag
endif
#Gionee <Finger_Unlock> longzp 20150331 add for CR01457465 begin
ifeq ($(strip $(GN_FINGERPRINT_UNLOCK_SUPPORT)), yes)
   PRODUCT_PACKAGES += GN_FingerUnlock
endif
#Gionee <Finger_Unlock> longzp 20150331 add for CR01457465 end

ifeq ($(strip $(GN_APK_AMIGO_WALLET_SUPPORT)), yes)
   PRODUCT_PACKAGES += Amigo_Wallet
endif

################### DRIVER BEGIN ####################################

################### DRIVER END ####################################


################### 3RD BEGIN ####################################
ifeq ($(strip $(GN_3RD_BAIDULBS_SUPPORT)),yes) 
	 PRODUCT_PACKAGES += NetworkLocation
endif
ifeq ($(strip $(GN_3RD_SPEECHSERVICE_SUPPORT)),yes) 
	 PRODUCT_PACKAGES += speechService
endif
ifeq ($(strip $(GN_3RD_IREADER_SUPPORT)),yes) 
	 PRODUCT_PACKAGES += iReader
endif
ifeq ($(strip $(GN_3RD_AORAMARKET_SUPPORT)),yes) 
	 PRODUCT_PACKAGES += AORAMARKET
endif

ifeq ($(strip $(GN_3RD_XUNFEI_INPUTMETHOD_SUPPORT)),yes) 
     PRODUCT_PACKAGES += iFlyIME
endif

ifeq ($(strip $(GN_3RD_UCBROWSER_SUPPORT)),yes) 
	 PRODUCT_PACKAGES += UCBrowser
endif
ifeq ($(strip $(GN_3RD_BAIDUMAP_SUPPORT)),yes) 
     PRODUCT_PACKAGES += BaiduMap
endif
ifeq ($(strip $(GN_3RD_VIPSHOP_SUPPORT)),yes) 
     PRODUCT_PACKAGES += vipshop
endif
ifeq ($(strip $(GN_3RD_HAOKAN_NEWS_SUPPORT)),yes) 
     PRODUCT_PACKAGES += haokan
endif
ifeq ($(strip $(GN_3RD_BAIDU_SEARCH_SUPPORT)),yes) 
     PRODUCT_PACKAGES += baidusearch
endif
ifeq ($(strip $(GN_3RD_TENCENT_PHONEMANAGER_SUPPORT)),yes) 
	 PRODUCT_PACKAGES += tencentmobilemanager
endif
ifeq ($(strip $(GN_3RD_BOYADDZ_SUPPORT)),yes) 
	 PRODUCT_PACKAGES += ddz
endif
ifeq ($(strip $(GN_3RD_TUDOU_SUPPORT)),yes) 
	 PRODUCT_PACKAGES += Tudou
endif
ifeq ($(strip $(GN_3RD_KAIXIN_XXL_SUPPORT)),yes) 
	 PRODUCT_PACKAGES += jinli
endif
ifeq ($(strip $(GN_3RD_TAOBAO_HOT_SUPPORT)),yes) 
	 PRODUCT_PACKAGES += TaobaoHot
endif
ifeq ($(strip $(GN_3RD_NEWSARTICLE_SUPPORT)),yes) 
	 PRODUCT_PACKAGES += NewsArticle
endif
ifeq ($(strip $(GN_3RD_WEIXIN_SUPPORT)),yes) 
	 PRODUCT_PACKAGES += weixin
endif
ifeq ($(strip $(GN_3RD_QUNAR_SUPPORT)),yes) 
	 PRODUCT_PACKAGES += Qunar
endif
ifeq ($(strip $(GN_3RD_QQ_SUPPORT)), yes)
   PRODUCT_PACKAGES += QQ
endif

ifeq ($(strip $(GN_3RD_UC_SUPPORT)), yes)
   PRODUCT_PACKAGES += UC
endif

#Gionee <3rd_GMS> <futao> <2014-12-15> add for GMS function begin
ifdef GN3RD_GMS_SUPPORT
ifneq ($(strip $(GN3RD_GMS_SUPPORT)), no)
$(call inherit-product-if-exists, $(GN_BUILD_ROOT_DIR)/packages/apps/3rd_GMS/gn_gms.mk)
endif
endif
#Gionee <3rd_GMS> <futao> <2014-12-15> add for GMS function end

ifeq ($(strip $(GN_3RD_NEWSARTICLE_SUPPORT)), yes)
   PRODUCT_PACKAGES += NewsArticle
endif

ifeq ($(strip $(GN_3RD_JINRITOUTIAO_SUPPORT)), yes)
   PRODUCT_PACKAGES += JinRiTouTiao
endif

ifeq ($(strip $(GN_3RD_BOYADDZ_SUPPORT)), yes)
   PRODUCT_PACKAGES += BoYaDDZ
endif

ifeq ($(strip $(GN_3RD_QUNAER_SUPPORT)), yes)
   PRODUCT_PACKAGES += QuNaEr
endif

ifeq ($(strip $(GN_3RD_WEIPINHUI_SUPPORT)), yes)
   PRODUCT_PACKAGES += WeiPinHui
endif

ifeq ($(strip $(GN_3RD_TUDOU_SUPPORT)), yes)
   PRODUCT_PACKAGES += TuDou
endif

ifeq ($(strip $(GN_3RD_HAOKAN_SUPPORT)), yes)
   PRODUCT_PACKAGES += HaoKan
endif

ifeq ($(strip $(GN_3RD_KAIXINXXL_SUPPORT)), yes)
   PRODUCT_PACKAGES += KaiXinXXL
endif

ifeq ($(strip $(GN_3RD_WEIXIN_SUPPORT)), yes)
   PRODUCT_PACKAGES += WeiXin
endif

ifeq ($(strip $(GN_3RD_TENCENTMANAGER_SUPPORT)), yes)
   PRODUCT_PACKAGES += TencentManager
endif

ifeq ($(strip $(GN_3RD_SOUGOUINPUTMETHOD_SUPPORT)), yes)
   PRODUCT_PACKAGES += SouGouInputMethod
endif

ifeq ($(strip $(GN_3RD_SINAWEIBO_SUPPORT)), yes)
   PRODUCT_PACKAGES += SinaWeiBo
endif

ifeq ($(strip $(GN_3RD_TAOHOT_SUPPORT)), yes)
   PRODUCT_PACKAGES += TaoHot
endif

ifeq ($(strip $(GN_3RD_BAIDUSEARCH_SUPPORT)), yes)
   PRODUCT_PACKAGES += BaiduSearch
endif

ifeq ($(strip $(GN_3RD_BAIDUMAP_SUPPORT)), yes)
   PRODUCT_PACKAGES += BaiduMap
endif


#Gionee <3rd_GMS> <futao> <2014-12-15> add for GMS function begin
ifdef GN3RD_GMS_SUPPORT
ifneq ($(strip $(GN3RD_GMS_SUPPORT)), no)
$(call inherit-product-if-exists, $(GN_BUILD_ROOT_DIR)/packages/apps/3rd_GMS/gn_gms.mk)
endif
endif
#Gionee <3rd_GMS> <futao> <2014-12-15> add for GMS function end

#Aurora <aurora app> <hujianwei> <20150825> add for aurora app control begin
$(call inherit-product-if-exists, $(GN_BUILD_ROOT_DIR)/packages/apps/mtk/product.mk)
#Aurora <aurora app> <hujianwei> <20150825> add for aurora app control end


#Gionee <Amigo_Font> <fengxb> <2015-04-08> add for CR01459284 begin
ifeq ($(strip $(GN_3RD_WEBVIEWGOOGLE_SUPPORT)),yes) 
	 PRODUCT_PACKAGES += WebViewGoogle
endif
#Gionee <Amigo_Font> <fengxb> <2015-04-08> add for CR01459284 end

#Gionee <fingerprint> <qudw> <2015-04-22> add for CR01468738 begin
ifeq ($(strip $(GN_ALIPAY_TESTTOOL_SUPPORT)),yes) 
    PRODUCT_PACKAGES += TestTAREE
    PRODUCT_PACKAGES += libteeclientsoft
endif
#Gionee <fingerprint> <qudw> <2015-04-22> add for CR01468738 end

################### 3RD END ####################################

################### libgn_prt begin ####################################
PRODUCT_PACKAGES += libgn_prt_jni
PRODUCT_PACKAGES += createcipher
################### libgn_prt end ####################################

