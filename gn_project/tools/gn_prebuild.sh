#!/bin/bash
#################################################

###dont remove packages

#
#if [ -z "${GIONEEPRODUCTID}" ]; then
#    echo "*** procuction invalid. ***"
#    exit 1
#else
#    rm -rf "./packages"
#    VENDOR="$1"
#    [ -n "$2" ] && PLATFORM_VERSION="_$2"
#fi

#GiONEE:20130725 taofp add for gen custom config begin
if [ "${GN_OVERSEA_PRODUCT}" == "yes" ];then
    . `dirname $0`/gn_oversea_gen_custom.sh
fi
#GiONEE:20130725 taofp add for gen custom config end

copy_to_alps(){
    sourcedir="$1"
    destdir="$2"
    if [ -d "$sourcedir" ];then
        echo "copy $sourcedir to  $destdir "
        pushd $sourcedir >/dev/null
        find . -path "*\.svn" -prune -o -type f -follow -print | cpio -pdmu --quiet $destdir
        popd >/dev/null
    fi
}


#if [ "${BUILD_OPTIONS}" == "platform" ];then
#    [ -z "${GN_APPS_NAME}" ] && GN_APPS_NAME="packages_mtk_l1_6753_65c_mp" 
#    ln -f -s "../${GN_APPS_NAME}/packages" "./packages"
#else
#   [ -z "${GN_APPS_NAME}" ] && GN_APPS_NAME="gionee_packages_apk_amigo_3.0"
#   if [ -d "../${GN_APPS_NAME}" ];then
#       if [ `expr match "${GN_APPS_NAME}" "^packages" ` -ne 0 ];then
#           ln -f -s "../${GN_APPS_NAME}/packages" "./packages"
#       else
#           ln -f -s "../${GN_APPS_NAME}/packages/${VENDOR}/packages${PLATFORM_VERSION}" "./packages"
#           echo "ln -f -s ../${GN_APPS_NAME}/packages/${VENDOR}/packages${PLATFORM_VERSION} to packages."
#       fi
#   else
#       echo "*** Link package directory error ***" >&2
#       exit 1
#   fi
#fi


alps_dir=`pwd`
###modify by aurora hujianwei 20150608 start

if [ "${GIONEEPRODUCTID:(-4):4}" == "IUNI" -o "${GIONEEPRODUCTID:(-3):3}" == "CTS" ]; then
#    if [ -d "../aurora_data" ];then
#             ln -f -s "../aurora_data" "./aurora_data"
#    else
#             echo "*** Link aurora_data  directory error ***" >&2
#             exit 1
#    fi
            echo "copy alps_IUNI"
            copy_to_alps "./gionee/alps_IUNI/" "$alps_dir"
fi
###modify by aurora hujianwei 20150608 start

#copy gionee/alps_drv to alps 
copy_to_alps "./gionee/alps_build" $alps_dir

#copy gionee/alps_drv to alps 
copy_to_alps "./gionee/alps_drv" $alps_dir

#GIONEE: luohui 2014-10-15 modify for CR01394477 start->
if [ "${GN_OVERSEA_ODM}" == "yes" ];then
    copy_to_alps "./gionee/alps_drv/ODM_drv" "$alps_dir"
fi
#GIONEE: luohui 2014-10-15 modify for CR01394477 end<-

#copy gionee/{projectID} to alps 
copy_to_alps "./gionee/project/${GIONEEPROJECTID}" "$alps_dir"

#copy gionee/{productID} to alps 
copy_to_alps "./gionee/project/${GIONEEPRODUCTID}" "$alps_dir"

#copy gionee/alps/alps_cta/alps_cmcc/alps_platform to alps
if [ "${BUILD_OPTIONS}" == "platform" -a "${GIONEEPRODUCTID:(-8):8}" == "platform" ];then
    copy_to_alps "./gionee/alps_platform" "$alps_dir"
elif [ "${BUILD_OPTIONS}" == "platform" -a "${GIONEEPRODUCTID:(-3):3}" == "CTA" ];then
    copy_to_alps "./gionee/alps_cta" "$alps_dir"
elif [ "${BUILD_OPTIONS}" == "platform" -a "${GIONEEPRODUCTID:(-4):4}" == "CMCC" ];then
    copy_to_alps "./gionee/alps_cmcc" "$alps_dir"
else
    #应用所有项目共用
    copy_to_alps "./gionee/alps/" "$alps_dir"
    #copy gionee/alps_oversea to alps
    if [ "${GN_OVERSEA_PRODUCT}" == "yes" ];then
        copy_to_alps "./gionee/alps_oversea/" "$alps_dir"
    fi
    #应用某个项目共用
    copy_to_alps "./gionee/project/${GIONEEPROJECTID}_alps" "$alps_dir"
    #应用某个项目某产品共用
    copy_to_alps "./gionee/project/${GIONEEPRODUCTID}_alps" "$alps_dir"

    copy_to_alps "./gionee/gnframework-res" "$alps_dir/gnframework-res" 
    copy_to_alps  "gionee/alps/YouJuAgent/" "$alps_dir/YouJuAgent"
    copy_to_alps  "gionee/alps/YouJuAgent/out" "$alps_dir/out"
    if [ -d "./gionee/Amigo_Framework/" ];then
        rm -rf "$alps_dir/amigoframework/"
        copy_to_alps "./gionee/Amigo_Framework/" "$alps_dir/amigoframework/"
    else
        copy_to_alps "./gionee/amigoframework/" "$alps_dir/amigoframework/"
        copy_to_alps "./gionee/amigoframework/out" "$alps_dir/out"
    fi
    #GIONEE: luohui 2014-04-03 modify for CR01014872 start->
    #copy gionee/alps_oversea amigoframework to amigoframework
    if [ "${GN_OVERSEA_PRODUCT}" == "yes" ];then
        if [ -d "./gionee/alps_oversea/amigoframework" ];then
            echo "copy ./gionee/alps_oversea/amigoframework/ to ./amigoframework."
            copy_to_alps "./gionee/alps_oversea/amigoframework/" "$alps_dir/amigoframework/"
            copy_to_alps "./gionee/alps_oversea/amigoframework/out" "$alps_dir/out"
        fi

        #Gionee 20150417 chenrui add for CR01465382 start
        if [ -d "./gionee/alps_oversea/gnframework-res" ];then
            echo "copy ./gionee/alps_oversea/gnframework-res/ to ./gnframework-res."
            copy_to_alps "./gionee/alps_oversea/gnframework-res/" "$alps_dir/gnframework-res/"
        fi
        #Gionee 20150417 chenrui add for CR01465382 end
    fi
    #GIONEE: luohui 2014-04-03 modify for CR01014872 end<-

fi

if [ "${BUILD_OPTIONS}" != "platform" ]
then

    #copy platform to alps
    if [ -d "../${GN_APPS_NAME}/platform" ];then
        copy_to_alps "../${GN_APPS_NAME}/platform/common"  "$alps_dir"
        copy_to_alps "../${GN_APPS_NAME}/platform/${VENDOR}" "$alps_dir"
    fi

    #copy packages to alps
    if [ -d "../${GN_APPS_NAME}/packages" ];then
        copy_to_alps "../${GN_APPS_NAME}/packages/common/" "$alps_dir"
    fi

    #copy gionee packages to alps
    if [ -d "../${GN_APPS_NAME}/packages/${VENDOR}/gionee_apps" ];then
        copy_to_alps "../${GN_APPS_NAME}/packages/${VENDOR}/gionee_apps/" "$alps_dir"
    fi

    #unzip gn_image.zip and copy to alps
    if [ -e "../${GN_APPS_NAME}/gn_image/gn_img_"${GIONEEPRODUCTID}".zip" ]
    then
        unzip -o -q ../${GN_APPS_NAME}/gn_image/gn_img_"${GIONEEPRODUCTID}".zip  -d ../${GN_APPS_NAME}/gn_image
        if [ -d "../${GN_APPS_NAME}/gn_image/gn_img" ];then
            find ../${GN_APPS_NAME}/gn_image/gn_img -type f -name "Thumbs.db*" | xargs rm -rf
            copy_to_alps "../${GN_APPS_NAME}/gn_image/gn_img" "$alps_dir"
        else
            echo "*** gn_image dirs tree error ***"
        fi
    fi

    #copy gn_config/product_data/oversea to alps
    if [ "${GN_OVERSEA_PRODUCT}" == "yes" ];then
        ANDROID_VER="$2"
        copy_to_alps "../${GN_APPS_NAME}/gn_config/product_data/oversea/COMMON/${ANDROID_VER}/" "$alps_dir"

        if [ "${GN_OVERSEA_ODM}" == "yes" ];then
            copy_to_alps "../${GN_APPS_NAME}/gn_config/product_data/oversea/ODM/${ANDROID_VER}/" "$alps_dir"
        else
            copy_to_alps "../${GN_APPS_NAME}/gn_config/product_data/oversea/GIONEE/${ANDROID_VER}/" "$alps_dir"
        fi
        echo "copy ../${GN_APPS_NAME}/gn_config/product_data/${GIONEEPROJECTID} to alps."
        copy_to_alps "../${GN_APPS_NAME}/gn_config/product_data/${GIONEEPROJECTID}" "$alps_dir"
    fi
    #copy gn_config to alps
    copy_to_alps "../${GN_APPS_NAME}/gn_config/product_data/${GIONEEPRODUCTID}"  "$alps_dir"

    if [ "${GN_THEME_WHITE_SUPPORT}" == "yes" ];then
        copy_to_alps "../${GN_APPS_NAME}/gn_config/theme_white" "$alps_dir"
    else
        copy_to_alps "../${GN_APPS_NAME}/gn_config/theme_black" "$alps_dir"
    fi

    . `dirname $0`/gn_auto_config.sh
else
    echo "*** platform ***"
fi
if [ "${GIONEEPRODUCTID:(-3):3}" == "CTS" ]; then
    echo "copy alps_cts"
    copy_to_alps "./gionee/alps_cts" "$alps_dir"
fi
