# Copyright 2006 The Android Open Source Project

# Setting LOCAL_PATH will mess up all-subdir-makefiles, so do it beforehand.
legacy_modules := power uevent vibrator wifi qemu qemu_tracing

SAVE_MAKEFILES := $(call all-named-subdir-makefiles,$(legacy_modules))
LEGACY_AUDIO_MAKEFILES := $(call all-named-subdir-makefiles,audio)

LOCAL_PATH:= $(call my-dir)

#Gionee BSP1 ningyd 20150303 modify for CR01452914 immersion vibrate begin
include $(CLEAR_VARS)
ifeq ($(TARGET_ARCH),arm64)
LOCAL_MODULE := libimmvibe
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := STATIC_LIBRARIES
LOCAL_MODULE_SUFFIX := .a
LOCAL_SRC_FILES_arm := vibrator/lib/libimmvibe.a
LOCAL_SRC_FILES_arm64 := vibrator/lib64/libimmvibe.a
LOCAL_MODULE_TARGET_ARCH := arm64 arm
LOCAL_MULTILIB := both
include $(BUILD_PREBUILT)
else
LOCAL_MODULE_TAGS := optional
LOCAL_PREBUILT_LIBS := vibrator/lib/libimmvibe.a
include $(BUILD_MULTI_PREBUILT)
endif
#Gionee BSP1 ningyd 20150303 modify for CR01452914 immersion vibrate end

include $(CLEAR_VARS)

LOCAL_SHARED_LIBRARIES := libcutils liblog

LOCAL_INCLUDES += $(LOCAL_PATH)

LOCAL_CFLAGS  += -DQEMU_HARDWARE
QEMU_HARDWARE := true

LOCAL_SHARED_LIBRARIES += libdl

include $(SAVE_MAKEFILES)

LOCAL_MODULE:= libhardware_legacy

include $(BUILD_SHARED_LIBRARY)

# static library for librpc
include $(CLEAR_VARS)

LOCAL_MODULE:= libpower

LOCAL_SRC_FILES += power/power.c

include $(BUILD_STATIC_LIBRARY)

# shared library for various HALs
include $(CLEAR_VARS)

LOCAL_MODULE := libpower

LOCAL_SRC_FILES := power/power.c

LOCAL_SHARED_LIBRARIES := libcutils

include $(BUILD_SHARED_LIBRARY)

# legacy_audio builds it's own set of libraries that aren't linked into
# hardware_legacy
include $(LEGACY_AUDIO_MAKEFILES)
