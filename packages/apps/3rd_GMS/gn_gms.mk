##########################################
#   
#   GMS custom
##########History of modify############### 
#2014-12-16 futao  Create
##########################################
ifdef GN3RD_GMS_SUPPORT 
ifneq ($(filter $(strip $(GN3RD_GMS_SUPPORT)),64_5_0_r2 64_5_1_r1),)
$(call inherit-product, $(GN_BUILD_ROOT_DIR)/packages/apps/3rd_GMS/3rd_GMS_$(GN3RD_GMS_SUPPORT)/products/gms.mk)
##GIONEE futao 20150104 modify for GMS overlay begin
ifeq ($(strip $(GN_GMS_TEST_MODIFY)),yes)
PRODUCT_PACKAGE_OVERLAYS += packages/apps/3rd_GMS/overlay
PRODUCT_PROPERTY_OVERRIDES += \
            ro.gn.gms.test.modify=yes
endif
##GIONEE futao 20150104 modify for GMS overlay end

#######Client ID begin
ifdef GN_OVERSEA_ODM
ifeq ($(strip $(GN_OVERSEA_ODM)),no)
PRODUCT_PROPERTY_OVERRIDES += \
      ro.com.google.clientidbase=android-gionee \
      ro.com.google.clientidbase.ms=android-gionee \
      ro.com.google.clientidbase.yt=android-gionee \
      ro.com.google.clientidbase.am=android-gionee \
      ro.com.google.clientidbase.gmm=android-gionee
else
ifneq ($(filter $(strip $(GN_OVERSEA_CUSTOM)),MEAST_FLY RUSSIA_FLY),)
PRODUCT_PROPERTY_OVERRIDES += \
      ro.com.google.clientidbase=android-fly \
      ro.com.google.clientidbase.ms=android-fly \
      ro.com.google.clientidbase.yt=android-fly \
      ro.com.google.clientidbase.am=android-fly \
      ro.com.google.clientidbase.gmm=android-fly
endif

ifneq ($(filter $(strip $(GN_OVERSEA_CUSTOM)),EUROPE_KAZAM),)
PRODUCT_PROPERTY_OVERRIDES += \
      ro.com.google.clientidbase=android-KAZAM \
      ro.com.google.clientidbase.ms=android-KAZAM \
      ro.com.google.clientidbase.yt=android-KAZAM \
      ro.com.google.clientidbase.am=android-KAZAM \
      ro.com.google.clientidbase.gmm=android-KAZAM
endif

ifneq ($(filter $(strip $(GN_OVERSEA_CUSTOM)),RUSSIA_PRESTIGIO),)
PRODUCT_PROPERTY_OVERRIDES += \
      ro.com.google.clientidbase=android-prestigio \
      ro.com.google.clientidbase.ms=android-prestigio \
      ro.com.google.clientidbase.yt=android-prestigio \
      ro.com.google.clientidbase.am=android-prestigio \
      ro.com.google.clientidbase.gmm=android-prestigio
endif

ifneq ($(filter $(strip $(GN_OVERSEA_CUSTOM)),ITALY_NGM),)
PRODUCT_PROPERTY_OVERRIDES += \
      ro.com.google.clientidbase=android-ngmitalia \
      ro.com.google.clientidbase.ms=android-ngmitalia \
      ro.com.google.clientidbase.yt=android-ngmitalia \
      ro.com.google.clientidbase.am=android-ngmitalia \
      ro.com.google.clientidbase.gmm=android-ngmitalia
endif

ifneq ($(filter $(strip $(GN_OVERSEA_CUSTOM)),ALGERIA_CONDOR),)
PRODUCT_PROPERTY_OVERRIDES += \
      ro.com.google.clientidbase=android-condor \
      ro.com.google.clientidbase.ms=android-condor \
      ro.com.google.clientidbase.yt=android-condor \
      ro.com.google.clientidbase.am=android-condor \
      ro.com.google.clientidbase.gmm=android-condor
endif

ifneq ($(filter $(strip $(GN_OVERSEA_CUSTOM)),TURKEY_GM),)
PRODUCT_PROPERTY_OVERRIDES += \
      ro.com.google.clientidbase=android-gmphone \
      ro.com.google.clientidbase.ms=android-gmphone \
      ro.com.google.clientidbase.yt=android-gmphone \
      ro.com.google.clientidbase.am=android-gmphone \
      ro.com.google.clientidbase.gmm=android-gmphone
endif

ifneq ($(filter $(strip $(GN_OVERSEA_CUSTOM)),VISUALFAN),)
PRODUCT_PROPERTY_OVERRIDES += \
      ro.com.google.clientidbase=android-allview \
      ro.com.google.clientidbase.ms=android-allview \
      ro.com.google.clientidbase.yt=android-allview \
      ro.com.google.clientidbase.am=android-allview \
      ro.com.google.clientidbase.gmm=android-allview
endif

endif
endif
#######Client ID end
endif
endif
