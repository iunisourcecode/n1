LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)
LOCAL_MODULE := libtlcWidevineModularDrm
LOCAL_SRC_FILES_64 := libtlcWidevineModularDrm.so
LOCAL_SRC_FILES_32 := arm/libtlcWidevineModularDrm.so
LOCAL_SHARED_LIBRARIES := libMcClient libcrypto libsec_mem libssl libstdc++ libstlport libz
LOCAL_MULTILIB := both
LOCAL_MODULE_CLASS := SHARED_LIBRARIES
LOCAL_MODULE_SUFFIX := .so
include $(BUILD_PREBUILT)
