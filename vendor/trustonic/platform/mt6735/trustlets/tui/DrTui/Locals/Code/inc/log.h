#include "drStd.h"
#include "DrApi/DrApi.h"

#if defined(TAG)
#define printf(fmt, args...)  drDbgPrintLnf("[DRTUI]"TAG fmt, ##args)
#else
#define printf(fmt, args...)  drDbgPrintLnf("[DRTUI]"fmt, ##args)
#endif
