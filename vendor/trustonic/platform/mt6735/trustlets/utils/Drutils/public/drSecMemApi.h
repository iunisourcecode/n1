/*
 * Copyright (c) 2013 TRUSTONIC LIMITED
 * All rights reserved
 *
 * The present software is the confidential and proprietary information of
 * TRUSTONIC LIMITED. You shall not disclose the present software and shall
 * use it only in accordance with the terms of the license agreement you
 * entered into with TRUSTONIC LIMITED. This software may be subject to
 * export or import laws in certain countries.
 */

/*
 * @file   tlDriverApi.h
 * @brief  Contains trustlet API definitions
 *
 */

#ifndef __TLDRIVERSECMEMAPI_H__
#define __TLDRIVERSECMEMAPI_H__

#if defined(TRUSTEDAPP)
#include "taStd.h"
#else
#include "TlApi/TlApiCommon.h"
#include "tlStd.h"
#endif
#include "TlApi/TlApiError.h"

#include "tlmem_api.h"


/*
 * Open session to the driver with given data
 *
 * @return  session id
 */

_TLAPI_EXTERN_C uint32_t tlApiOpenSession( void );

/*
 * Close session
 *
 * @param sid  session id
 *
 * @return  TLAPI_OK upon success or specific error
 */
_TLAPI_EXTERN_C tlApiResult_t tlApiCloseSession( uint32_t sid );


/*
 * Executes command
 *
 * @param sid        session id
 * @param commandId  command id
 *
 * @return  TLAPI_OK upon success or specific error
 */
_TLAPI_EXTERN_C tlApiResult_t tlApiExecute(
        uint32_t sid,
        tlApimem_ptr memData);


/* tlApi function to call driver via IPC.
 * Function should be called only from customer specific TlApi library. 
 * Sends a MSG_RQ message via IPC to a MobiCore driver.
 *
 * @param driverID The driver to send the IPC to.
 * @param pMarParam MPointer to marshaling parameters to send to the driver.
 *
 * @return TLAPI_OK
 * @return E_TLAPI_COM_ERROR in case of an IPC error.
 */
_TLAPI_EXTERN_C tlApiResult_t tlApi_callDriver(
        uint32_t driver_ID,
        void* pMarParam);

#ifdef SECMEM_DEBUG
_TLAPI_EXTERN_C tlApiResult_t drSecMemAlloc_Debug(uint32_t * handle, uint32_t size, uint32_t alignment, uint8_t *owner, uint32_t id);
_TLAPI_EXTERN_C tlApiResult_t drSecMemRef_Debug(uint32_t handle, uint32_t *refcount, uint8_t *owner, uint32_t id);
_TLAPI_EXTERN_C tlApiResult_t drSecMemUnref_Debug(uint32_t handle, uint32_t *refcount, uint8_t *owner, uint32_t id);
_TLAPI_EXTERN_C tlApiResult_t drSecMemAllocPA_Debug(uint32_t * phy_addr, uint32_t size, uint32_t alignment, uint8_t *owner, uint32_t id);
_TLAPI_EXTERN_C tlApiResult_t drSecMemRefPA_Debug(uint32_t phy_addr, uint32_t *refcount, uint8_t *owner, uint32_t id);
_TLAPI_EXTERN_C tlApiResult_t drSecMemUnrefPA_Debug(uint32_t phy_addr, uint32_t *refcount, uint8_t *owner, uint32_t id);
_TLAPI_EXTERN_C tlApiResult_t drSecMemAllocTBL_Debug(uint32_t *handle, uint32_t size, uint32_t alignment, uint8_t *owner, uint32_t id);
_TLAPI_EXTERN_C tlApiResult_t drSecMemUnrefTBL_Debug(uint32_t handle, uint32_t *refcount, uint8_t *owner, uint32_t id);

#define drSecMemAlloc(hndl, size, align)        drSecMemAlloc_Debug(hndl, size, align, __FILE__, __LINE__)
#define drSecMemRef(hndl, refcnt)               drSecMemRef_Debug(hndl, refcnt, __FILE__, __LINE__)
#define drSecMemUnref(hndl, refcnt)             drSecMemUnref_Debug(hndl, refcnt, __FILE__, __LINE__)
#define drSecMemAllocPA(phyaddr, size, align)   drSecMemAllocPA_Debug(phyaddr, size, align, __FILE__, __LINE__)
#define drSecMemRefPA(phyaddr, refcnt)          drSecMemRefPA_Debug(phyaddr, refcnt, __FILE__, __LINE__)
#define drSecMemUnrefPA(phyaddr, refcnt)        drSecMemUnrefPA_Debug(phyaddr, refcnt, __FILE__, __LINE__)
#define drSecMemAllocTBL(hndl, size, align)     drSecMemAllocTBL_Debug(hndl, size, align, __FILE__, __LINE__)
#define drSecMemUnrefTBL(hndl, refcnt)          drSecMemUnrefTBL_Debug(hndl, refcnt, __FILE__, __LINE__)

#else
_TLAPI_EXTERN_C tlApiResult_t drSecMemAlloc(uint32_t * handle, uint32_t size, uint32_t alignment);
_TLAPI_EXTERN_C tlApiResult_t drSecMemRef(uint32_t handle, uint32_t *refcount);
_TLAPI_EXTERN_C tlApiResult_t drSecMemUnref(uint32_t handle, uint32_t *refcount);
_TLAPI_EXTERN_C tlApiResult_t drSecMemAllocPA(uint32_t * phy_addr, uint32_t size, uint32_t alignment);
_TLAPI_EXTERN_C tlApiResult_t drSecMemRefPA(uint32_t phy_addr, uint32_t *refcount);
_TLAPI_EXTERN_C tlApiResult_t drSecMemUnrefPA(uint32_t phy_addr, uint32_t *refcount);
_TLAPI_EXTERN_C tlApiResult_t drSecMemAllocTBL(uint32_t *handle, uint32_t size, uint32_t alignment);
_TLAPI_EXTERN_C tlApiResult_t drSecMemUnrefTBL(uint32_t handle, uint32_t *refcount);
#endif

_TLAPI_EXTERN_C tlApiResult_t drSecMemQuery(tlApimem_ptr mem_result_ptr);
_TLAPI_EXTERN_C tlApiResult_t drSecMemQueryArray(tlApimem_ptr mem_result_ptr, uint32_t len);
_TLAPI_EXTERN_C tlApiResult_t drSecMemQueryPA(tlApimem_ptr mem_result_ptr);
_TLAPI_EXTERN_C tlApiResult_t drSecMemQueryArrayPA(tlApimem_ptr mem_result_ptr, uint32_t len);
_TLAPI_EXTERN_C tlApiResult_t drSecMemQueryPool(uint32_t *start_addr, uint32_t *size);
_TLAPI_EXTERN_C tlApiResult_t drSecMemDumpInfo(void);

#define drSecMemFree(fmt, args...) drSecMemUnref(fmt, ##args)
#define drSecMemFreePA(fmt, args...) drSecMemUnrefPA(fmt, ##args)

        
#endif // __TLDRIVERSECMEMAPI_H__