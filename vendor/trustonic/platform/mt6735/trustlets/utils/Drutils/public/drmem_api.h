/*
 * Copyright (c) 2013 TRUSTONIC LIMITED
 * All rights reserved
 * 
 * The present software is the confidential and proprietary information of
 * TRUSTONIC LIMITED. You shall not disclose the present software and shall
 * use it only in accordance with the terms of the license agreement you
 * entered into with TRUSTONIC LIMITED. This software may be subject to
 * export or import laws in certain countries.
 */

/**
 * @file   drfoo_Api.h
 * @brief  Contains DCI command definitions and data structures
 *
 */

#ifndef __DRMEMAPI_H__
#define __DRMEMAPI_H__

#include "dci.h"

/*
 * Command ID's
 */
#define CMD_SEC_DRV_MEM_ALLOC          1
#define CMD_SEC_DRV_MEM_REF            2
#define CMD_SEC_DRV_MEM_UNREF          3
#define CMD_SEC_DRV_MEM_ALLOC_PA       4
#define CMD_SEC_DRV_MEM_REF_PA         5
#define CMD_SEC_DRV_MEM_UNREF_PA       6
#define CMD_SEC_DRV_MEM_QUERY          7
#define CMD_SEC_DRV_MEM_QUERY_ARRAY    8
#define CMD_SEC_DRV_MEM_QUERY_POOL     9
#define CMD_SEC_DRV_MEM_ALLOC_TBL      10
#define CMD_SEC_DRV_MEM_UNREF_TBL      11
#define CMD_SEC_DRV_MEM_QUERY_SECMEM_PATTERN     13

#define CMD_SEC_DRV_MEM_INFO_DUMP      255

/*... add more command ids when needed */

/*
 * command message.
 *
 * @param len Lenght of the data to process.
 * @param data Data to be processed
 */
#define EXIT_ERROR                  ((uint32_t)(-1))

typedef struct {
    dciCommandHeader_t  header;     /**< Command header */
    uint32_t            len;        /**< Length of data to process */
} dr_cmd_t;

/**
 * Response structure
 */
typedef struct {
    dciResponseHeader_t header;     /**< Response header */
    uint32_t            len;
} dr_rsp_t;

/*
 * DCI message data.
 */
typedef struct {
    union {
        dr_cmd_t     cmd_secmem;
        dr_rsp_t     rsp_secmem;
    };
    uint32_t    alignment;  /* IN */    
    uint32_t    size;       /* IN */    
    uint32_t    refcount;   /* INOUT */    
    uint32_t    sec_handle; /* OUT */
    uint32_t    ResultData;
} dciMessage_t;

/*
 * Driver UUID. Update accordingly after reserving UUID
 */
#define DRV_DBG_UUID { { 7, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 } }

#endif // __DRMEMAPI_H__
