/*
 * Copyright (c) 2013-2014 TRUSTONIC LIMITED
 * All rights reserved
 *
 * The present software is the confidential and proprietary information of
 * TRUSTONIC LIMITED. You shall not disclose the present software and shall
 * use it only in accordance with the terms of the license agreement you
 * entered into with TRUSTONIC LIMITED. This software may be subject to
 * export or import laws in certain countries.
 */

#ifndef TL_SAMPLE_FLOAT_API_H_
#define TL_SAMPLE_FLOAT_API_H_

#include "tci.h"

/**
 * Command ID's for communication Trusted Application Connector (TAC)
 * -> Trusted Application (TA).
 */
#define CMD_SAMPLE_FLOAT_INIT 1 /**< Init floating point variables in FLOAT TA  */
#define CMD_SAMPLE_FLOAT_ADD  2 /**< Add FPs in FLOAT TA  */
#define CMD_SAMPLE_FLOAT_MUL  3 /**< Multiply FPs in FLOAT TA  */
#define CMD_SAMPLE_FLOAT_SQRT 4 /**< Calculate square root in FLOAT TA  */
#define CMD_SAMPLE_FLOAT_OPERATORS 10 /**< Use float and double with C-operators in FLOAT TA  */
#define CMD_SAMPLE_FLOAT_FUNCTIONS 20 /**< Use common FP runtime functions in FLOAT TA */
#define CMD_SAMPLE_FLOAT_REGISTERS 30 /**< Use FP registers in FLOAT TA */

/**
 * Return codes
 */
#define RET_ERR_FLOAT_INVALID_LENGTH RET_CUSTOM_START + 1

/**
 * Termination codes
 */
#define EXIT_ERROR ((uint32_t)(-1))


typedef union 
{
    float  f;
    double d;
} tlFloat_param_t;

/**
 * TlFloat command messages.
 *
 * The FLOAT INIT command will get the values of floating points into params 0-3.
 * The FLOAT ADD command will add params 0 and 1 into param 2.
 * The FLOAT MUL command will multiply params 0 and 1 into param 2.
 *
 * @param params Data to process.
 */
typedef struct {
    tciCommandHeader_t header; /**< Command header */
    tlFloat_param_t    params[4];
} cmdFLOAT_t;

/**
 * Response structure TA -> TAC.
 */
typedef struct {
    tciResponseHeader_t header; /**< Response header */
    tlFloat_param_t    params[4];
} rspFLOAT_t;

/**
 * TCI message data.
 */
typedef struct {
    union {
        tciCommandHeader_t command; /**< Command header */
        tciResponseHeader_t response; /**< Response header */
        cmdFLOAT_t cmdFLOAT;
        rspFLOAT_t rspFLOAT;
    };
} tciMessage_t;

/**
 * TA UUID.
 */
#define TL_SAMPLE_FLOAT_UUID { { 0x66, 0x70, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 } }

#endif // TL_SAMPLE_FLOAT_API_H_
