/*
 * Copyright (c) 2013-2014 TRUSTONIC LIMITED
 * All rights reserved
 *
 * The present software is the confidential and proprietary information of
 * TRUSTONIC LIMITED. You shall not disclose the present software and shall
 * use it only in accordance with the terms of the license agreement you
 * entered into with TRUSTONIC LIMITED. This software may be subject to
 * export or import laws in certain countries.
 */
#define taSampleRot13_UUID { 0xd51a83c9, 0x474a, 0x5655, { 0xaf, 0x2a, 0x58, 0xdc, 0xfd, 0x2b, 0x1d, 0x37 }} 
